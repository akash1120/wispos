<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//-------------------------------------------------------------------------
// Tables.
//-------------------------------------------------------------------------
// Database table names.
//
	$config['tables']['lists']  = 'megamenu_lists';
	$config['tables']['groups'] = 'megamenu_groups';


//-------------------------------------------------------------------------
// Max levels.
//-------------------------------------------------------------------------
// Max number of levels in list. Set 0 for unlimited levels.
//
	$config['max_levels'] = 1;


//-------------------------------------------------------------------------
// Dropdown parent name.
//-------------------------------------------------------------------------
// First name in dropdown.
//
	$config['dropdown']['parent'] = '--- None ---';


//-------------------------------------------------------------------------
// Dropdown space.
//-------------------------------------------------------------------------
// Space between levels in dropdown.
//
	$config['dropdown']['space'] = '&nbsp;&nbsp;&nbsp;';


//-------------------------------------------------------------------------
// Flash names.
//-------------------------------------------------------------------------
// Names for flash success and error.
//
	$config['flash']['success'] = 'success';
	$config['flash']['error']   = 'error';


/* End of file adjacency_list.php */
/* Location: ./config/adjacency_list.php */