<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Shop_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct();

        $this->Settings = $this->site->get_setting();

        if(file_exists(APPPATH.'controllers'.DIRECTORY_SEPARATOR.'shop'.DIRECTORY_SEPARATOR.'Shop.php')) {

            define("SHOP", 1);
            $this->load->shop_model('shop_model');
            $this->load->library('Tec_cart', '', 'cart');
            $this->shop_settings = $this->shop_model->getShopSettings();
            if($shop_language = get_cookie('shop_language', TRUE)) {
                $this->config->set_item('language', $shop_language);
                $this->lang->admin_load('sma', $shop_language);
                $this->lang->shop_load('shop', $shop_language);
                $this->Settings->user_language = $shop_language;
            } else {
                $this->config->set_item('language', $this->Settings->language);
                $this->lang->admin_load('sma', $this->Settings->language);
                $this->lang->shop_load('shop', $this->Settings->language);
                $this->Settings->user_language = $this->Settings->language;
            }

            $this->theme = $this->Settings->theme.'/shop/views/';
            if(is_dir(VIEWPATH.$this->Settings->theme.DIRECTORY_SEPARATOR.'shop'.DIRECTORY_SEPARATOR.'assets')) {
                $this->data['assets'] = 'themes/' . $this->Settings->theme . '/shop/assets/';
            } else {
               // $this->data['assets'] = base_url() . 'themes/default/shop/assets/';
                $this->data['assets'] = 'themes/enso/shop/assets/';
            }
            //current country
             $this->get_client_ip();
            //exit('sd');
            $url = json_decode(file_get_contents("http://api.ipinfodb.com/v3/ip-city/?key=f6308086f528b070f5eac795c2fa7936dfa04ef405cfa6fb1ffa3bf9c14acf69&ip=".$this->get_client_ip()."&format=json"));
           // $getloc = json_decode(file_get_contents("http://ipinfo.io/"));
            $this->Settings->current_country_code = $url->countryCode;
            $this->Settings->current_country = $this->shop_model->getCountryByCode($this->Settings->current_country_code);
            $current_country_currency = $this->shop_model->getCurrencyById($this->Settings->current_country->currency_id);
            $this->Settings->current_country_currency = $current_country_currency->symbol;
            if($this->Settings->current_country_currency == ''){
                $this->Settings->current_country_currency =  $this->Settings->default_currency;
            }


            if($check_country = get_cookie('check_current_country', TRUE)) {
                $this->Settings->check_country = 1;
            } else {
                $this->Settings->check_country = 0;
            }

            //end current country


            if($selected_country = get_cookie('shop_country', TRUE)) {

                $this->Settings->selected_country = $selected_country;
            } else {
                $this->Settings->selected_country = $this->Settings->default_country;
            }

            if($selected_currency = get_cookie('shop_currency', TRUE)) {
                $this->Settings->selected_currency = $selected_currency;
            } else {
                $this->Settings->selected_currency = $this->Settings->default_currency;
            }


            $this->default_currency = $this->shop_model->getCurrencyByCode($this->Settings->default_currency);
            $this->data['default_currency'] = $this->default_currency;
            $this->selected_currency = $this->shop_model->getCurrencyByCode($this->Settings->selected_currency);
            if(empty($this->selected_currency)){
                $this->selected_currency = $this->default_currency;
            }
            $this->data['selected_currency'] = $this->selected_currency;

            if($selected_country = get_cookie('shop_country', TRUE)) {

                $this->Settings->selected_country = $selected_country;
            } else {
                $this->Settings->selected_country = $this->Settings->default_country;
            }

            $this->default_country = $this->shop_model->getCountryByCode($this->Settings->default_country);
            $this->data['default_country'] = $this->default_country;
            $this->selected_country = $this->shop_model->getCountryByCode($this->Settings->selected_country);
            $this->data['selected_country'] = $this->selected_country;

            if ($this->Settings->default_country != $this->Settings->selected_country) {
                $this->Settings->country_vat  = $this->selected_country->vat;
                $this->Settings->country_price_group  = $this->selected_country->price_group_id;
                $this->Settings->country_name  = $this->selected_country->name;
            }else{

                $this->Settings->country_vat  = $this->default_country->vat;
                $this->Settings->country_price_group  = $this->default_country->price_group_id;
                $this->Settings->country_name  = $this->default_country->name;
            }
            $this->session->set_userdata('country_vat',$this->Settings->country_vat);
            $this->session->set_userdata('country_price_group', $this->Settings->country_price_group);

            if ($this->Settings->default_currency != $this->Settings->selected_currency) {
                $this->Settings->current_currency_symbol  = $this->selected_currency->symbol;
            }else{
                $this->Settings->current_currency_symbol  = $this->default_currency->symbol;
            }
            if ($this->Settings->current_country_code == $this->Settings->default_country) {
                $this->Settings->check_country = 1;
            }

            $this->loggedIn = $this->sma->logged_in();
            $this->data['loggedIn'] = $this->loggedIn;
            $this->loggedInUser = $this->site->getUser();
            $this->data['loggedInUser'] = $this->loggedInUser;
            if ($this->loggedIn) {
                $this->Customer = $this->sma->in_group('customer') ? TRUE : NULL;
                $this->data['Customer'] = $this->Customer;
                $this->Supplier = $this->sma->in_group('supplier') ? TRUE : NULL;
                $this->data['Supplier'] = $this->Supplier;
                $this->Staff = (!$this->sma->in_group('customer') && !$this->sma->in_group('supplier') ) ? TRUE : NULL;
                $this->data['Staff'] = $this->Staff;
            } else {
                $this->config->load('hybridauthlib');
            }

            if($sd = $this->shop_model->getDateFormat($this->Settings->dateformat)) {
                $dateFormats = array(
                    'js_sdate' => $sd->js,
                    'php_sdate' => $sd->php,
                    'mysq_sdate' => $sd->sql,
                    'js_ldate' => $sd->js . ' hh:ii',
                    'php_ldate' => $sd->php . ' H:i',
                    'mysql_ldate' => $sd->sql . ' %H:%i'
                    );
            } else {
                $dateFormats = array(
                    'js_sdate' => 'mm-dd-yyyy',
                    'php_sdate' => 'm-d-Y',
                    'mysq_sdate' => '%m-%d-%Y',
                    'js_ldate' => 'mm-dd-yyyy hh:ii:ss',
                    'php_ldate' => 'm-d-Y H:i:s',
                    'mysql_ldate' => '%m-%d-%Y %T'
                    );
            }
            $this->dateFormats = $dateFormats;
            $this->data['dateFormats'] = $dateFormats;

        } else {
            define("SHOP", 0);
        }

        $this->m = strtolower($this->router->fetch_class());
        $this->v = strtolower($this->router->fetch_method());
        $this->data['m']= $this->m;
        $this->data['v'] = $this->v;
        $this->Settings->indian_gst = FALSE;
        if ($this->Settings->invoice_view > 0) {
            $this->Settings->indian_gst = $this->Settings->invoice_view == 2 ? TRUE : FALSE;
            $this->Settings->format_gst = TRUE;
            $this->load->library('gst');
        }

    }

    function page_construct($page, $data = array()) {
        if (SHOP) {
            $data['message'] = isset($data['message']) ? $data['message'] : $this->session->flashdata('message');
            $data['error'] = isset($data['error']) ? $data['error'] : $this->session->flashdata('error');
            $data['warning'] = isset($data['warning']) ? $data['warning'] : $this->session->flashdata('warning');
            $data['reminder'] = isset($data['reminder']) ? $data['reminder'] : $this->session->flashdata('reminder');

            $data['Settings'] = $this->Settings;
            $data['shop_settings'] = $this->shop_settings;
            $data['currencies'] = $this->shop_model->getAllCurrencies();
            $data['countries'] = $this->shop_model->getAllCountries();
            $data['pages'] = $this->shop_model->getAllPages();
            $data['brands'] = $this->shop_model->getAllBrands();
            $data['system_settings_main_shop'] = $this->shop_model->getSettings();
            $categories = $this->shop_model->getAllCategories();

            $getKidsCategories = $this->shop_model->getKidsCategories();
            $data['kidscategories'] = $getKidsCategories;

            $getscarfCategories = $this->shop_model->getScarfMixCategories();
            $data['scarfCategories'] = $getscarfCategories;

            $getmostsearchCategories = $this->shop_model->getMostSearchCategories();
            $data['mostSearchCategories'] = $getmostsearchCategories;

            $getcarouselCategories = $this->shop_model->getCarouselCategories();
            $data['carousel_categories'] = $getcarouselCategories;

            $data['latest_products'] = $this->shop_model->getLatestProducts();

            //$data['upcoming_promotions'] = $this->shop_model->getUpcomingPromotions();
            $upcoming_promotions = $this->shop_model->getUpcomingPromotions('onCheckout');

            if($upcoming_promotions){
                foreach ($upcoming_promotions as $upcoming_promotion){
                    $img_path = $this->shop_model->getProductImagePath($upcoming_promotion->item_id);
                }
                $data['upcoming_promotions'] = $upcoming_promotions;
                $data['upcoming_promotions_img_path'] = $img_path;
            }



            foreach ($categories as $category) {
                $cat = $category;
                $cat->subcategories = $this->shop_model->getSubCategories($category->id);
                $cat->product_count = $this->shop_model->getRelatedProducts($category->id);
                $cats[] = $cat;
            }
            $data['categories'] = $cats;
            $data['cart'] = $this->cart->cart_data(true);

            if (!$this->loggedIn && $this->Settings->captcha) {
                $this->load->helper('captcha');
                $vals = array(
                    'img_path' => './assets/captcha/',
                    'img_url' => base_url('assets/captcha/'),
                    'img_width' => 210,
                    'img_height' => 34,
                    'word_length' => 5,
                    'colors' => array('background' => array(255, 255, 255), 'border' => array(204, 204, 204), 'text' => array(102, 102, 102), 'grid' => array(204, 204, 204))
                    );
                $cap = create_captcha($vals);
                $capdata = array(
                    'captcha_time' => $cap['time'],
                    'ip_address' => $this->input->ip_address(),
                    'word' => $cap['word']
                    );

                $query = $this->db->insert_string('captcha', $capdata);
                $this->db->query($query);
                $data['image'] = $cap['image'];
                $data['captcha'] = array('name' => 'captcha',
                    'id' => 'captcha',
                    'type' => 'text',
                    'class' => 'form-control',
                    'required' => 'required',
                    'placeholder' => lang('type_captcha')
                    );
            }

            $data['side_featured'] = $this->shop_model->getFeaturedProducts(4, false);
            $data['wishlist'] = $this->shop_model->getWishlist(TRUE);
            $data['info'] = $this->shop_model->getNotifications();
            $data['ip_address'] = $this->input->ip_address();
            $data['image_path'] =  !empty($data['product']->image) ? $data['product']->image : 'caf_image.png';
            $data['page_desc'] = isset($data['page_desc']) && !empty($data['page_desc']) ? $data['page_desc'] : 'Default description';

            $main_footer = $this->shop_model->getPageBySlug('main-footer');
            $data['main_footer'] = html_entity_decode(htmlspecialchars_decode($main_footer->body));
            /* open grapgh settings start*/
            $data['og_title'] ='';
            $data['og_type'] ='website';
            $data['og_site_name'] ='Ensodubai';
            $data['og_description'] ='';
            $data['og_url'] ='';
            $data['og_image'] ='';

            /* open grapgh settings end*/

            $uri_arary = $this->uri->segment_array();
            if(!empty($uri_arary)) {

                $current_slug = end($uri_arary);
                /* seo settings start*/
                $data['meta_title'] = $this->shop_model->getMetaTitleBySlug($current_slug);
                $data['meta_description'] =  $this->shop_model->getMetaDescriptionBySlug($current_slug);

                if($data['meta_title'] == ''){
                    $data['meta_title'] = $this->shop_model->getMetaTitleBySlug('default');
                }
                if($data['meta_description'] == ''){
                    $data['meta_description'] =  $this->shop_model->getMetaDescriptionBySlug('default');
                }
                /* seo settings end*/

                /* open grapgh settings start*/
                $og_settings =$this->shop_model->getOpenGraphDataBySlug($current_slug);
                if(!empty($og_settings)){
                    $data['og_title'] = $og_settings->opn_title;
                    $data['og_description'] = $og_settings->opn_description;
                    $data['og_url'] = base_url(uri_string());
                    $data['og_image'] =base_url('assets/uploads/').$og_settings->image;
                }
                /* open grapgh settings end*/

            }else{
                /* seo settings start*/
                $data['meta_title'] = $this->shop_model->getMetaTitleBySlug('default');
                $data['meta_description'] =  $this->shop_model->getMetaDescriptionBySlug('default');
                /* seo settings end*/

                /* open grapgh settings start*/
                $og_settings =$this->shop_model->getOpenGraphDataBySlug('default');
                if(!empty($og_settings)){
                    $data['og_title'] = $og_settings->opn_title;
                    $data['og_description'] = $og_settings->opn_description;
                    $data['og_url'] = base_url(uri_string());
                    //$data['og_image'] =base_url('assets/uploads/').$og_settings->image;
                    $data['og_image'] =base_url('ucloud/plugins/filepreviewer/site/resize_image_inline.php?f='.$og_settings->image.'&w=160&h=134&m=medium&uh=&o=jpg');
                }
                /* open grapgh settings end*/
            }

            $this->load->view($this->theme . 'header', $data);
            $this->load->view($this->theme . $page, $data);
            $this->load->view($this->theme . 'footer');
        }
    }
    // Function to get the client IP address
    function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

}