<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Products_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getAllProducts()
    {
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllProductsSlug()
    {
        $this->db->select('slug');
        $this->db->where('hide' , 0 );
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row->slug;
            }
            return $data;
        }
        return FALSE;
    }
    public function getAllPagesSlug()
    {
        $this->db->select('slug');
        $q = $this->db->get('pages');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row->slug;
            }
            return $data;
        }
        return FALSE;
    }
    public function getAllCategoriesSlug()
    {
        $q = $this->db->get('categories');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                if($row->parent_id == 0){
                    $data[] = $row->slug;
                }else{
                    $parent_category_name = $this->getCategoryNameByID($row->parent_id);

                    $data[] = $parent_category_name.'/'.$row->slug;
                }
            }
            return $data;
        }
        return FALSE;
    }
    public function getCategoryNameByID($id)
    {
        $q = $this->db->get_where('categories', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row()->slug;
        }
        return FALSE;
    }
    public function getMetaTitleBySlug($slug)
    {
        $q = $this->db->get_where('seo_settings', array('slug' => $slug), 1);
        if ($q->num_rows() > 0) {
            return $q->row()->meta_title;
        }
        return FALSE;
    }
    public function getMetaDescriptionBySlug($slug)
    {
        $q = $this->db->get_where('seo_settings', array('slug' => $slug), 1);
        if ($q->num_rows() > 0) {
            return $q->row()->meta_description;
        }
        return FALSE;
    }
    public function getProductImageBySlug($slug)
    {
        $q = $this->db->get_where('products', array('slug' => $slug), 1);
        if ($q->num_rows() > 0) {
            return $q->row()->image;
        }
        return 'no_image.png';
    }
    public function getOpenGraphDataBySlug($slug)
    {
        $q = $this->db->get_where('opengraph_settings', array('slug' => $slug), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
    public function getSeoSettingsDataBySlug($slug)
    {
        $q = $this->db->get_where('seo_settings', array('slug' => $slug), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCategoryProducts($category_id)
    {
        $q = $this->db->get_where('products', array('category_id' => $category_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSubCategoryProducts($subcategory_id)
    {
        $q = $this->db->get_where('products', array('subcategory_id' => $subcategory_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductOptions($pid)
    {
        $q = $this->db->get_where('product_variants', array('product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
    public function getProductOptionsForWebsite($pid)
    {
        $q = $this->db->get_where('product_variants', array('product_id' => $pid,'website_status' => '1'));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductOptionsWithWH($pid)
    {
        $this->db->select($this->db->dbprefix('product_variants') . '.*, ' . $this->db->dbprefix('warehouses') . '.name as wh_name, ' . $this->db->dbprefix('warehouses') . '.id as warehouse_id, ' . $this->db->dbprefix('warehouses_products_variants') . '.quantity as wh_qty')
            ->join('warehouses_products_variants', 'warehouses_products_variants.option_id=product_variants.id', 'left')
            ->join('warehouses', 'warehouses.id=warehouses_products_variants.warehouse_id', 'left')
            //->group_by(array('' . $this->db->dbprefix('product_variants') . '.id', '' . $this->db->dbprefix('warehouses_products_variants') . '.warehouse_id'))
            ->order_by('product_variants.id');
        $q = $this->db->get_where('product_variants', array('product_variants.product_id' => $pid, 'warehouses_products_variants.quantity !=' => NULL));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getProductComboItems($pid)
    {
        $this->db->select($this->db->dbprefix('products') . '.id as id, ' . $this->db->dbprefix('products') . '.code as code, ' . $this->db->dbprefix('combo_items') . '.quantity as qty, ' . $this->db->dbprefix('products') . '.name as name, ' . $this->db->dbprefix('combo_items') . '.unit_price as price')->join('products', 'products.code=combo_items.item_code', 'left')->group_by('combo_items.id');
        $q = $this->db->get_where('combo_items', array('product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
        return FALSE;
    }

    public function getProductByID($id)
    {
        $q = $this->db->get_where('products', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductWithCategory($id)
    {
        $this->db->select($this->db->dbprefix('products') . '.*, ' . $this->db->dbprefix('categories') . '.name as category, translated_category_name ')
            ->join('categories', 'categories.id=products.category_id', 'left');
        $q = $this->db->get_where('products', array('products.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function has_purchase($product_id, $warehouse_id = NULL)
    {
        if($warehouse_id) { $this->db->where('warehouse_id', $warehouse_id); }
        $q = $this->db->get_where('purchase_items', array('product_id' => $product_id), 1);
        if ($q->num_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function getProductDetails($id)
    {
        $this->db->select($this->db->dbprefix('products') . '.code, ' . $this->db->dbprefix('products') . '.name, ' . $this->db->dbprefix('categories') . '.code as category_code, cost, price, quantity, alert_quantity')
            ->join('categories', 'categories.id=products.category_id', 'left');
        $q = $this->db->get_where('products', array('products.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductDetail($id)
    {
        $this->db->select($this->db->dbprefix('products') . '.*, ' . $this->db->dbprefix('tax_rates') . '.name as tax_rate_name, '.$this->db->dbprefix('tax_rates') . '.code as tax_rate_code, c.code as category_code, sc.code as subcategory_code', FALSE)
            ->join('tax_rates', 'tax_rates.id=products.tax_rate', 'left')
            ->join('categories c', 'c.id=products.category_id', 'left')
            ->join('categories sc', 'sc.id=products.subcategory_id', 'left');
        $q = $this->db->get_where('products', array('products.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSubCategories($parent_id) {
        $this->db->select('id as id, name as text')
            ->where('parent_id', $parent_id)->order_by('name');
        $q = $this->db->get("categories");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductByCategoryID($id)
    {

        $q = $this->db->get_where('products', array('category_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return true;
        }
        return FALSE;
    }

    public function getAllWarehousesWithPQ($product_id)
    {
        $this->db->select('' . $this->db->dbprefix('warehouses') . '.*, ' . $this->db->dbprefix('warehouses_products') . '.quantity, ' . $this->db->dbprefix('warehouses_products') . '.rack')
            ->join('warehouses_products', 'warehouses_products.warehouse_id=warehouses.id', 'left')
            ->where('warehouses_products.product_id', $product_id);
            //->group_by('warehouses.id');
        $q = $this->db->get('warehouses');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductPhotos($id)
    {
        $q = $this->db->get_where("product_photos", array('product_id' => $id , 'status'=>1 ));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }
    public function getGalleryImages($pid)
    {
        $q = $this->db->get_where('product_photos', array('product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            //$data[] = $q->result_array();
            return $data;
        }
        return FALSE;
    }

    public function getGalleryimagesByUniquId($id)
    {
        $q = $this->db->get_where("product_photos", array('gallery_unique_code	' => $id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getGalleryimagesById($id)
    {
        $q = $this->db->get_where("product_photos", array('product_id	' => $id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function addGalleryImages($unique_id, $photo)
    {
        $this->db->insert('product_photos', array('gallery_unique_code' => $unique_id, 'photo' => $photo));
        $photo_id = $this->db->insert_id();
        $q = $this->db->get_where('product_photos', array('id' => $photo_id), 1);
        if ($q->num_rows() > 0) {
            echo json_encode($q->row());
        }
        return true;
    }

    public function removeGalleryImage($photo)
    {
        $this->db->delete('product_photos', array('photo' => $photo));
        /* if ($photo) {
             $file = getcwd() .
        '/assets/uploads/' . $photo;
             $file_thumb = getcwd() . '/assets/uploads/thumbs/' . $photo;
             if (file_exists($file)) {
                 unlink($file);
             }
             if (file_exists($file_thumb)) {
                 unlink($file_thumb);
             }
         }*/
        return true;
    }

    public function editGalleryImages($id, $photo)
    {
        $this->db->insert('product_photos', array('product_id' => $id, 'photo' => $photo));
        $photo_id = $this->db->insert_id();
        $q = $this->db->get_where('product_photos', array('id' => $photo_id), 1);
        if ($q->num_rows() > 0) {
            echo json_encode($q->row());
        }
        return true;
    }


    public function getProductAllPhotos($id)
    {
        $q1 = $this->db->get_where("products", array('id' => $id , 'hide'=>0));
        $q = $this->db->get_where("product_photos", array('product_id' => $id , 'status'=>1 ));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $gallery_images[] = base_url().'assets/uploads/'.$row->photo;
            }
        }

        if ($q1->num_rows() > 0) {
            foreach (($q1->result()) as $row1) {
                $main_image[] = base_url().'assets/uploads/'.$row1->image;
            }
        }

        if($gallery_images){
            return implode(',' , $main_image)  . ',' . implode(',' , $gallery_images);
        }else{
            return implode(',' , $main_image) ;
        }

    }

    public function updateProductPhots($insertData){
        $this->db->update_batch('product_photos', $insertData, 'id');
        return true;
    }

    public function getProductByCode($code)
    {
        $q = $this->db->get_where('products', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addProduct($data, $items, $warehouse_qty, $product_attributes, $photos)
    {
        /* echo "<pre>";
         print_r($data);
         print_r($product_attributes);
         exit('asd');*/
        $gallery_images = $data['gimage'];
        $gallery_images_alt = $data['gimage_alt'];
        $variants = $data['variants'];
        $additional_guide = $data['additional_guide'];
        $attributesInput2 = $data['attributesInput2'];
        unset($data['gimage_alt']);
        unset($data['variants']);
        unset($data['additional_guide']);
        unset($data['attributesInput2']);
        unset($data['gimage']);

        $gallary_unique_code = $data['gallary_unique_code'];
        unset($data['gallary_unique_code']);
        if ($this->db->insert('products', $data)) {
            $product_id = $this->db->insert_id();

            if ($items) {
                foreach ($items as $item) {
                    $item['product_id'] = $product_id;
                    $this->db->insert('combo_items', $item);
                }
            }

            $warehouses = $this->site->getAllWarehouses();
            if ($data['type'] != 'standard') {
                foreach ($warehouses as $warehouse) {
                    $this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse->id, 'quantity' => 0));
                }
            }

            $tax_rate = $this->site->getTaxRateByID($data['tax_rate']);

            if ($warehouse_qty && !empty($warehouse_qty)) {
                foreach ($warehouse_qty as $wh_qty) {
                    if (isset($wh_qty['quantity']) && ! empty($wh_qty['quantity'])) {
                        $this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $wh_qty['warehouse_id'], 'quantity' => $wh_qty['quantity'], 'rack' => $wh_qty['rack'], 'avg_cost' => $data['cost']));

                        if (!$product_attributes) {
                            $tax_rate_id = $tax_rate ? $tax_rate->id : NULL;
                            $tax = $tax_rate ? (($tax_rate->type == 1) ? $tax_rate->rate . "%" : $tax_rate->rate) : NULL;
                            $unit_cost = $data['cost'];
                            if ($tax_rate) {
                                if ($tax_rate->type == 1 && $tax_rate->rate != 0) {
                                    if ($data['tax_method'] == '0') {
                                        $pr_tax_val = ($data['cost'] * $tax_rate->rate) / (100 + $tax_rate->rate);
                                        $net_item_cost = $data['cost'] - $pr_tax_val;
                                        $item_tax = $pr_tax_val * $wh_qty['quantity'];
                                    } else {
                                        $net_item_cost = $data['cost'];
                                        $pr_tax_val = ($data['cost'] * $tax_rate->rate) / 100;
                                        $unit_cost = $data['cost'] + $pr_tax_val;
                                        $item_tax = $pr_tax_val * $wh_qty['quantity'];
                                    }
                                } else {
                                    $net_item_cost = $data['cost'];
                                    $item_tax = $tax_rate->rate;
                                }
                            } else {
                                $net_item_cost = $data['cost'];
                                $item_tax = 0;
                            }

                            $subtotal = (($net_item_cost * $wh_qty['quantity']) + $item_tax);

                            $item = array(
                                'product_id' => $product_id,
                                'product_code' => $data['code'],
                                'product_name' => $data['name'],
                                'net_unit_cost' => $net_item_cost,
                                'unit_cost' => $unit_cost,
                                'real_unit_cost' => $unit_cost,
                                'quantity' => $wh_qty['quantity'],
                                'quantity_balance' => $wh_qty['quantity'],
                                'quantity_received' => $wh_qty['quantity'],
                                'item_tax' => $item_tax,
                                'tax_rate_id' => $tax_rate_id,
                                'tax' => $tax,
                                'subtotal' => $subtotal,
                                'warehouse_id' => $wh_qty['warehouse_id'],
                                'date' => date('Y-m-d'),
                                'status' => 'received',
                            );
                            $this->db->insert('purchase_items', $item);
                            $this->site->syncProductQty($product_id, $wh_qty['warehouse_id']);
                        }
                    }
                }
            }

            if ($product_attributes) {
                foreach ($product_attributes as $pr_attr) {
                    $pr_attr_details = $this->getPrductVariantByPIDandName($product_id, $pr_attr['name']);


                    $pr_attr['product_id'] = $product_id;
                    $variant_warehouse_id = $pr_attr['warehouse_id'];
                    unset($pr_attr['warehouse_id']);
                    if ($pr_attr_details) {
                        $option_id = $pr_attr_details->id;
                    } else {
                        $this->db->insert('product_variants', $pr_attr);
                        $option_id = $this->db->insert_id();
                    }
                    if ($pr_attr['quantity'] != 0) {
                        $this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $variant_warehouse_id, 'quantity' => $pr_attr['quantity']));

                        $tax_rate_id = $tax_rate ? $tax_rate->id : NULL;
                        $tax = $tax_rate ? (($tax_rate->type == 1) ? $tax_rate->rate . "%" : $tax_rate->rate) : NULL;
                        $unit_cost = $data['cost'];
                        if ($tax_rate) {
                            if ($tax_rate->type == 1 && $tax_rate->rate != 0) {
                                if ($data['tax_method'] == '0') {
                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / (100 + $tax_rate->rate);
                                    $net_item_cost = $data['cost'] - $pr_tax_val;
                                    $item_tax = $pr_tax_val * $pr_attr['quantity'];
                                } else {
                                    $net_item_cost = $data['cost'];
                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / 100;
                                    $unit_cost = $data['cost'] + $pr_tax_val;
                                    $item_tax = $pr_tax_val * $pr_attr['quantity'];
                                }
                            } else {
                                $net_item_cost = $data['cost'];
                                $item_tax = $tax_rate->rate;
                            }
                        } else {
                            $net_item_cost = $data['cost'];
                            $item_tax = 0;
                        }

                        $subtotal = (($net_item_cost * $pr_attr['quantity']) + $item_tax);
                        $item = array(
                            'product_id' => $product_id,
                            'product_code' => $data['code'],
                            'product_name' => $data['name'],
                            'net_unit_cost' => $net_item_cost,
                            'unit_cost' => $unit_cost,
                            'quantity' => $pr_attr['quantity'],
                            'option_id' => $option_id,
                            'quantity_balance' => $pr_attr['quantity'],
                            'quantity_received' => $pr_attr['quantity'],
                            'item_tax' => $item_tax,
                            'tax_rate_id' => $tax_rate_id,
                            'tax' => $tax,
                            'subtotal' => $subtotal,
                            'warehouse_id' => $variant_warehouse_id,
                            'date' => date('Y-m-d'),
                            'status' => 'received',
                        );
                        $item['option_id'] = !empty($item['option_id']) && is_numeric($item['option_id']) ? $item['option_id'] : NULL;
                        $this->db->insert('purchase_items', $item);

                    }

                    /*foreach ($warehouses as $warehouse) {
                        $default_warehousee = 1;
                        if (!$this->getWarehouseProductVariant($default_warehousee, $product_id, $option_id)) {
                            $this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $warehouse->id, 'quantity' => 0));
                        }
                    }*/

                    $this->site->syncVariantQty($option_id, $variant_warehouse_id);
                }

                $length = count($variants);
                for ($i = 0; $i < $length; $i++) {
                    if($attributesInput2[$i] !='') {
                        $item_variant = array(
                            'variant' => $variants[$i],
                            'variant_value' => $attributesInput2[$i],
                            'additional_guide' => $additional_guide[$i],
                            'product_id' => $product_id
                        );
                        $this->db->insert('variants_combination', $item_variant);
                    }
                }
            }

            $length1 = count($gallery_images);
            for ($j = 0; $j < $length1; $j++) {
                if($gallery_images[$j] !='') {
                    $item_gallery = array(
                        'photo' => $gallery_images[$j],
                        'alt_text'=>$gallery_images_alt[$j],
                        'product_id' => $product_id
                    );
                    $this->db->insert('product_photos', $item_gallery);
                }
            }

            return true;
        }
        return false;

    }

    public function getPrductVariantByPIDandName($product_id, $name)
    {
        $q = $this->db->get_where('product_variants', array('product_id' => $product_id, 'name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addAjaxProduct($data)
    {
        if ($this->db->insert('products', $data)) {
            $product_id = $this->db->insert_id();
            return $this->getProductByID($product_id);
        }
        return false;
    }

    public function add_products($products = array())
    {
        if (!empty($products)) {
            foreach ($products as $product) {
                $variants = explode('|', $product['variants']);
                unset($product['variants']);
                if ($this->db->insert('products', $product)) {
                    $product_id = $this->db->insert_id();
                    foreach ($variants as $variant) {
                        if ($variant && trim($variant) != '') {
                            $vat = array('product_id' => $product_id, 'name' => trim($variant));
                            $this->db->insert('product_variants', $vat);
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }

    public function getProductNames($term, $limit = 5)
    {
        $this->db->select('' . $this->db->dbprefix('products') . '.id, code, ' . $this->db->dbprefix('products') . '.name as name, ' . $this->db->dbprefix('products') . '.price as price, ' . $this->db->dbprefix('product_variants') . '.name as vname')
            ->where("type != 'combo' AND "
                . "(" . $this->db->dbprefix('products') . ".name LIKE '%" . $term . "%' OR code LIKE '%" . $term . "%' OR
                concat(" . $this->db->dbprefix('products') . ".name, ' (', code, ')') LIKE '%" . $term . "%')");
        $this->db->join('product_variants', 'product_variants.product_id=products.id', 'left')
            ->where('' . $this->db->dbprefix('product_variants') . '.name', NULL)
            ->group_by('products.id')->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getQASuggestions($term, $limit = 5)
    {
        $this->db->select('' . $this->db->dbprefix('products') . '.id, code, ' . $this->db->dbprefix('products') . '.name as name')
            ->where("type != 'combo' AND "
                . "(" . $this->db->dbprefix('products') . ".name LIKE '%" . $term . "%' OR code LIKE '%" . $term . "%' OR
                concat(" . $this->db->dbprefix('products') . ".name, ' (', code, ')') LIKE '%" . $term . "%')")
            ->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getProductsForPrinting($term, $limit = 5)
    {
        $this->db->select('' . $this->db->dbprefix('products') . '.id, , translated_name, code, ' . $this->db->dbprefix('products') . '.name as name, ' . $this->db->dbprefix('products') . '.price as price')
            ->where("(" . $this->db->dbprefix('products') . ".name LIKE '%" . $term . "%' OR code LIKE '%" . $term . "%' OR
                concat(" . $this->db->dbprefix('products') . ".name, ' (', code, ')') LIKE '%" . $term . "%')")
            ->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }


    public function getProductsForPrintingEnabled($term, $limit = 5)
    {
        $this->db->where('hide' , 0 );
        $this->db->select('' . $this->db->dbprefix('products') . '.id, , translated_name, code, ' . $this->db->dbprefix('products') . '.name as name, ' . $this->db->dbprefix('products') . '.price as price')
            ->where("(" . $this->db->dbprefix('products') . ".name LIKE '%" . $term . "%' OR code LIKE '%" . $term . "%' OR
                concat(" . $this->db->dbprefix('products') . ".name, ' (', code, ')') LIKE '%" . $term . "%')")
            ->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function updateProduct($id, $data, $items, $warehouse_qty, $product_attributes, $photos, $update_variants)
    {

        $gallery_images = $data['gimage'];
        $gallery_images_alt = $data['gimage_alt'];
        unset($data['gimage']);
        unset($data['gimage_alt']);
        $variants = $data['variants'];
        $additional_guide = $data['additional_guide'];
        $attributesInput2 = $data['attributesInput2'];
        unset($data['variants']);
        unset($data['additional_guide']);
        unset($data['attributesInput2']);
        if ($this->db->update('products', $data, array('id' => $id))) {

            if ($items) {
                $this->db->delete('combo_items', array('product_id' => $id));
                foreach ($items as $item) {
                    $item['product_id'] = $id;
                    $this->db->insert('combo_items', $item);
                }
            }

            $tax_rate = $this->site->getTaxRateByID($data['tax_rate']);

            if ($warehouse_qty && !empty($warehouse_qty)) {
                foreach ($warehouse_qty as $wh_qty) {
                    $this->db->update('warehouses_products', array('rack' => $wh_qty['rack']), array('product_id' => $id, 'warehouse_id' => $wh_qty['warehouse_id']));
                }
            }

            if (!empty($update_variants)) {
                //  $this->db->update_batch('product_variants', $update_variants, 'id');
            }

            if ($photos) {
                foreach ($photos as $photo) {
                    $this->db->insert('product_photos', array('product_id' => $id, 'photo' => $photo));
                }
            }


            $product_variants =  $this->getProductOptions($id);
            $reserved_variants = array();
            $reserved_save_variables = array();
            $reserved_variants_name = array();
            $reserved_variants_save_array = array();
            $reserved_save_variables_del = array();
            $update_araray_updated=
            $insertvariants = array();
            if(!empty($product_variants)) {
                foreach ($product_variants as $key => $value) {
                    $variant = $this->VariantSavedGetByNamepId($value->name,$id);
                    $variant_selected = $this->getReserveVariant($variant, $value->product_id);
                    if ($variant_selected) {
                        $reserved_variants_name[$key]['name'] = $this->VariantSavedGetNameByID($variant_selected);
                        $reserved_variants_name[$key]['id'] = $variant;
                        $reserved_variants[] = $variant_selected;

                    } else {
                        $reserved_variables_name[$key]['name'] = $this->VariantSavedGetNameByID($variant);
                        $reserved_variables_name[$key]['id'] = $variant;
                        $reserved_save_variables[]= $variant;
                    }
                }
            }

            if(!empty($reserved_variants_name)){
                foreach ($reserved_variants_name as $key => $variant_name){
                    foreach ($product_attributes as $ky => $attribute){
                        if($attribute['name'] == $variant_name['name']){
                            $this->db->update('product_variants', $product_attributes[$ky], array('id' => $variant_name['id']));
                            unset($product_attributes[$ky]);
                        }
                    }

                }
            }

            if(!empty($reserved_variables_name)){
                foreach ($reserved_variables_name as $key => $variant_name){
                    foreach ($product_attributes as $ky => $attribute){
                        if($attribute['name'] == $variant_name['name']){
                            $reserved_variants_save_array[]=$product_attributes[$ky];
                            $this->db->update('product_variants', $product_attributes[$ky], array('id' => $variant_name['id']));
                            $reserved_save_variables_del[]=$variant_name['id'];
                            unset($product_attributes[$ky]);
                        }
                    }

                }
            }

            // $this->db->update('adjustments', $data, array('id' => $id)
            if(!empty($reserved_variants)){
                $this->db->where_not_in('id', $reserved_variants);
                if(!empty($reserved_save_variables_del)) {
                    $this->db->where_not_in('id', $reserved_save_variables_del);
                }

                $this->db->where('product_id', $id);
                $this->db->delete('product_variants');

            }else if(!empty($reserved_save_variables)){
                if(!empty($reserved_save_variables_del)) {

                    $this->db->where_not_in('id', $reserved_save_variables_del);
                }
                $this->db->where('product_id', $id);
                $this->db->delete('product_variants');
                //$this->db->delete('product_variants', array('product_id' => $id));
            }


            if ($product_attributes) {

                foreach ($product_attributes as $pr_attr) {

                    $pr_attr['product_id'] = $id;
                    $variant_warehouse_id = $pr_attr['warehouse_id'];
                    unset($pr_attr['warehouse_id']);
                    //before addition new variantrs remove old ones

                    $this->db->insert('product_variants', $pr_attr);
                    $option_id = $this->db->insert_id();

                    if ($pr_attr['quantity'] != 0) {
                        $this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $id, 'warehouse_id' => $variant_warehouse_id, 'quantity' => $pr_attr['quantity']));

                        $tax_rate_id = $tax_rate ? $tax_rate->id : NULL;
                        $tax = $tax_rate ? (($tax_rate->type == 1) ? $tax_rate->rate . "%" : $tax_rate->rate) : NULL;
                        $unit_cost = $data['cost'];
                        if ($tax_rate) {
                            if ($tax_rate->type == 1 && $tax_rate->rate != 0) {
                                if ($data['tax_method'] == '0') {
                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / (100 + $tax_rate->rate);
                                    $net_item_cost = $data['cost'] - $pr_tax_val;
                                    $item_tax = $pr_tax_val * $pr_attr['quantity'];
                                } else {
                                    $net_item_cost = $data['cost'];
                                    $pr_tax_val = ($data['cost'] * $tax_rate->rate) / 100;
                                    $unit_cost = $data['cost'] + $pr_tax_val;
                                    $item_tax = $pr_tax_val * $pr_attr['quantity'];
                                }
                            } else {
                                $net_item_cost = $data['cost'];
                                $item_tax = $tax_rate->rate;
                            }
                        } else {
                            $net_item_cost = $data['cost'];
                            $item_tax = 0;
                        }

                        $subtotal = (($net_item_cost * $pr_attr['quantity']) + $item_tax);
                        $item = array(
                            'product_id' => $id,
                            'product_code' => $data['code'],
                            'product_name' => $data['name'],
                            'net_unit_cost' => $net_item_cost,
                            'unit_cost' => $unit_cost,
                            'quantity' => $pr_attr['quantity'],
                            'option_id' => $option_id,
                            'quantity_balance' => $pr_attr['quantity'],
                            'quantity_received' => $pr_attr['quantity'],
                            'item_tax' => $item_tax,
                            'tax_rate_id' => $tax_rate_id,
                            'tax' => $tax,
                            'subtotal' => $subtotal,
                            'warehouse_id' => $variant_warehouse_id,
                            'date' => date('Y-m-d'),
                            'status' => 'received',
                        );
                        $item['option_id'] = !empty($item['option_id']) && is_numeric($item['option_id']) ? $item['option_id'] : NULL;
                        $this->db->insert('purchase_items', $item);

                    }
                }
            }
            $length = count($variants);
            $this->db->delete('variants_combination', array('product_id' => $id));
            for ($i = 0; $i < $length; $i++) {
                if($attributesInput2[$i] !='') {
                    $item_variant = array(
                        'variant' => $variants[$i],
                        'variant_value' => $attributesInput2[$i],
                        'additional_guide' => $additional_guide[$i],
                        'product_id' => $id
                    );
                    $this->db->insert('variants_combination', $item_variant);
                }
            }

            $this->site->syncQuantity(NULL, NULL, NULL, $id);
            $this->db->delete('product_photos', array('product_id' => $id));
            $length1 = count($gallery_images);
            for ($j = 0; $j < $length1; $j++) {
                if($gallery_images[$j] !='') {
                    $item_gallery = array(
                        'photo' => $gallery_images[$j],
                        'alt_text'=>$gallery_images_alt[$j],
                        'product_id' => $id
                    );
                    $this->db->insert('product_photos', $item_gallery);
                }
            }

            return true;
        } else {
            return false;
        }
    }

    public function updateVariableImage($product_id,$variable,$photo_variable,$photo_alt){
        $this->db->delete('variants_combination_images', array('product_id' => $product_id,'variable' => $variable));
        $item_variant = array(
            'product_id' => $product_id,
            'variable' => $variable,
            'image' => $photo_variable,
            'image_alt' => $photo_alt,
        );
        $this->db->insert('variants_combination_images', $item_variant);

    }
    public function RemoveVariableImage($product_id,$variable){
        $this->db->delete('variants_combination_images', array('product_id' => $product_id,'variable' => $variable));
    }
    public function getVariableImages($pid)
    {
        $q = $this->db->get_where('variants_combination_images', array('product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            //$data[] = $q->result_array();
            return $data;
        }
        return FALSE;
    }


    public function updateProductName($pro_id,$pro_name){
        $this->db->set('name', $pro_name);
        $this->db->where('id', $pro_id);
        $this->db->update('products');
        return true;
    }
    public function updateDelFrontImage($pro_id){
        $this->db->set('image', 'no_image.png');
        $this->db->where('id', $pro_id);
        $this->db->update('products');
        return true;
    }
    public function updateDelBackImage($pro_id){
        $this->db->set('back_image', 'no_image.png');
        $this->db->where('id', $pro_id);
        $this->db->update('products');
        return true;
    }

    public function updateProductOptionQuantity($option_id, $warehouse_id, $quantity, $product_id)
    {
        if ($option = $this->getProductWarehouseOptionQty($option_id, $warehouse_id)) {
            if ($this->db->update('warehouses_products_variants', array('quantity' => $quantity), array('option_id' => $option_id, 'warehouse_id' => $warehouse_id))) {
                $this->site->syncVariantQty($option_id, $warehouse_id);
                return TRUE;
            }
        } else {
            if ($this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $quantity))) {
                $this->site->syncVariantQty($option_id, $warehouse_id);
                return TRUE;
            }
        }
        return FALSE;
    }

    public function updatePrice($data = array())
    {
        if ($this->db->update_batch('products', $data, 'code')) {
            return true;
        }
        return false;
    }

    public function deleteProduct($id)
    {
        if ($this->db->delete('products', array('id' => $id)) && $this->db->delete('warehouses_products', array('product_id' => $id))) {
            $this->db->delete('warehouses_products_variants', array('product_id' => $id));
            $this->db->delete('product_variants', array('product_id' => $id));
            $this->db->delete('product_photos', array('product_id' => $id));
            $this->db->delete('product_prices', array('product_id' => $id));
            return true;
        }
        return FALSE;
    }


    public function totalCategoryProducts($category_id)
    {
        $q = $this->db->get_where('products', array('category_id' => $category_id));
        return $q->num_rows();
    }


    public function getTaxRateByName($name)
    {
        $q = $this->db->get_where('tax_rates', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAdjustmentByID($id)
    {
        $q = $this->db->get_where('adjustments', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAdjustmentItems($adjustment_id)
    {
        $this->db->select('adjustment_items.*, products.code as product_code, products.name as product_name, products.image, products.details as details, product_variants.name as variant')
            ->join('products', 'products.id=adjustment_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=adjustment_items.option_id', 'left')
            ->group_by('adjustment_items.id')
            ->order_by('id', 'asc');

        $this->db->where('adjustment_id', $adjustment_id);

        $q = $this->db->get('adjustment_items');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function syncAdjustment($data = array())
    {
        if(! empty($data)) {
            $clause = array('product_id' => $data['product_id'], 'option_id' => $data['option_id'], 'warehouse_id' => $data['warehouse_id'], 'status' => 'received');
            $qty = $data['type'] == 'subtraction' ? 0 - $data['quantity'] : 0 + $data['quantity'];
            $this->site->setPurchaseItem($clause, $qty);

            $this->site->syncProductQty($data['product_id'], $data['warehouse_id']);
            if ($data['option_id']) {
                $this->site->syncVariantQty($data['option_id'], $data['warehouse_id'], $data['product_id']);
            }
        }
    }

    public function reverseAdjustment($id)
    {
        if ($products = $this->getAdjustmentItems($id)) {
            foreach ($products as $adjustment) {
                $clause = array('product_id' => $adjustment->product_id, 'warehouse_id' => $adjustment->warehouse_id, 'option_id' => $adjustment->option_id, 'status' => 'received');
                $qty = $adjustment->type == 'subtraction' ? (0+$adjustment->quantity) : (0-$adjustment->quantity);
                $this->site->setPurchaseItem($clause, $qty);
                $this->site->syncProductQty($adjustment->product_id, $adjustment->warehouse_id);
                if ($adjustment->option_id) {
                    $this->site->syncVariantQty($adjustment->option_id, $adjustment->warehouse_id, $adjustment->product_id);
                }
            }
        }
    }

    public function addAdjustment($data, $products)
    {
        if ($this->db->insert('adjustments', $data)) {
            $adjustment_id = $this->db->insert_id();
            foreach ($products as $product) {
                $product['adjustment_id'] = $adjustment_id;
                $this->db->insert('adjustment_items', $product);
                $this->syncAdjustment($product);
            }
            if ($this->site->getReference('qa') == $data['reference_no']) {
                $this->site->updateReference('qa');
            }
            return true;
        }
        return false;
    }

    public function updateAdjustment($id, $data, $products)
    {
        $this->reverseAdjustment($id);
        if ($this->db->update('adjustments', $data, array('id' => $id)) &&
            $this->db->delete('adjustment_items', array('adjustment_id' => $id))) {
            foreach ($products as $product) {
                $product['adjustment_id'] = $id;
                $this->db->insert('adjustment_items', $product);
                $this->syncAdjustment($product);
            }
            return true;
        }
        return false;
    }

    public function deleteAdjustment($id)
    {
        $this->reverseAdjustment($id);
        if ( $this->db->delete('adjustments', array('id' => $id)) &&
            $this->db->delete('adjustment_items', array('adjustment_id' => $id))) {
            return true;
        }
        return false;
    }

    public function getProductQuantity($product_id, $warehouse)
    {
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse), 1);
        if ($q->num_rows() > 0) {
            return $q->row_array();
        }
        return FALSE;
    }

    public function addQuantity($product_id, $warehouse_id, $quantity, $rack = NULL)
    {

        if ($this->getProductQuantity($product_id, $warehouse_id)) {
            if ($this->updateQuantity($product_id, $warehouse_id, $quantity, $rack)) {
                return TRUE;
            }
        } else {
            if ($this->insertQuantity($product_id, $warehouse_id, $quantity, $rack)) {
                return TRUE;
            }
        }

        return FALSE;
    }

    public function insertQuantity($product_id, $warehouse_id, $quantity, $rack = NULL)
    {
        $product = $this->site->getProductByID($product_id);
        if ($this->db->insert('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $quantity, 'rack' => $rack, 'avg_cost' => $product->cost))) {
            $this->site->syncProductQty($product_id, $warehouse_id);
            return true;
        }
        return false;
    }

    public function updateQuantity($product_id, $warehouse_id, $quantity, $rack = NULL)
    {
        $data = $rack ? array('quantity' => $quantity, 'rack' => $rack) : $data = array('quantity' => $quantity);
        if ($this->db->update('warehouses_products', $data, array('product_id' => $product_id, 'warehouse_id' => $warehouse_id))) {
            $this->site->syncProductQty($product_id, $warehouse_id);
            return true;
        }
        return false;
    }

    public function products_count($category_id, $subcategory_id = NULL)
    {
        if ($category_id) {
            $this->db->where('category_id', $category_id);
        }
        if ($subcategory_id) {
            $this->db->where('subcategory_id', $subcategory_id);
        }
        $this->db->from('products');
        return $this->db->count_all_results();
    }

    public function fetch_products($category_id, $limit, $start, $subcategory_id = NULL)
    {

        $this->db->limit($limit, $start);
        if ($category_id) {
            $this->db->where('category_id', $category_id);
        }
        if ($subcategory_id) {
            $this->db->where('subcategory_id', $subcategory_id);
        }
        $this->db->order_by("id", "asc");
        $query = $this->db->get("products");

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getProductWarehouseOptionQty($option_id, $warehouse_id)
    {
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function syncVariantQty($option_id)
    {
        $wh_pr_vars = $this->getProductWarehouseOptions($option_id);
        $qty = 0;
        foreach ($wh_pr_vars as $row) {
            $qty += $row->quantity;
        }
        if ($this->db->update('product_variants', array('quantity' => $qty), array('id' => $option_id))) {
            return TRUE;
        }
        return FALSE;
    }

    public function getProductWarehouseOptions($option_id)
    {
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function setRack($data)
    {
        if ($this->db->update('warehouses_products', array('rack' => $data['rack']), array('product_id' => $data['product_id'], 'warehouse_id' => $data['warehouse_id']))) {
            return TRUE;
        }
        return FALSE;
    }

    public function getSoldQty($id)
    {
        $this->db->select("date_format(" . $this->db->dbprefix('sales') . ".date, '%Y-%M') month, SUM( " . $this->db->dbprefix('sale_items') . ".quantity ) as sold, SUM( " . $this->db->dbprefix('sale_items') . ".subtotal ) as amount")
            ->from('sales')
            ->join('sale_items', 'sales.id=sale_items.sale_id', 'left')
            ->group_by("date_format(" . $this->db->dbprefix('sales') . ".date, '%Y-%m')")
            ->where($this->db->dbprefix('sale_items') . '.product_id', $id)
            //->where('DATE(NOW()) - INTERVAL 1 MONTH')
            ->where('DATE_ADD(curdate(), INTERVAL 1 MONTH)')
            ->order_by("date_format(" . $this->db->dbprefix('sales') . ".date, '%Y-%m') desc")->limit(3);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getPurchasedQty($id)
    {
        $this->db->select("date_format(" . $this->db->dbprefix('purchases') . ".date, '%Y-%M') month, SUM( " . $this->db->dbprefix('purchase_items') . ".quantity ) as purchased, SUM( " . $this->db->dbprefix('purchase_items') . ".subtotal ) as amount")
            ->from('purchases')
            ->join('purchase_items', 'purchases.id=purchase_items.purchase_id', 'left')
            ->group_by("date_format(" . $this->db->dbprefix('purchases') . ".date, '%Y-%m')")
            ->where($this->db->dbprefix('purchase_items') . '.product_id', $id)
            //->where('DATE(NOW()) - INTERVAL 1 MONTH')
            ->where('DATE_ADD(curdate(), INTERVAL 1 MONTH)')
            ->order_by("date_format(" . $this->db->dbprefix('purchases') . ".date, '%Y-%m') desc")->limit(3);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getWarehouseProductVariant($warehouse_id, $product_id, $option_id = NULL)
    {
        $q = $this->db->get_where('warehouses_products_variants', array('product_id' => $product_id, 'option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPurchaseItems($purchase_id)
    {
        $q = $this->db->get_where('purchase_items', array('purchase_id' => $purchase_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getTransferItems($transfer_id)
    {
        $q = $this->db->get_where('purchase_items', array('transfer_id' => $transfer_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getUnitByCode($code)
    {
        $q = $this->db->get_where("units", array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }



    public function getStockCountProducts($warehouse_id, $type, $categories = NULL, $brands = NULL)
    {
        $this->db->select("{$this->db->dbprefix('products')}.id as id, {$this->db->dbprefix('products')}.code as code, {$this->db->dbprefix('products')}.name as name, {$this->db->dbprefix('warehouses_products')}.quantity as quantity")
            ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            ->where('warehouses_products.warehouse_id', $warehouse_id)
            ->where('products.type', 'standard')
            ->order_by('products.code', 'asc');
        if ($categories) {
            $r = 1;
            $this->db->group_start();
            foreach ($categories as $category) {
                if ($r == 1) {
                    $this->db->where('products.category_id', $category);
                } else {
                    $this->db->or_where('products.category_id', $category);
                }
                $r++;
            }
            $this->db->group_end();
        }
        if ($brands) {
            $r = 1;
            $this->db->group_start();
            foreach ($brands as $brand) {
                if ($r == 1) {
                    $this->db->where('products.brand', $brand);
                } else {
                    $this->db->or_where('products.brand', $brand);
                }
                $r++;
            }
            $this->db->group_end();
        }

        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getStockCountProductVariants($warehouse_id, $product_id)
    {
        $this->db->select("{$this->db->dbprefix('product_variants')}.name, {$this->db->dbprefix('warehouses_products_variants')}.quantity as quantity")
            ->join('warehouses_products_variants', 'warehouses_products_variants.option_id=product_variants.id', 'left');
        $q = $this->db->get_where('product_variants', array('product_variants.product_id' => $product_id, 'warehouses_products_variants.warehouse_id' => $warehouse_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function addStockCount($data)
    {
        if ($this->db->insert('stock_counts', $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function finalizeStockCount($id, $data, $products)
    {
        if ($this->db->update('stock_counts', $data, array('id' => $id))) {
            foreach ($products as $product) {
                $this->db->insert('stock_count_items', $product);
            }
            return TRUE;
        }
        return FALSE;
    }

    public function getStouckCountByID($id)
    {
        $q = $this->db->get_where("stock_counts", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getStockCountItems($stock_count_id)
    {
        $q = $this->db->get_where("stock_count_items", array('stock_count_id' => $stock_count_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return NULL;
    }

    public function getAdjustmentByCountID($count_id)
    {
        $q = $this->db->get_where('adjustments', array('count_id' => $count_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductVariantID($product_id, $name)
    {
        $q = $this->db->get_where("product_variants", array('product_id' => $product_id, 'name' => $name), 1);
        if ($q->num_rows() > 0) {
            $variant = $q->row();
            return $variant->id;
        }
        return NULL;
    }


    //Category Functions
    public function getCategoryByID($id)
    {
        $q = $this->db->get_where("categories", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }


    public function getParentCategories()
    {
        $this->db->where('parent_id', NULL)->or_where('parent_id', 0);
        $q = $this->db->get("categories");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCategoryByCode($code)
    {
        $q = $this->db->get_where('categories', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addCategory($data)
    {
        if ($this->db->insert("categories", $data)) {
            return true;
        }
        return false;
    }

    public function addCategories($data)
    {
        if ($this->db->insert_batch('categories', $data)) {
            return true;
        }
        return false;
    }



    public function updateCategory($id, $data = array())
    {
        $this->db->where('id', $id);
        $q = $this->db->get("categories");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $old_record = $row;
            }
        }

        if ($this->db->update("categories", $data, array('id' => $id))) {
            //To update the products table category and subcategory_id according to the Case.
            if($old_record->parent_id == 0 && $data['parent_id'] != ''){
                $pro_record=array('category_id'=>$data['parent_id'] , 'subcategory_id'=>$id);
                $this->db->where('category_id',$id);
                $this->db->update("products", $pro_record);
            }else if($old_record->parent_id != 0 && $data['parent_id'] != $old_record->parent_id && $data['parent_id']){
                $pro_record=array('category_id'=>$data['parent_id']);
                $this->db->where('subcategory_id',$id);
                $this->db->update("products", $pro_record);
            }else if($old_record->parent_id != 0 && $data['parent_id'] == ''){
                $pro_record=array('category_id'=>$id , 'subcategory_id'=>NULL);
                $this->db->where('subcategory_id',$id);
                $this->db->update("products", $pro_record);
            }
            return true;
        }
        return false;
    }

    public function deleteCategory($id)
    {
        if ($this->db->delete("categories", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }


    public function updateUnit($id, $data = array())
    {
        if ($this->db->update("units", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function addUnit($data)
    {
        if ($this->db->insert("units", $data)) {
            return true;
        }
        return false;
    }

    public function getUnitChildren($base_unit)
    {
        $this->db->where('base_unit', $base_unit);
        $q = $this->db->get("units");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function deleteUnit($id)
    {
        if ($this->db->delete("units", array('id' => $id))) {
            $this->db->delete("units", array('base_unit' => $id));
            return true;
        }
        return FALSE;
    }



    public function getBrandByName($name)
    {
        $q = $this->db->get_where('brands', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addBrand($data)
    {
        if ($this->db->insert("brands", $data)) {
            return true;
        }
        return false;
    }

    public function addBrands($data)
    {
        if ($this->db->insert_batch('brands', $data)) {
            return true;
        }
        return false;
    }

    public function updateBrand($id, $data = array())
    {
        if ($this->db->update("brands", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function brandHasProducts($brand_id)
    {
        $q = $this->db->get_where('products', array('brand' => $brand_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteBrand($id)
    {
        if ($this->db->delete("brands", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }



    public function getAuthorByName($name)
    {
        $q = $this->db->get_where('authors', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAuthorByCode($code)
    {
        $q = $this->db->get_where('authors', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addAuthor($data)
    {
        if ($this->db->insert("authors", $data)) {
            return true;
        }
        return false;
    }

    public function addAuthors($data)
    {
        if ($this->db->insert_batch('authors', $data)) {
            return true;
        }
        return false;
    }

    public function updateAuthor($id, $data = array())
    {
        if ($this->db->update("authors", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function authorHasProducts($author_id)
    {
        $q = $this->db->get_where('products', array('author' => $author_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteAuthor($id)
    {
        if ($this->db->delete("authors", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }



    public function getPublisherByName($name)
    {
        $q = $this->db->get_where('publishers', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPublisherByCode($code)
    {
        $q = $this->db->get_where('publishers', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addPublisher($data)
    {
        if ($this->db->insert("publishers", $data)) {
            return true;
        }
        return false;
    }

    public function addPublishers($data)
    {
        if ($this->db->insert_batch('publishers', $data)) {
            return true;
        }
        return false;
    }

    public function updatePublisher($id, $data = array())
    {
        if ($this->db->update("publishers", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function publisherHasProducts($author_id)
    {
        $q = $this->db->get_where('products', array('publisher' => $author_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deletePublisher($id)
    {
        if ($this->db->delete("publishers", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }


    public function addVariantGroup($data)
    {
        if ($this->db->insert('variants_group', $data)) {
            return true;
        }
        return false;
    }

    public function checkVariantGroupDuplication($variant_id,$variants_group_name)
    {
        $this->db->select('name');
        $this->db->where('name',$variants_group_name );
        $this->db->where('variant_id',$variant_id );
        $q = $this->db->get('variants_group');
        if ($q->num_rows() > 0) {
            return  true;
        }
        return FALSE;
    }



    public function addVariant($data)
    {
        if ($this->db->insert('variants', $data)) {
            return true;
        }
        return false;
    }

    public function updateVariant($id, $data = array())
    {
        $this->db->where('id', $id);
        if ($this->db->update('variants', $data)) {
            return true;
        }
        return false;
    }

    public function updateVariant_group($id, $data = array())
    {
        $this->db->where('id', $id);
        if ($this->db->update('variants_group', $data)) {
            return true;
        }
        return false;
    }

    public function getAllVariants()
    {
        $q = $this->db->get('variants');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getVariantByID($id)
    {
        $q = $this->db->get_where('variants', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function VariantIdGetByName($name)
    {
        $q = $this->db->get_where('variants', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row()->id;
        }
        return FALSE;
    }

    public function VariantSavedGetByName($name)
    {
        $q = $this->db->get_where('product_variants', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row()->id;
        }
        return FALSE;
    }
    public function VariantSavedGetByNamepId($name,$product_id)
    {
        $q = $this->db->get_where('product_variants', array('name' => $name,'product_id'=>$product_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row()->id;
        }
        return FALSE;
    }
    public function VariantSavedGetNameByID($id)
    {
        $q = $this->db->get_where('product_variants', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row()->name;
        }
        return FALSE;
    }
    public function VariantSavedNameGetByName($name)
    {
        $q = $this->db->get_where('product_variants', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row()->name;
        }
        return FALSE;
    }

    public function getReserveVariant($option_id,$product_id)
    {
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id,'product_id' => $product_id), 1);
        if ($q->num_rows() > 0) {
            return $option_id;
        }
        return FALSE;
    }


    public function VariantValuesByID($id)
    {
        $q = $this->db->get_where('variants_group', array('variant_id' => $id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function VariantValuesByIDSelect($id,$pid)
    {

        $product_variants =  $this->getProductOptions($pid);
        $reserved_variants = array();
        $data_array = array();
        $saved_variableofcombinations = array();
        foreach ($product_variants as $key => $value){
            $variant = $this->VariantSavedGetByNamePid($value->name,$value->product_id);
            $variant_selected = $this->getReserveVariant($variant,$value->product_id);
            if($variant_selected){
                $reserved_variants[]= $variant_selected;
                $product_variants[$key]->del_check = 0;
                $saved_comb_values= explode(' / ',$value->name);
                foreach ($saved_comb_values as $val){
                    $saved_variableofcombinations[] = $val;
                }
            }else{
                $product_variants[$key]->del_check = 1;
            }

        }

        $q = $this->db->get_where('variants_group', array('variant_id' => $id));
        $disabled=0;
        if ($q->num_rows() > 0) {

            foreach (($q->result()) as $key => $row) {
                if (in_array($row->name, $saved_variableofcombinations))
                {
                    $data[$key]['id'] = $row->name;
                    $data[$key]['text'] = $row->name;
                    $data[$key]['locked'] = true;
                    $disabled=1;
                }else{
                $data[$key]['id'] = $row->name;
                $data[$key]['text'] = $row->name;
            }
            }
            $data_array['data']=$data;
            $data_array['disable']=$disabled;
            return $data_array;
        }
        return FALSE;
    }
    public function VariantValuesByIDSelect_add($id)
    {

        $q = $this->db->get_where('variants_group', array('variant_id' => $id));
        if ($q->num_rows() > 0) {

            foreach (($q->result()) as $key => $row) {

                    $data[$key]['id'] = $row->name;
                    $data[$key]['text'] = $row->name;
            }
            return $data;
        }
        return FALSE;
    }

    public function getVariantGroupByID($id)
    {
        $q = $this->db->get_where('variants_group', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteVariant($id)
    {
        if ($this->db->delete('variants', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteVariantGroup($id)
    {
        if ($this->db->delete('variants_group', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }


    public function getPromotions()
    {
        $this->db->where('status', 'active');
        $this->db->where('endDate >=', date('Y-m-d'));
        $q = $this->db->get('promotions');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    public function addPromotions($data,$products, $free_category_id = NULL ,  $warehous_id = NULL ,   $products_for_items = NULL)
    {
        // print_r($products); die;
        if ($this->db->insert("promotions", $data)) {
            $prom_id = $this->db->insert_id();
            if(!empty($products) && empty($free_category_id)){
                foreach ($warehous_id as $shop) {
                    foreach ($products as $product) {
                        $this->db->insert('promotion_items', array('upselling_id' => $prom_id, 'item_id' => $product , 'warehouse_id'=>$shop));
                    }
                }
            }else{
                //Insert the category
                foreach ($warehous_id as $shop){
                    foreach($free_category_id as $cat){
                        $this->db->insert('promotion_items', array('upselling_id' => $prom_id, 'item_id' => 0 , 'category_id'=>$cat , 'warehouse_id'=>$shop));
                    }
                }
            }
            if(!empty($products_for_items)){
                foreach ($products_for_items as $products_for_item){
                    $this->db->insert('promotion_for_items', array('upselling_id' => $prom_id, 'item_id' => $products_for_item));
                }
            }
            return true;
        }
        return false;
    }


    public function deleteUpselling($id)
    {
        if ($this->db->update('promotions', array('status' => 'inactive'),array('id' => $id) )) {
            return true;
        }
        return FALSE;
    }

    public function editUpselling($id)
    {
        $this->db->select('promotions.*, promotion_items.*')
            ->from('promotions')
            ->join('promotion_items', 'promotions.id = promotion_items.upselling_id','left')
            ->where('promotions.status', 'active')
            ->where('promotions.id', $id)
            ->order_by('promotions.id desc');
        $query = $this->db->get();
        // $q = $this->db->get_where("promotions", array('id' => $id , 'status'=>'active'), 1);
        if ($query->num_rows() > 0) {
            return $query->row();
        }
        return FALSE;
    }

    public function delete_updated_promotion($id)
    {
        $this->db->delete('promotions', array('id' => $id));
        $this->db->delete('promotion_items', array('upselling_id' => $id));
        $this->db->delete('promotion_for_items', array('upselling_id' => $id));

        return true;
    }

    public function findCategoryAlreadyOnPromotion(){
        $this->db->distinct('sma_promotion_items.category_id');
        $this->db->select('sma_promotion_items.category_id')
            ->from('promotions')
            ->join('promotion_items', 'promotions.id = promotion_items.upselling_id')
            ->where('promotions.endDate >=', date('Y-m-d'))
            ->where('promotions.status !=','inactive');
            //->order_by('promotions.id desc');
        $query = $this->db->get();
        //echo $this->db->get_compiled_select(); die;
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row->category_id;
            }
            return $data;
        }
        return FALSE;
    }


    //Sizing guide moudele
    public function addSizingGuide($data){
        if ($this->db->insert("sizing_info", $data)) {
            return true;
        }
        return false;
    }

    public function getAllSizeGuide(){
        $this->db->select('id,title');
        $q = $this->db->get('sizing_info');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function editSizingGuide($id)
    {
        $q = $this->db->get_where("sizing_info", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateSizingGuide($data, $id){
        if($this->db->update('sizing_info', $data, array('id' => $id))){
            return true;
        }
        return false;
    }

    public function deleteSizingGuide($id)
    {
        if ($this->db->delete('sizing_info',array('id' => $id) )) {
            return true;
        }
        return FALSE;
    }

    function slugify() {
        if ($products = $this->getAllProducts()) {
            $this->db->update('products', ['slug' => NULL]);
            foreach ($products as $product) {
                $slug = $this->sma->slug($product->name);
                $this->db->update('products', ['slug' => $slug], ['id' => $product->id]);
            }
            return true;
        }
        return true;
    }
    public function getAllSavedVariants_selected($pid)
    {
        $q = $this->db->get_where('variants_combination', array('product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $saved_comb_values= explode(' / ',$row->variant_value);
                foreach ($saved_comb_values as $val){
                    $data[] = $val;
                }
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
    public function getAllSavedVariants($pid)
    {
        $q = $this->db->get_where('variants_combination', array('product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result_array()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllAdditionalGuide(){
        $q = $this->db->get('sizing_info');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }



    public function addPaymentMethod($data)
    {
        if ($this->db->insert("payment_methods", $data)) {
            return true;
        }
        return false;
    }
    public function getPaymentMethodByID($id)
    {
        $q = $this->db->get_where("payment_methods", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }


    public function updatePaymentMethod($id, $data = array()){
        if ($this->db->update("payment_methods", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function deletPaymentMethod($id)
    {
        if ($this->db->delete("payment_methods", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }




    public function addShipmentMethod($data)
    {
        if ($this->db->insert("shipment_methods", $data)) {
            return true;
        }
        return false;
    }
    public function getShipmentMethodByID($id)
    {
        $q = $this->db->get_where("shipment_methods", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateShipmentMethod($id, $data = array()){
        if ($this->db->update("shipment_methods", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function deletShipmentMethod($id)
    {
        if ($this->db->delete("shipment_methods", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }
}
