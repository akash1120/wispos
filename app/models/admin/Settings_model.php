<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function updateLogo($photo)
    {
        $logo = array('logo' => $photo);
        if ($this->db->update('settings', $logo)) {
            return true;
        }
        return false;
    }

    public function updateLoginLogo($photo)
    {
        $logo = array('logo2' => $photo);
        if ($this->db->update('settings', $logo)) {
            return true;
        }
        return false;
    }

    public function getSettings()
    {
        $q = $this->db->get('settings');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getDateFormats()
    {
        $q = $this->db->get('date_format');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function updateSetting($data, $payment_methods=NULL)
    {
        //Paypal = 1
        //Skrill = 2
        //COD = 3

        $this->db->where('setting_id', '1');
        if ($this->db->update('settings', $data)) {
            if($payment_methods){
                foreach ($payment_methods as $payment_method){
                    if($payment_method == 1){
                        $this->db->where('id' , $payment_method);
                        $this->db->update('paypal', array('active'=>'1'));
                    }elseif ($payment_method == 2){
                        $this->db->where('id' , $payment_method);
                        $this->db->update('skrill', array('active'=>'1'));
                    }elseif ($payment_method == 3){
                        $this->db->where('id' , $payment_method);
                        $this->db->update('cod', array('active'=>'1'));
                    }
                }

                //if paypal need to deactive
                if(!in_array(1, $payment_methods)){
                    $this->db->where('id' , 1);
                    $this->db->update('paypal', array('active'=>'0'));
                }
                //if skrill need to deactive
                if(!in_array(2, $payment_methods)){
                    $this->db->where('id' , 2);
                    $this->db->update('skrill', array('active'=>'0'));
                }
                //if COD need to deactive
                if(!in_array(3, $payment_methods)){
                    $this->db->where('id' , 3);
                    $this->db->update('cod', array('active'=>'0'));
                }
            }else{
                    //If need to deactive all the payment methods
                    //Deactive PayPal
                    $this->db->where('id', 1);
                    $this->db->update('paypal', array('active'=>'0'));
                    //Deacive Skrill
                    $this->db->where('id', 2);
                    $this->db->update('skrill', array('active'=>'0'));
                    //Deactive COD
                    $this->db->where('id', 3);
                    $this->db->update('cod', array('active'=>'0'));
            }
            return true;
        }
        return false;
    }

    public function addTaxRate($data)
    {
        if ($this->db->insert('tax_rates', $data)) {
            return true;
        }
        return false;
    }

    public function updateTaxRate($id, $data = array())
    {
        $this->db->where('id', $id);
        if ($this->db->update('tax_rates', $data)) {
            return true;
        }
        return false;
    }

    public function getAllTaxRates()
    {
        $q = $this->db->get('tax_rates');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getTaxRateByID($id)
    {
        $q = $this->db->get_where('tax_rates', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getWarehouseByID($id)
    {
        $q = $this->db->get_where('warehouses', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteTaxRate($id)
    {
        if ($this->db->delete('tax_rates', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteInvoiceType($id)
    {
        if ($this->db->delete('invoice_types', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteWarehouse($id)
    {
        if ($this->db->delete('warehouses', array('id' => $id)) && $this->db->delete('warehouses_products', array('warehouse_id' => $id))) {
            return true;
        }
        return FALSE;
    }
    public function getAllWarehouses()
    {
        $q = $this->db->get('warehouses');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function addCustomerGroup($data)
    {
        if ($this->db->insert('customer_groups', $data)) {
            return true;
        }
        return false;
    }

    public function updateCustomerGroup($id, $data = array())
    {
        $this->db->where('id', $id);
        if ($this->db->update('customer_groups', $data)) {
            return true;
        }
        return false;
    }

    public function getAllCustomerGroups()
    {
        $q = $this->db->get('customer_groups');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCustomerGroupByID($id)
    {
        $q = $this->db->get_where('customer_groups', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteCustomerGroup($id)
    {
        if ($this->db->delete('customer_groups', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getGroups()
    {
        $this->db->where('id >', 4);
        $q = $this->db->get('groups');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getGroupByID($id)
    {
        $q = $this->db->get_where('groups', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getGroupPermissions($id)
    {
        $q = $this->db->get_where('permissions', array('group_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function GroupPermissions($id)
    {
        $q = $this->db->get_where('permissions', array('group_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->result_array();
        }
        return FALSE;
    }

    public function updatePermissions($id, $data = array())
    {
        if ($this->db->update('permissions', $data, array('group_id' => $id)) && $this->db->update('users', array('show_price' => $data['products-price'], 'show_cost' => $data['products-cost']), array('group_id' => $id))) {
            return true;
        }
        return false;
    }

    public function addGroup($data)
    {
        if ($this->db->insert("groups", $data)) {
            $gid = $this->db->insert_id();
            $this->db->insert('permissions', array('group_id' => $gid));
            return $gid;
        }
        return false;
    }

    public function updateGroup($id, $data = array())
    {
        $this->db->where('id', $id);
        if ($this->db->update("groups", $data)) {
            return true;
        }
        return false;
    }


    public function getAllCurrencies()
    {
        $q = $this->db->get('currencies');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCurrencyByID($id)
    {
        $q = $this->db->get_where('currencies', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }


    public function addCurrency($data)
    {
        if ($this->db->insert("currencies", $data)) {
            return true;
        }
        return false;
    }
    public function getCountryByID($id)
    {
        $q = $this->db->get_where('country', array('country_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
    public function addCountry($data)
    {

        if ($this->db->insert("country", $data)) {
            return true;
        }
        return false;
    }
    public function updateCountry($id, $data = array())
    {
        $this->db->where('country_id', $id);
        if ($this->db->update("country", $data)) {
            return true;
        }
        return false;
    }
    public function deleteCountry($id)
    {
        if ($this->db->delete("country", array('country_id' => $id))) {
            return true;
        }
        return FALSE;
    }


    public function addArea($data)
    {

        if ($this->db->insert("zone", $data)) {
            return true;
        }
        return false;
    }
    public function getAreaByID($id)
    {
        $q = $this->db->get_where('zone', array('zone_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
    public function updateArea($id, $data = array())
    {
        $this->db->where('zone_id', $id);
        if ($this->db->update("zone", $data)) {
            return true;
        }
        return false;
    }
    public function deleteArea($id)
    {
        if ($this->db->delete("zone", array('zone_id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function addSlider($data)
    {

        if ($this->db->insert("sliders", $data)) {
            return true;
        }
        return false;
    }
    public function getSliderByID($id)
    {
        $q = $this->db->get_where('sliders', array('slider_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
    public function updateSlider($id, $data = array())
    {
        $this->db->where('slider_id', $id);
        if ($this->db->update("sliders", $data)) {
            return true;
        }
        return false;
    }
    public function deleteSlider($id)
    {
        if ($this->db->delete("sliders", array('slider_id' => $id))) {
            return true;
        }
        return FALSE;
    }
    public function updateSliderSettings($data,$id)
    {
       /* echo "<pre>";
        print_r($data);
        exit('fg');*/
        $this->db->delete('slider_settings', array('slider_id' => $id));
        $length1 = count($data['image']);
        for ($j = 0; $j < $length1; $j++) {
            if ($data['image'][$j] != '') {
                $item_slider = array(
                    'image' => $data['image'][$j],
                    'image_alt' => $data['image_alt'][$j],
                    'link' => $data['link'][$j],
                    'caption' => $data['caption'][$j],
                    'slider_id' => $id
                );
                $this->db->insert('slider_settings', $item_slider);
            }
        }
    }

    public function getSliderSettingData($slider_id) {

        $this->db->select('*')
            ->where('slider_id', $slider_id)->order_by('slider_id');
        $q = $this->db->get("slider_settings");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
           /* echo "<pre>";
            print_r($data);
            exit('fg');*/
            return $data;
        }
        return FALSE;
    }


    public function getAllCountries()
    {
        $q = $this->db->get('country');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
    public function getZonesOptionsByID($country_id)
    {

        $this->db->where('country_id', $country_id);
        $q = $this->db->get('zone');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
        }
      return $data;
    }
    public function getZonesOptionsByIDAjax($country_id)
    {
        $this->db->where('country_id', $country_id);
        $q = $this->db->get('zone');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
        }
        $options = '<option value="0"> All Areas</option>';
        if(!empty($data)){
            foreach ($data As $key => $zone){
                $options .= '<option value="'.$zone->zone_id.'"> '.$zone->name.'</option>';
            }
        }
        echo  $options;
    }
    public function addZone($post_data)
    {
        $data = array(
            'name' => $post_data['name'],
            'description' =>$post_data['description']
        );

        if ($this->db->insert("geo_zone", $data)) {
            $insert_id = $this->db->insert_id();
            if (isset($post_data['country_id']) && !empty($post_data['country_id'])) {
                $length = count($post_data['country_id']);
               // $this->db->delete('variants_combination', array('product_id' => $id));
                for ($i = 0; $i < $length; $i++) {
                    if ($post_data['country_id'][$i] != '') {
                        $item_variant = array(
                            'country_id' => $post_data['country_id'][$i],
                            'zone_id' => $post_data['zone_id'][$i],
                            'geo_zone_id' => $insert_id
                        );
                        $this->db->insert('zone_to_geo_zone', $item_variant);
                    }
                }
            }
            return true;
        }
        return false;
    }
    public function updateZone($id, $post_data = array())
    {
        $data = array(
            'name' => $post_data['name'],
            'description' =>$post_data['description']
        );
        $this->db->where('geo_zone_id', $id);
        if ($this->db->update("geo_zone", $data)) {
            if (isset($post_data['country_id']) && !empty($post_data['country_id'])) {
                $length = count($post_data['country_id']);
                 $this->db->delete('zone_to_geo_zone', array('geo_zone_id' => $id));
                for ($i = 0; $i < $length; $i++) {
                    if ($post_data['country_id'][$i] != '') {
                        $item_variant = array(
                            'country_id' => $post_data['country_id'][$i],
                            'zone_id' => $post_data['zone_id'][$i],
                            'geo_zone_id' => $id
                        );
                        $this->db->insert('zone_to_geo_zone', $item_variant);
                    }
                }
            }
            return true;
        }
        return false;
    }
    public function deleteZone($id)
    {
        if ($this->db->delete("geo_zone", array('geo_zone_id' => $id))) {
            $this->db->delete('zone_to_geo_zone', array('geo_zone_id' => $id));
            return true;
        }
        return FALSE;
    }
    public function getZoneByID($id)
    {
        $q = $this->db->get_where('geo_zone', array('geo_zone_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
    public function getAllCountriesBygeoZone($id)
    {
        $this->db->where('geo_zone_id', $id);
        $q = $this->db->get('zone_to_geo_zone');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }


    public function updateCurrency($id, $data = array())
    {
        $this->db->where('id', $id);
        if ($this->db->update("currencies", $data)) {
            return true;
        }
        return false;
    }
    public function deleteCurrency($id)
    {
        if ($this->db->delete("currencies", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }




    public function getPaypalSettings()
    {
        $q = $this->db->get('paypal');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updatePaypal($data)
    {
        $this->db->where('id', '1');
        if ($this->db->update('paypal', $data)) {
            return true;
        }
        return FALSE;
    }

    public function getSkrillSettings()
    {
        $q = $this->db->get('skrill');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateSkrill($data)
    {
        $this->db->where('id', '1');
        if ($this->db->update('skrill', $data)) {
            return true;
        }
        return FALSE;
    }

    public function checkGroupUsers($id)
    {
        $q = $this->db->get_where("users", array('group_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteGroup($id)
    {
        if ($this->db->delete('groups', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getAllVariants()
    {
        $q = $this->db->get('variants');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getExpenseCategoryByID($id)
    {
        $q = $this->db->get_where("expense_categories", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getExpenseCategoryByCode($code)
    {
        $q = $this->db->get_where("expense_categories", array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addExpenseCategory($data)
    {
        if ($this->db->insert("expense_categories", $data)) {
            return true;
        }
        return false;
    }

    public function addExpenseCategories($data)
    {
        if ($this->db->insert_batch("expense_categories", $data)) {
            return true;
        }
        return false;
    }

    public function updateExpenseCategory($id, $data = array())
    {
        if ($this->db->update("expense_categories", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function hasExpenseCategoryRecord($id)
    {
        $this->db->where('category_id', $id);
        return $this->db->count_all_results('expenses');
    }

    public function deleteExpenseCategory($id)
    {
        if ($this->db->delete("expense_categories", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }



    public function addPriceGroup($data)
    {
        if ($this->db->insert('price_groups', $data)) {
            return true;
        }
        return false;
    }

    public function updatePriceGroup($id, $data = array())
    {
        $this->db->where('id', $id);
        if ($this->db->update('price_groups', $data)) {
            return true;
        }
        return false;
    }

    public function getAllPriceGroups()
    {
        $q = $this->db->get('price_groups');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getPriceGroupByID($id)
    {
        $q = $this->db->get_where('price_groups', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deletePriceGroup($id)
    {
        if ($this->db->delete('price_groups', array('id' => $id)) && $this->db->delete('product_prices', array('price_group_id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function setProductPriceForPriceGroup($product_id, $group_id, $price, $option_id=null)
    {
        if($option_id === NULL){$option_id = 0;}

        if ($this->getGroupPrice($group_id, $product_id , $option_id)) {
            if ($this->db->update('product_prices', array('price' => $price), array('price_group_id' => $group_id, 'product_id' => $product_id , 'option_id' => $option_id ))) {
                return true;
            }
        } else {

            if ($this->db->insert('product_prices', array('price' => $price, 'price_group_id' => $group_id, 'product_id' => $product_id , 'option_id' => $option_id  ))) {
                return true;
            }
        }
        return FALSE;
    }

    public function getGroupPrice($group_id, $product_id, $option_id)
    {
        $q = $this->db->get_where('product_prices', array('price_group_id' => $group_id, 'product_id' => $product_id, 'option_id' => $option_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductGroupPriceByPID($product_id, $group_id)
    {
        $pg = "(SELECT {$this->db->dbprefix('product_prices')}.price as price, {$this->db->dbprefix('product_prices')}.product_id as product_id FROM {$this->db->dbprefix('product_prices')} WHERE {$this->db->dbprefix('product_prices')}.product_id = {$product_id} AND {$this->db->dbprefix('product_prices')}.price_group_id = {$group_id}) GP";

        $this->db->select("{$this->db->dbprefix('products')}.id as id, {$this->db->dbprefix('products')}.code as code, {$this->db->dbprefix('products')}.name as name, GP.price", FALSE)
        // ->join('products', 'products.id=product_prices.product_id', 'left')
        ->join($pg, 'GP.product_id=products.id', 'left');
        $q = $this->db->get_where('products', array('products.id' => $product_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateGroupPrices($data = array())
    {
        foreach ($data as $row) {
            if ($this->getGroupPrice($row['price_group_id'], $row['product_id'])) {
                $this->db->update('product_prices', array('price' => $row['price']), array('product_id' => $row['product_id'], 'price_group_id' => $row['price_group_id']));
            } else {
                $this->db->insert('product_prices', $row);
            }
        }
        return true;
    }

    public function deleteProductGroupPrice($product_id, $option_id , $group_id)
    {
        if ($this->db->delete('product_prices', array('price_group_id' => $group_id, 'product_id' => $product_id, 'option_id' => $option_id ))) {
            return TRUE;
        }
        return FALSE;
    }

    public function getBrandByName($name)
    {
        $q = $this->db->get_where('brands', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }



    public function brandHasProducts($brand_id)
    {
        $q = $this->db->get_where('products', array('brand' => $brand_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }



}