<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Add admin_form_open
if ( ! function_exists('admin_form_open')) {
	function admin_form_open($action = '', $attributes = array(), $hidden = array()) {
		return form_open('admin/'.$action, $attributes, $hidden);
	}
}

// Add admin_form_open_multipart
if ( ! function_exists('admin_form_open_multipart')) {
	function admin_form_open_multipart($action = '', $attributes = array(), $hidden = array()) {
		if (is_string($attributes)) {
			$attributes .= ' enctype="multipart/form-data"';
		} else {
			$attributes['enctype'] = 'multipart/form-data';
		}
		return admin_form_open($action, $attributes, $hidden);
	}
}

// Add shop_form_open
if ( ! function_exists('shop_form_open')) {
	function shop_form_open($action = '', $attributes = array(), $hidden = array()) {
		return form_open('shop/'.$action, $attributes, $hidden);
	}
}

// Add shop_form_open_multipart
if ( ! function_exists('shop_form_open_multipart')) {
	function shop_form_open_multipart($action = '', $attributes = array(), $hidden = array()) {
		if (is_string($attributes)) {
			$attributes .= ' enctype="multipart/form-data"';
		} else {
			$attributes['enctype'] = 'multipart/form-data';
		}
		return shop_form_open($action, $attributes, $hidden);
	}
}
function getSliderData($short_code)
{

    if($short_code) {
        $CI = &get_instance();
        $CI->db->select('*');
        $CI->db->from('sliders');
        //$CI->db->order_by('id', "desc");
        $CI->db->limit(1);
        $CI->db->where('short_code',$short_code);
        $CI->db->where('status',1);
        $q = $CI->db->get();
        if ($q->num_rows() > 0) {
            $result = $q->result();
            $slider_id =  $result[0]->slider_id;

            if($slider_id){



                $CIs = &get_instance();
                $CIs->db->select('*');
                $CIs->db->from('slider_settings');
                //$CIs->db->order_by('id', "desc");
                $CIs->db->where('slider_id',$slider_id);
                $query = $CIs->db->get();

                if ($query->num_rows() > 0) {
                    foreach (($query->result()) as $row) {
                        $data[] = $row;
                    }
                    return $data;
                }else{
                    return false;
                }
            }
        }
    }else{
        return false;
    }
}