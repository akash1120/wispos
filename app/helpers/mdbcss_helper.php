<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('mcss_card')){

   //title
   //content
   // image
   // action
   // size : small,medium,large

   function mcss_card($header ='', $content = '', $image = '', $action = '', $action_title = ''){

   $card = '<div class="card">';

	              if($header != ''){
	              	$card .= '<div class="card-header '.config_item('panel_color').' '.config_item('panel_color_text').'-text">';
					   if($action != ''){
					 				$card .=  '<a href="'.base_url($action).'" class="'.config_item('panel_color_text').'-text">';
					 	}
	              	$card .= '<h4 class="card-title">'.$header.'</h4>';

				   if($action != ''){
					 				$card .=  '</a>';
					 	}
					$card .= '</div>';
	              }

	            if($image != ''){

					   if($action != ''){
					 				$card .=  '<a href="'.base_url($action).'">';
					 	}

	              		$card .= '<img class="img-fluid" src="'.base_url($image).'">';

				   if($action != ''){
					 				$card .=  '</a>';
					}

	            }

	            $card .= '<div class="card-body">';
	            $card .= '<p class="card-text">'.$content.'</p>';
	            $card .= '</div>'; // content end

  	            if($action != ''){

				$card .= '<div class="card-footer text-muted '.config_item('panel_color').' '.config_item('panel_color_text').'-text"">';

		            		if($action_title != '')
		              			$card .=  '<a href="'.base_url($action).'" class="'.config_item('panel_color_text').'-text">'.$action_title.'</a>';
		           			else
		           				$card .=  '<a href="'.base_url($action).'" class="'.config_item('panel_color_text').'-text">'.$action.'</a>';

				$card .= '</div>';
				}

	          $card .= '</div>';

return $card;

   }
}

if ( ! function_exists('infoMessage')){
	function infoMessage($message = ''){

		   $info_message = '<div id="infoMessage">';

	      		if($message){
	      			$info_message .= '<div class="card red white-text heading valign-wrapper" style="height:80px">';
	      			$info_message .= '<div class="m-auto">'.$message.'</div>';
	      			$info_message .= '</div>';
	      		}

	      $info_message .= '</div>';

		return $info_message;
	}
}

if ( ! function_exists('section_header')){
	function section_header($heading,$subheading = ''){

			$section_header = '<div class="card header_menu_heading '.config_item('panel_color').' '.config_item('panel_color_text').'-text heading text-center">
	            	<div class="card-body">
	                    <h4 class="card-title">'.$heading.'</h4>';
	            		if($subheading != '')
	        $section_header .= '<p>'.$subheading.'</p>';
	        $section_header .= '</div>
	  		</div>';
	  		return $section_header;
	}
}


	/**
	 * @return array A CSRF key-value pair
	 */
if ( ! function_exists('_get_csrf_nonce'))
{
	function _get_csrf_nonce()
	{

		 $CI =& get_instance();

		$CI->load->helper('string');
		$key = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$CI->session->set_flashdata('csrfkey', $key);
		$CI->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

}

	/**
	 * @return bool Whether the posted CSRF token matches
	 */
if ( ! function_exists('_valid_csrf_nonce'))
{
	function _valid_csrf_nonce()
	{

		$CI =& get_instance();

		$csrfkey = $CI->input->post($CI->session->flashdata('csrfkey'));
		if ($csrfkey && $csrfkey === $CI->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}

	/**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
if ( ! function_exists('_render_page'))
{
	function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{
		$CI =& get_instance();
		$CI->viewdata = (empty($data)) ? $CI->data : $data;

		$view_html = $CI->load->view($view, $CI->viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}

}
?>