<?php defined('BASEPATH') OR exit('No direct script access allowed');

class system_sync extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }

        if (!$this->Owner) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect('admin');
        }

        $this->lang->admin_load('settings', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('sync_system');
        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif';
        $this->allowed_file_size = '1024';
    }


    function check_connection(){
        $this->sync_system->check_secure_connection();
    }

    //Sync of Sales and sales Items
    function sync_of_sales(){
        $this->sync_system->sync_sales('sales');
    }

    //Sync of brands
    function sync_of_brands(){
        $this->sync_system->synchronization('brands');
    }

    //Sync of category
    function sync_of_categories(){
        $this->sync_system->synchronization('categories');
    }

    //Sync of Warehouses
    function sync_of_warehouses(){
        $this->sync_system->synchronization('warehouses');
    }

    //Sync of Products
    function sync_of_products(){
        $this->sync_system->synchronization('products');
        $this->sync_system->synchronization('product_photos');
        $this->sync_system->synchronization('warehouses_products');
        $this->sync_system->synchronization('product_variants');
    }

    //Sync of variants
    function sync_of_variants(){
        $this->sync_system->synchronization('variants');
    }




}
