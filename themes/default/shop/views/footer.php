<footer>
    <div class="container">
        <div id="cmsblock-23" class="cmsblock">
            <div class="description">
                <div class="footer-logo"><img src="<?= base_url(); ?>assets/uploads/logos/logo.jpg"
                                              class="img-responsive"><br>
                    <a href="#" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                    <a href="#" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
        <div class="middle-footer row">
            <div class="col-sm-2"></div>
            <div class="col-sm-2">
                <div id="cmsblock-35" class="cmsblock">
                    <div class="description">
                        <h4>SHOPPING GUIDE</h4>
                        <a herf="#">Faq's</a>
                        <a herf="#">Purchasing Process</a>
                        <a herf="#">Payment</a>
                        <a herf="#">Delivery</a>
                        <a herf="#">Returns</a>
                        <a herf="#">Technology</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                <h4>POLICIES</h4>
                <ul class="list-unstyled">
                    <li><a href="#">Conditions of Purchase</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Cookies Policy</a></li>
                </ul>
            </div>
            <div class="col-sm-2 position-4-socialfooter">
                <div id="cmsblock-36" class="cmsblock">
                    <div class="description">
                        <h4>COMPANY</h4>
                        <a herf="#">Work with us</a>
                        <a herf="#">Press</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-2 position-4-statictags">
                <div id="cmsblock-38" class="cmsblock">
                    <div class="description">
                        <h4>CONTACT</h4>
                        <a herf="#">Contact Form</a>
                        <a herf="#">Stores</a>
                        <b>+971 0 000 0000</b>
                    </div>
                </div>
            </div>
            <div class="col-sm-2"></div>
        </div>
    </div>
    <div class="bottom-footer">
        <div class="container">
            <p class="text_powered">© <?= date('Y'); ?> <?= $shop_settings->shop_name; ?>
                . <?= lang('all_rights_reserved'); ?></p>
            <div class="payment-method"><a href="http://wistech.biz/" target="_blank">Web Developer</a>:
                <a href="http://800wisdom.ae/" target="_blank"> 800Wisdom.ae</a></div>
            <div id="back-top" style="display: block;"><i class="fa fa-angle-double-up"></i></div>
        </div>
    </div>
</footer>


<script type="text/javascript">
    $(document).ready(function () {
        // hide #back-top first
        $("#back-top").hide();
        // fade in #back-top
        $(function () {
            $(window).scroll(function () {
                if ($(this).scrollTop() > 300) {
                    $('#back-top').fadeIn();
                } else {
                    $('#back-top').fadeOut();
                }
            });
            // scroll body to 0px on click
            $('#back-top').click(function () {
                $('body,html').animate({scrollTop: 0}, 800);
                return false;
            });
        });
    });
</script>


<script type="text/javascript">
    var m = '<?= $m; ?>', v = '<?= $v; ?>', products = {},
        filters = <?= isset($filters) && !empty($filters) ? json_encode($filters) : '{}'; ?>, shop_color, shop_grid,
        sorting;
    var cart = <?= isset($cart) && !empty($cart) ? json_encode($cart) : '{}' ?>;
    var site = {
        base_url: '<?= base_url(); ?>',
        site_url: '<?= site_url('/'); ?>',
        shop_url: '<?= shop_url(); ?>',
        csrf_token: '<?= $this->security->get_csrf_token_name() ?>',
        csrf_token_value: '<?= $this->security->get_csrf_hash() ?>',
        settings: {
            display_symbol: '<?= $Settings->display_symbol; ?>',
            symbol: '<?= $Settings->symbol; ?>',
            decimals: <?= $Settings->decimals; ?>,
            thousands_sep: '<?= $Settings->thousands_sep; ?>',
            decimals_sep: '<?= $Settings->decimals_sep; ?>',
            order_tax_rate: false,
            products_page: <?= $shop_settings->products_page ? 1 : 0; ?>}
    }

    var lang = {};
    lang.page_info = '<?= lang('page_info'); ?>';
    lang.cart_empty = '<?= lang('empty_cart'); ?>';
    lang.item = '<?= lang('item'); ?>';
    lang.items = '<?= lang('items'); ?>';
    lang.unique = '<?= lang('unique'); ?>';
    lang.total_items = '<?= lang('total_items'); ?>';
    lang.total_unique_items = '<?= lang('total_unique_items'); ?>';
    lang.tax = '<?= lang('tax'); ?>';
    lang.shipping = '<?= lang('shipping'); ?>';
    lang.total_w_o_tax = '<?= lang('total_w_o_tax'); ?>';
    lang.product_tax = '<?= lang('product_tax'); ?>';
    lang.order_tax = '<?= lang('order_tax'); ?>';
    lang.total = '<?= lang('total'); ?>';
    lang.grand_total = '<?= lang('grand_total'); ?>';
    lang.reset_pw = '<?= lang('forgot_password?'); ?>';
    lang.type_email = '<?= lang('type_email_to_reset'); ?>';
    lang.submit = '<?= lang('submit'); ?>';
    lang.error = '<?= lang('error'); ?>';
    lang.add_address = '<?= lang('add_address'); ?>';
    lang.update_address = '<?= lang('update_address'); ?>';
    lang.fill_form = '<?= lang('fill_form'); ?>';
    lang.already_have_max_addresses = '<?= lang('already_have_max_addresses'); ?>';
    lang.send_email_title = '<?= lang('send_email_title'); ?>';
    lang.message_sent = '<?= lang('message_sent'); ?>';
</script>
<script src="<?= $assets; ?>js/validator.js"></script>
<script src="<?= $assets; ?>js/libs.min.js"></script>
<script src="<?= $assets; ?>js/scripts.min.js"></script>
<script src="<?= $assets; ?>js/jquery.elevatezoom.js"></script>
<script src="<?= $assets; ?>js/owl.carousel.js"></script>

<script type="text/javascript">
    $(document).ready(function () {

        $(document).on("click", ".moredeatil", function (t) {
            t.preventDefault();
            var url = $(this).attr("href");
            window.location.href = url;
        });

        $("#collapse1").click(function(){
            $(".test").toggle();
        });


        var minimum = 1;
        $("#input-quantity").change(function () {
            if ($(this).val() < minimum) {
                alert("Minimum Quantity: " + minimum);
                $("#input-quantity").val(minimum);
            }
        });
        // increase number of product
        function minus(minimum) {
            var currentval = parseInt($("#input-quantity").val());
            $("#input-quantity").val(currentval - 1);
            if ($("#input-quantity").val() <= 0 || $("#input-quantity").val() < minimum) {
                alert("Minimum Quantity: " + minimum);
                $("#input-quantity").val(minimum);
            }
        };
        // decrease of product
        function plus() {
            var currentval = parseInt($("#input-quantity").val());
            $("#input-quantity").val(currentval + 1);
        };
        $('#minus').click(function () {
            minus(minimum);
        });
        $('#plus').click(function () {
            plus();
        });


        $('.imggallery').owlCarousel({
            loop: true,
            margin: 10,
            nav: true,
            dots: false,
            // navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
            responsive: {
                0: {
                    items: 3
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 5
                }
            }
        });
        $('.relgallary').owlCarousel({
            loop: true,
            margin: 10,
            nav: true,
            dots: false,
            // navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>','<i class="fa fa-angle-right" aria-hidden="true"></i>'],
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                1000: {
                    items: 3
                }
            }
        });
    });
    $('.owlimage').on('click', function () {
        var src = $(this).attr('src');
        $('#zoom_01').attr('src', src);
        /* $('#zoom_01').attr('data-zoom-image',src);*/
        $('.zoomContainer').remove();
        $('#zoom_01').removeData('elevateZoom');
        // Update source for images
        $('#zoom_01').attr('src', src);
        $('#zoom_01').data('zoom-image', src);
        // Reinitialize EZ
        $('#zoom_01').elevateZoom({
            cursor: "crosshair",
            zoomWindowWidth: 400,
            zoomWindowHeight: 400,
            zoomWindowPosition: 1,
            zoomWindowOffetx: 10,
            zoomWindowFadeIn: 500,
            zoomWindowFadeOut: 500,
            zoomLens: false
        });
        //  alert(src);
    });
    $('#zoom_01').elevateZoom({
        cursor: "crosshair",
        zoomWindowFadeIn: 500,
        zoomWindowFadeOut: 750,
        zoomWindowWidth: '400',
        zoomLens: false
    });
    <?php if ($message || $warning || $error || $reminder) { ?>
    $(document).ready(function () {
        <?php if ($message) { ?>
        sa_alert('<?=lang('success');?>', '<?= trim(str_replace(array("\r", "\n", "\r\n"), '', addslashes($message))); ?>');
        <?php } if ($warning) { ?>
        sa_alert('<?=lang('warning');?>', '<?= trim(str_replace(array("\r", "\n", "\r\n"), '', addslashes($warning))); ?>', 'warning');
        <?php } if ($error) { ?>
        sa_alert('<?=lang('error');?>', '<?= trim(str_replace(array("\r", "\n", "\r\n"), '', addslashes($error))); ?>', 'error', 1);
        <?php } if ($reminder) { ?>
        sa_alert('<?=lang('reminder');?>', '<?= trim(str_replace(array("\r", "\n", "\r\n"), '', addslashes($reminder))); ?>', 'info');
        <?php } ?>
    });
    <?php } ?>
</script>
<script type="text/javascript">
    <?php if ($message || $warning || $error || $reminder) { ?>
    $(document).ready(function () {
        <?php if ($message) { ?>
        sa_alert('<?=lang('success');?>', '<?= trim(str_replace(array("\r", "\n", "\r\n"), '', addslashes($message))); ?>');
        <?php } if ($warning) { ?>
        sa_alert('<?=lang('warning');?>', '<?= trim(str_replace(array("\r", "\n", "\r\n"), '', addslashes($warning))); ?>', 'warning');
        <?php } if ($error) { ?>
        sa_alert('<?=lang('error');?>', '<?= trim(str_replace(array("\r", "\n", "\r\n"), '', addslashes($error))); ?>', 'error', 1);
        <?php } if ($reminder) { ?>
        sa_alert('<?=lang('reminder');?>', '<?= trim(str_replace(array("\r", "\n", "\r\n"), '', addslashes($reminder))); ?>', 'info');
        <?php } ?>
    });
    <?php } ?>
</script>

<script type="text/javascript">

    $("#results").on("mouseover", ".grid .custom_pro_list_img_bucket", function () {
        var qv_id = this.id;
        $("#results .grid .product-image_" + qv_id).addClass("grid_product_quick");
        $("#results .grid .product-image_" + qv_id + " a .quickview").removeClass('hide');

    });
    $("#results").on("mouseout", ".grid .custom_pro_list_img_bucket", function () {
        var qv_id = this.id;
        $("#results .grid .product-image_" + qv_id).removeClass("grid_product_quick");
        $("#results .grid .product-image_" + qv_id + " a .quickview").addClass('hide');
    });


    function shareit() {
        var url = $('.link_viewfull_pro').attr('href'); //Set desired URL here
        var img = $('#pro_quickview_img').attr('src'); //Set Desired Image here

        var totalurl = encodeURIComponent(url + '?img=' + img);

        window.open('http://www.facebook.com/sharer.php?u=' + totalurl, '', 'width=500, height=500, scrollbars=yes, resizable=no');

    }

    //To check the product BuyOneGetOne
    /*    $(document).on('click', '.add-to-cart', function(e) {
     var product_id_ = $(this).attr("data-id");
     if(product_id_){
     $.ajax({
     url: site.site_url+'cart/checkBuyOneGetOne/'+product_id_,
     type: 'GET',
     dataType: 'json',
     data: {
     },
     });
     }
     });*/
    //End of To check the product BuyOneGetOne

    //To check Promotion on specific product add to cart
    $(document).on('click', '.add-to-cart', function (e) {
        var product_id_ = $(this).attr("data-id");
        if (product_id_) {
            $.ajax({
                url: site.site_url + 'cart/specificProductInCart/' + product_id_,
                type: 'GET',
                dataType: 'json',
                data: {},
            }).done(
                function (data) {
                    //alert(JSON.stringify(data));
                    $("#itembasepromotion .modal-title").html(data.title);
                    $('#upsell_img_path').attr('src', site.site_url + 'assets/uploads/' + data.product_image_path);
                    $("#itembasepromotion .modal_custom_padding.name").html(data.name);
                    $("#itembasepromotion .modal_promotion.description").html(data.description);
                    $("#itembasepromotion .modal_promotion_price.price .innerside_price").html(data.product_real_price);
                    $("#itembasepromotion .modal-footer .custom_onbase_ofproduct").attr('data-id', data.popup_product_id);
                    $("#itembasepromotion").modal('show');
                });
        }
    });

    $(document).on('click', '.custom_onbase_ofproduct , .quick_view_cart', function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var cart = $('.shopping-cart:visible');
        $.ajax({
            url: site.site_url + 'cart/add/' + id, type: 'GET', dataType: 'json', data: {
                //ty: qty_input.val()
            }
        })
            .done(function (data) {
                cart = data;
                update_mini_cart(data);

                var is_cat_on_prom = JSON.stringify(data.cat_on_promotion);
                is_cat_on_prom = is_cat_on_prom.replace('"', '');
                is_cat_on_prom = is_cat_on_prom.replace('"', '');
                if (is_cat_on_prom != '' && is_cat_on_prom != 'null') {
                    swal("",
                        is_cat_on_prom,
                        "success");
                }

            });

        $("#itembasepromotion").modal('hide');
        $("#pro_quickview").modal('hide');
    });


    //Product Quick view popup
    $(document).on('click', '.quickview_pop', function (e) {
        e.preventDefault();
        var pro_id = $(this).attr('data-id');
        var cart = $('.shopping-cart:visible');
        $.ajax({
            url: site.site_url + 'shop/product_quickview/' + pro_id, type: 'GET', dataType: 'json', data: {
                //ty: qty_input.val()
            }
        }).done(function (data) {
            //alert(JSON.stringify(data));
            //alert(JSON.stringify(data.free_shipment));
            $("#pro_quickview .modal-title").html(data.name + ' (' + data.code + ')');

            if ($.trim(data.free_shipment) == 1) {
                $("#pro_quickview .free-shipping-cont .free-shipping-cont_inner .free_shipng").html("<strong class='green-text'>FREE SHIPPING</strong>");
            } else {
                $("#pro_quickview .free-shipping-cont .free-shipping-cont_inner .free_shipng").html("<strong class='green-text'></strong>");
            }

            if (data.promotion == 0 || data.promotion == null) {
                $(".save_amount_sec").hide();
                $("#pro_quickview .price_sec_contaire").html(data.price);
            } else {
                $(".save_amount_sec").show();
                $("#pro_quickview .price_sec_contaire").html(data.promo_price);
                $("#pro_quickview .save_amount_sec .was").html(data.price);
                $("#pro_quickview .save_amount_sec .saved .noWrap").html(data.save_amount);
            }
            if (!$.trim(data.details)) {
                $("#pro_quickview .description_sec").hide();
            } else {
                $("#pro_quickview .description_sec").show();
                $("#pro_quickview .description_sec p").html(data.details);
            }

            if (!$.trim(data.variants)) {
                $("#pro_quickview .variants").hide();
            } else {
                $("#pro_quickview .variants").show();
                $("#pro_quickview .variants .variants_space").html(data.variants);
            }

            $("#pro_quickview #twitter_share_btn").attr('href', 'https://twitter.com/share?url=' + site.site_url + 'product/' + data.slug + '/&amp;text=' + data.name + '&amp;hashtags=' + data.name);

            $("#pro_quickview #google_share_btn").attr('href', 'https://plus.google.com/share?url=' + site.site_url + 'product/' + data.slug);

            $("#pro_quickview .cart_mainarea span.text_span_stock span.amount_span_stock").html(data.quantity);
            $("#pro_quickview .link_viewfull_pro").attr('href', site.site_url + 'product/' + data.slug);
            $('#pro_quickview_img').attr('src', site.site_url + 'assets/uploads/' + data.image);
            $('#pro_quickview .cart_mainarea button.cus_quick_addtocart_btn.add-to-cart').attr('data-id', pro_id);
            $("#pro_quickview").modal('show');
        });

    });
    //End of Product Quick view popup
</script>

</body>
</html>