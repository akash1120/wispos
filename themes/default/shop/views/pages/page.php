<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="page-contents">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">

                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <h1><?= $page->title; ?></h1>
                        <?php if($page->slug == $shop_settings->contact_link){ ?>
                            <?= $page->body; ?>

                            <form action="<?= base_url().'shop/send_message/'; ?>" method="post" enctype="multipart/form-data" class="form-horizontal" data-toggle="validator">
                                <fieldset>
                                    <legend>Contact Form</legend>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-name">Your Name</label>
                                        <div class="col-sm-10">
                                            <input required type="text" name="name" value="" data-error="Name must be between 3 and 32 characters!" id="name" class="form-control">
                                            <div class="help-block with-errors"></div>
                                        </div>


                                    </div>

                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-email">E-Mail Address</label>
                                        <div class="col-sm-10">
                                            <input required type="text" name="email" value="" data-error="E-Mail Address does not appear to be valid!" id="email" class="form-control">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-enquiry">Enquiry</label>
                                        <div class="col-sm-10">
                                            <textarea maxlength="3000" minlength="10" required name="enquiry" rows="10" id="enquiry" data-error="Enquiry must be between 10 and 3000 characters!" class="form-control"></textarea>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="buttons">
                                    <div class="pull-right">
                                        <input class="btn btn-primary" type="submit" value="Submit">
                                    </div>
                                </div>
                            </form>


                            <?php '<p><button type="button" class="btn btn-primary email-modal">Send us email</button></p>'; ?>

                        <?php }else{ ?>
                            <div class="">
                                <?= $page->body; ?>
                            </div>
                        <?php }?>

                </div>
            </div>
        </div>
    </div>
</section>
