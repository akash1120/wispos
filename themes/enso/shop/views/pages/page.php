<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- Page Breadcrumb Start -->
<div class="main-breadcrumb mb-100">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="breadcrumb-content text-center ptb-70">
                    <ul class="breadcrumb-list breadcrumb">
                        <li><a href="<?= base_url(); ?>">home</a></li>
                        <li><a href="#"><?= $page->title; ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>
<!-- Page Breadcrumb End -->
<!--<h1><?/*= $page->title; */?></h1>-->
<?php

if($page->slug == $shop_settings->contact_link){ ?>


    <div id="map" class="mt-100" style="height:550px"></div>
    <!-- Google Map End -->
    <!-- Contact Email Area Start -->
    <div class="contact-email-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="mb-5">Contact Us</h3>
                    <p class="text-capitalize mb-40"></p>
                    <div class="row">
                        <form action="<?= base_url().'shop/send_message/'; ?>" method="post" enctype="multipart/form-data" class="form-horizontal" data-toggle="validator">
                            <div class="address-wrapper">
                                <div class="col-md-12 form-group">
                                    <div class="address-fname ">
                                        <input required type="text" name="name" placeholder="Name"  style="margin-bottom: 0px">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12 form-group">
                                    <div class="address-email">
                                        <input required type="email" name="email" placeholder="Email" style="margin-bottom: 10px">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <!-- <div class="col-md-6">
                                     <div class="address-web">
                                         <input type="text" name="website" placeholder="Website">
                                     </div>
                                 </div>-->
                                <div class="col-xs-12 form-group">
                                    <div class="address-subject">
                                        <input required type="text" name="subject" placeholder="Subject">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-xs-12 form-group">
                                    <div class="address-textarea">
                                        <textarea  maxlength="3000" minlength="10" required name="enquiry" placeholder="Write your message"></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>

                                </div>
                            </div>
                            <p class="form-message ml-15"></p>
                            <div class="col-xs-12 footer-content mail-content">
                                <div class="send-email pull-right">
                                    <input type="submit" value="Send Email" class="submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <!--   <form action="<?/*= base_url().'shop/send_message/'; */?>" method="post" enctype="multipart/form-data" class="form-horizontal" data-toggle="validator">
                                <fieldset>
                                    <legend>Contact Form</legend>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-name">Your Name</label>
                                        <div class="col-sm-10">
                                            <input required type="text" name="name" value="" data-error="Name must be between 3 and 32 characters!" id="name" class="form-control">
                                            <div class="help-block with-errors"></div>
                                        </div>


                                    </div>

                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-email">E-Mail Address</label>
                                        <div class="col-sm-10">
                                            <input required type="text" name="email" value="" data-error="E-Mail Address does not appear to be valid!" id="email" class="form-control">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-enquiry">Enquiry</label>
                                        <div class="col-sm-10">
                                            <textarea maxlength="3000" minlength="10" required name="enquiry" rows="10" id="enquiry" data-error="Enquiry must be between 10 and 3000 characters!" class="form-control"></textarea>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="buttons">
                                    <div class="pull-right">
                                        <input class="btn btn-primary" type="submit" value="Submit">
                                    </div>
                                </div>
                            </form>
-->

    <?php '<p><button type="button" class="btn btn-primary email-modal">Send us email</button></p>'; ?>

<?php }
else if($page->slug == 'custom-orders'){ ?>
    
    <!-- Google Map End -->
    <!-- Contact Email Area Start -->
    <div class="contact-email-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h3 class="mb-5">Custom Orders</h3>
                    <p class="text-capitalize mb-40"></p>
                    <div class="row">
                        <form action="<?= base_url().'shop/send_custome_order/'; ?>" method="post" enctype="multipart/form-data" class="form-horizontal" data-toggle="validator">
                            <div class="address-wrapper">
                                <div class="col-md-12 form-group">
                                    <div class="address-fname ">
                                        <input required type="text" name="name" placeholder="Name"  style="margin-bottom: 0px">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12 form-group">
                                    <div class="address-email">
                                        <input required type="email" name="email" placeholder="Email" style="margin-bottom: 10px">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <!-- <div class="col-md-6">
                                     <div class="address-web">
                                         <input type="text" name="website" placeholder="Website">
                                     </div>
                                 </div>-->
                                <div class="col-xs-12 form-group">
                                    <div class="address-subject">
                                        <input required type="text" name="subject" placeholder="Subject">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-xs-12 form-group">
                                    <div class="address-textarea">
                                        <textarea  maxlength="3000" minlength="10" required name="enquiry" placeholder="Write your message"></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="textbox-wrap form-group">
                                    <div class="row" style="padding-right: 25px; padding-left: 5px;">
                                        <div class="col-sm-6 div-captcha-left">
                                            <span class="captcha-image"><?php echo $image; ?></span>
                                        </div>
                                        <div class="col-sm-6 div-captcha-right">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <a href="#" class="reload-captcha">
                                                        <i class="fa fa-refresh"></i>
                                                    </a>
                                                </span>
                                                <?php echo form_input($captcha); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p class="form-message ml-15"></p>
                            <div class="col-xs-12 footer-content mail-content">
                                <div class="send-email pull-right">
                                    <input type="submit" value="Send Email" class="submit">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>




    <!--   <form action="<?/*= base_url().'shop/send_message/'; */?>" method="post" enctype="multipart/form-data" class="form-horizontal" data-toggle="validator">
                                <fieldset>
                                    <legend>Contact Form</legend>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-name">Your Name</label>
                                        <div class="col-sm-10">
                                            <input required type="text" name="name" value="" data-error="Name must be between 3 and 32 characters!" id="name" class="form-control">
                                            <div class="help-block with-errors"></div>
                                        </div>


                                    </div>

                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-email">E-Mail Address</label>
                                        <div class="col-sm-10">
                                            <input required type="text" name="email" value="" data-error="E-Mail Address does not appear to be valid!" id="email" class="form-control">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-enquiry">Enquiry</label>
                                        <div class="col-sm-10">
                                            <textarea maxlength="3000" minlength="10" required name="enquiry" rows="10" id="enquiry" data-error="Enquiry must be between 10 and 3000 characters!" class="form-control"></textarea>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="buttons">
                                    <div class="pull-right">
                                        <input class="btn btn-primary" type="submit" value="Submit">
                                    </div>
                                </div>
                            </form>
-->

    <?php '<p><button type="button" class="btn btn-primary email-modal">Send us email</button></p>'; ?>

<?php }
else if($page->slug == $shop_settings->about_link){
    ?>
    <style>
        h3{
            text-align:center;
        }
    </style>
    <div class="privacy-policy pb-100">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="about-us">
                        <img class="img width100percent" src="<?= base_url($assets.'enso-images/about-us.jpg')?>" alt="blog-image" style="width: 100%;" >
                        <?= $page->body; ?>
                    </div>
                </div>
            </div>
            <!-- Row End -->
        </div>
        <!-- Container End -->
    </div>
<?php }else if($page->slug == 'privacy-policy'){
    ?>
    <div class="privacy-policy pb-100">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?= $page->body; ?>
                </div>
            </div>
            <!-- Row End -->
        </div>
        <!-- Container End -->
    </div>
<?php } else{ ?>
    <div class="container text-justify">
        <?= $page->body; ?>
    </div>
<?php }?>

<script>
    $(document).ready(function () {
        $('.reload-captcha').click(function (event) {
            event.preventDefault();
            $.ajax({
                url: '<?=admin_url();?>auth/reload_captcha',
                success: function (data) {
                    $('.captcha-image').html(data);
                }
            });
        });
    });
</script>