<!-- Header Area End -->
<!-- Page Breadcrumb Start -->
<div class="main-breadcrumb mb-100">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="breadcrumb-content text-center ptb-70">
                    <ul class="breadcrumb-list breadcrumb">
                        <li><a href="#">home</a></li>
                        <li><a href="#">wishlist</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>
<!-- Page Breadcrumb End -->
<!-- Wish List Start -->
<!-- Page Breadcrumb End -->
<!-- Wish List Start -->
<div class="cart-main-area wish-list pb-50">
    <div class="container">
        <!-- Section Title Start -->
        <div class="section-title mb-50">
            <h2>wish list</h2>
        </div>
        <!-- Section Title Start End -->
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <!-- Form Start -->

                    <!-- Table Content Start -->
                    <div class="table-content table-responsive">
                        <?php
                        if (!empty($items)) {
                            $r = 1;
                            ?>
                        <table>
                            <thead>
                            <tr>
                                <th class="product-remove">Remove</th>
                                <th class="product-thumbnail">Image</th>
                                <th class="product-name">Product</th>
                                <th class="product-price">Unit Price</th>
                                <th class="product-quantity">Stock Status</th>
                                <th class="product-subtotal">add to cart</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php
                            foreach ($items as $item) {
                            ?>

                            <tr>
                                <td class="product-remove"> <a href="#" class="remove-wishlist" data-id="<?= $item->id; ?>"><i class="fa fa-times" aria-hidden="true"></i></a></td>
                                <td class="product-thumbnail">
                                    <a href="#"><img src="<?= base_url('ucloud/plugins/filepreviewer/site/resize_image_inline.php?f='.$item->image.'&w=500&h=500&m=medium&uh=&o=jpg'); ?>" alt="cart-image" /></a>
                                </td>
                                <td class="product-name"><a href="#"><?= '<a href="#">'.$item->name.'</a><br>'.$item->details; ?></a></td>
                                <td class="product-price"><span class="amount">
                                <?php
                                if ($item->promotion) {
                                    echo '<del class="text-red">'.$this->sma->convertMoney($item->price).'</del><br>';
                                    echo $this->sma->convertMoney($item->promo_price);
                                } else {
                                    echo $this->sma->convertMoney($item->price);
                                }
                                ?>
                                </span></td>

                                <td class="product-stock-status"><span><?= $item->quantity > 0 ? 'In Stock' : 'Out of Stock'; ?></span></td>
                                <td class="product-add-to-cart "><a  href="<?php echo base_url().'product/'. $item->slug; ?>" class="" id="<?= $item->id; ?>" data-id="<?= $item->id; ?>">add to cart</a></td>
                            </tr>
                                <?php
                                $r++;
                            }
                            ?>


                            </tbody>
                        </table>
                      <?php
                        } else {
                            echo '<strong>'.lang('wishlist_empty').'</strong>';
                        }
                        ?>
                    </div>
                    <!-- Table Content Start -->
                <!-- Form End -->
            </div>
        </div>
        <!-- Row End -->
    </div>
</div>
<!-- Wish List End -->
<!-- Footer Start -->


<script type="text/javascript">
var addresses = <?= !empty($addresses) ? json_encode($addresses) : 'false'; ?>;
</script>
