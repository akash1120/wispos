<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>




<style>
    .padlr30{
        padding-left: 30px;
        padding-right: 30px;
    }
</style>

    <!-- Header Area End -->
    <!-- Page Breadcrumb Start -->
    <div class="main-breadcrumb mb-100">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="breadcrumb-content text-center ptb-70">
                        <ul class="breadcrumb-list breadcrumb">
                            <li><a href="<?= base_url(); ?>">home</a></li>
                            <li><a href="<?= base_url().'cart'; ?>">cart</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Row End -->
        </div>
        <!-- Container End -->
    </div>

    <!-- cart-main-area & wish list start -->
    <div class="cart-main-area pb-100">
        <div class="container">
            <!-- Section Title Start -->
            <div class="section-title mb-50">
                <h2>cart</h2>
            </div>
            <!-- Section Title Start End -->
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <!-- Form Start -->
                        <!-- Table Content Start -->
                        <div class="table-content table-responsive mb-50">
                            <table id="cart-table" >
                                <thead>
                                <tr>
                                    <th class="product-thumbnail">Image</th>
                                    <th class="product-name">Product</th>
                                    <th class="product-price">Price</th>
                                    <th class="product-quantity">Quantity</th>
                                    <th class="product-subtotal">Total</th>
                                    <th class="product-remove">Remove</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    <div class="clearfix"></div>
                    <input type="hidden" id="giftcart_amount" value="<?php if($this->session->userdata('gift_cart_amount')){ echo $this->session->userdata('gift_cart_amount');} ?>">
                    <input type="hidden" id="hide_shipping_amount" value="1">

                        <div class="cart-contents">
                            <div class="col-sm-12 cart_adjustment">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h4 class="panel-title"><a href="#collapse-voucher" data-toggle="collapse"
                                                                   data-parent="#accordion"
                                                                   class="accordion-toggle collapsed"
                                                                   aria-expanded="false">Use Gift Certificate <i
                                                        class="fa fa-caret-down"></i></a></h4>
                                    </div>
                                    <div id="collapse-voucher" class="panel-collapse collapse" aria-expanded="false"
                                         style="height: 0px;">
                                        <div class="panel-body">
                                            <label class="control-label" for="input-voucher">Enter your gift
                                                certificate code here</label>
                                            <div class="input-group1">
                                                <input type="text" name="voucher"
                                                       placeholder="Enter your gift certificate code here"
                                                       id="input-voucher" class="form-control" value="<?php if($this->session->userdata('gift_cart_no')){ echo $this->session->userdata('gift_cart_no');} ?>"
                                                    <?php if($this->session->userdata('gift_cart_no')){ echo'readonly';} ?>
                                                >
                                                <span class="input-group-btn">
        <input type="submit" value="Apply Gift Certificate" id="button-voucher" data-loading-text="Loading..."
               class="newsletter-btn pwd-btn-width"
            <?php if($this->session->userdata('gift_cart_no')){ echo'disabled';} ?>
        >
        </span></div>
                                            <?php

                                            if (!$this->loggedIn) {
                                                ?>
                                                <!--<input type="hidden" id="customer_id" value="no">-->
                                                <?php
                                            }else{
                                                ?>
                                                <!--<input type="hidden" id="customer_id" value="<?php /*echo $this->session->userdata('user_id'); */?>">-->

                                            <?php } ?>

                                            <script src="<?= base_url($assets.'enso-js/vendor/jquery-1.12.4.min.js')?>"></script>
                                            <script src="<?= base_url($assets.'js/sweetalert.min.js')?>"></script>
                                            <script type="text/javascript">
                                                $('#button-voucher').on('click', function () {
                                                    var that = $(this);

                                                    var customer_id = <?php echo $customer_id; ?>;
                                                    if(customer_id=='0'){
                                                        window.location.href = site.base_url+"login";
                                                    }
                                                    var cn = $('input[name=\'voucher\']').val();
                                                    if (cn != '') {
                                                        $.ajax({
                                                            type: "get", async: false,
                                                            url: site.base_url + "admin/sales/validate_gift_card/" + cn,
                                                            dataType: "json",
                                                            success: function (data) {
                                                                console.log(data);
                                                                if (data === false) {
                                                                    $('#giftcart_amount').val();
                                                                    swal('<?=lang('incorrect_gift_card')?>');
                                                                } else if (data.customer_id !== null && parseInt(data.customer_id) !== customer_id) {
                                                                    swal('<?=lang('gift_card_not_for_customer')?>');
                                                                } else {
                                                                    $('#giftcart_amount').val(data.balance);
                                                                    update_cart_gift(data.balance,cn);
                                                                    swal('Balnace updated');
                                                                    location.reload();
                                                                    that.prop('disabled',true);
                                                                    $('input[name=\'voucher\']').prop('disabled', true);
                                                                    // $('#gc_details').html('<small>Card No: ' + data.card_no + '<br>Value: ' + data.value + ' - Balance: ' + data.balance + '</small>');
                                                                    //$('#gift_card_no').parent('.form-group').removeClass('has-error');
                                                                }
                                                            }
                                                        });
                                                    }

                                                });
                                            </script>
                                        </div>
                                    </div>
                                </div>


                                </div>
                            </div>
                        </div>


                        <!-- Table Content Start -->
                        <div class="row padlr30">
                            <!-- Cart Button Start -->
                            <div class="col-md-7 col-sm-7 col-xs-12">
                                <div class="buttons-cart">
                                    <input type="submit" value="Update Cart" />
                                    <a href="<?= site_url('shop/products'); ?>">Continue Shopping</a>
                                </div>
                            </div>
                            <!-- Cart Button Start -->
                            <!-- Cart Totals Start -->
                            <div class="col-md-5 col-sm-5 col-xs-12">
                                <div class="cart_totals">
                                    <h2>Cart Totals</h2>
                                    <br />
                                    <table id="cart-totals">
                                        <tbody>
                                        </tbody>
                                    </table>
                                    <div class="wc-proceed-to-checkout">
                                       <!-- <a href="<?/*= site_url('cart/checkout'); */?>">Proceed to Checkout</a>-->
                                        <a href="<?= site_url('shop/order_check'); ?>">Proceed to Checkout</a>
                                    </div>
                                </div>
                            </div>
                            <!-- Cart Totals End -->





                        </div>
                        <!-- Row End -->

                    <!-- Form End -->
                </div>
            </div>
            <!-- Row End -->
        </div>
    </div>
    <!-- cart-main-area & wish list end -->




    <?php
    $this->load->helper('shop_helper');
    if ($upcoming_promotions != '' && multi_array_search($upcoming_promotions[0]->item_id, $this->cart->contents()) == false) {
        require 'show_upselling.php';
    }

    if (!isset($_COOKIE['cart_upselling'])) {
        //start of set cookies
        //$cart_upselling = "cart_upselling";
        //$cookie_value = "showed";
        //setcookie($cart_upselling, $cookie_value, time() + (86400 * 30), "/");
        //End of set Cookies
        //require 'show_upselling.php';
    } else {
        //setcookie('cart_upselling', 'showed' , time() - 3600, "/");
    }


    ?>