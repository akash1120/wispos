<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
$wm = array('0' => lang('no'), '1' => lang('yes'));
$ps = array('0' => lang("disable"), '1' => lang("enable"));

$paypal = $this->db->get("paypal")->result();
$cod = $this->db->get("cod")->result();
$skrill = $this->db->get("skrill")->result();
$get_all_payment_method = array_merge($paypal , $cod, $skrill);
$get_all_warehouses = $this->site->getAllWarehouses();

$is_pp = $this->site->getActivePaymentMethod('PayPal','paypal');
$is_skrill = $this->site->getActivePaymentMethod('Skrill','skrill');
$is_cod = $this->site->getActivePaymentMethod('COD','cod');
$active_payment_method = array_merge((array)$is_pp,(array)$is_skrill,(array)$is_cod);
$active_ecommerce_shop = $this->site->getAllWarehousesForEcommerce();

?>

<script>
    $(document).ready(function () {
        <?php if(isset($message)) { echo 'localStorage.clear();'; } ?>
        var timezones = <?= json_encode(DateTimeZone::listIdentifiers(DateTimeZone::ALL)); ?>;
        $('#timezone').autocomplete({
            source: timezones
        });
        if ($('#protocol').val() == 'smtp') {
            $('#smtp_config').slideDown();
        } else if ($('#protocol').val() == 'sendmail') {
            $('#sendmail_config').slideDown();
        }
        $('#protocol').change(function () {
            if ($(this).val() == 'smtp') {
                $('#sendmail_config').slideUp();
                $('#smtp_config').slideDown();
            } else if ($(this).val() == 'sendmail') {
                $('#smtp_config').slideUp();
                $('#sendmail_config').slideDown();
            } else {
                $('#smtp_config').slideUp();
                $('#sendmail_config').slideUp();
            }
        });
        $('#overselling').change(function () {
            if ($(this).val() == 1) {
                if ($('#accounting_method').select2("val") != 2) {
                    bootbox.alert('<?=lang('overselling_will_only_work_with_AVCO_accounting_method_only')?>');
                    $('#accounting_method').select2("val", '2');
                }
            }
        });
        $('#accounting_method').change(function () {
            var oam = <?=$Settings->accounting_method?>, nam = $(this).val();
            if (oam != nam) {
                bootbox.alert('<?=lang('accounting_method_change_alert')?>');
            }
        });
        $('#accounting_method').change(function () {
            if ($(this).val() != 2) {
                if ($('#overselling').select2("val") == 1) {
                    bootbox.alert('<?=lang('overselling_will_only_work_with_AVCO_accounting_method_only')?>');
                    $('#overselling').select2("val", 0);
                }
            }
        });
        $('#item_addition').change(function () {
            if ($(this).val() == 1) {
                bootbox.alert('<?=lang('product_variants_feature_x')?>');
            }
        });
        var sac = $('#sac').val()
        if(sac == 1) {
            $('.nsac').slideUp();
        } else {
            $('.nsac').slideDown();
        }
        $('#sac').change(function () {
            if ($(this).val() == 1) {
                $('.nsac').slideUp();
            } else {
                $('.nsac').slideDown();
            }
        });
    });
</script>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-cog"></i> Google Tag Manager</h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">

                <p class="introtext"><?= lang('update_info'); ?></p>

                <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
                echo admin_form_open_multipart("system_settings", $attrib);
                ?>
                <div class="row">
                    <div class="col-lg-12">

                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border">
                                <label class="control-label">Google Analytics Settings Body</legend>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Google Analytics Script Body</label>
                                    <div class="form-control" style="height: auto;" id="google_analytisc_script">
                                        <code>
                                            <?= htmlentities($Settings->google_analytic_script);?>
                                        </code>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <a class="btn btn-primary btn-lg cpointer" data-toggle="modal" data-target="#myModalImage">Update Code</a>
                                </div>
                            </div>
                        </fieldset>

                        <fieldset class="scheduler-border">
                            <legend class="scheduler-border">
                                <label class="control-label">Google Analytics Settings Head</legend>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Google Analytics Script Head</label>
                                    <div class="form-control" style="height: auto;" id="google_analytisc_script_head">
                                        <code>
                                            <?= htmlentities($Settings->google_analytic_script_head);?>
                                        </code>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <a class="btn btn-primary btn-lg cpointer" data-toggle="modal" data-target="#myModalImage_head">Update Code</a>
                                </div>
                            </div>
                        </fieldset>

                    </div>
                </div>

                <?= form_close(); ?>
            </div>
        </div>

    </div>
</div>
<style>
    .redactor_box textarea{
        background-color: #ffffff;!important;
    }
</style>
<div class="modal fade" id="myModalImage" tabindex="-1" role="dialog" aria-labelledby="myModalImage" aria-hidden="true">
    <div class="modal-dialog cus_sizechart_modal_width">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="myModalLabel">Google Analytical Script Update</h3>
            </div>
            <div class="modal-body">
                <div role="tabpanel" >
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Google analytics</label>
                            <textarea name="" id="google_analytic_script" class="">
                                  </textarea>

                        </div>
                    </div>
                    <div class=" col-md-12 form-group">
                        <a class="btn btn-success pull-right google_analytic_form"> Save</a>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModalImage_head" tabindex="-1" role="dialog" aria-labelledby="myModalImage" aria-hidden="true">
    <div class="modal-dialog cus_sizechart_modal_width">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="myModalLabel">Google Analytical Script Head Update</h3>
            </div>
            <div class="modal-body">
                <div role="tabpanel" >
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Google analytics</label>
                            <textarea name="" id="google_analytic_script_head" class="" >
                                  </textarea>

                        </div>
                    </div>
                    <div class=" col-md-12 form-group">
                        <a class="btn btn-success pull-right google_analytic_form_head"> Save</a>
                    </div>
                    <div class="clearfix"></div>

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#invoice_view').change(function(e) {
            if ($(this).val() == 2) {
                $('#states').show();
            } else {
                $('#states').hide();
            }
        });
        if ($('#invoice_view').val() == 2) {
            $('#states').show();
        } else {
            $('#states').hide();
        }

        $('.google_analytic_form').on('click',function(e){

            var google_analytic_script =  $('#google_analytic_script').val();

            if(google_analytic_script){
                $.ajax({
                    url: '<?= base_url(); ?>admin/marketing/savegooglecode',
                    type: 'POST',
                    async: true,
                    data: {gdata: google_analytic_script},
                    success: function (data) {
                        location.reload();
                    },
                    error: function () {
                        sa_alert('Error!', 'Ajax call failed, please try again or contact site owner.', 'error', true);
                    }
                });
            }else{
            }
            e.preventDefault();
            return false;
        });
        $('.google_analytic_form_head').on('click',function(e){

            var google_analytic_script_head =  $('#google_analytic_script_head').val();

            if(google_analytic_script_head){
                $.ajax({
                    url: '<?= base_url(); ?>admin/marketing/savegooglecode_head',
                    type: 'POST',

                    dataType: "script",
                    data: {gdata_head: google_analytic_script_head},
                    success: function (data) {
                        location.reload();
                    },
                    error: function () {
                        sa_alert('Error!', 'Ajax call failed, please try again or contact site owner.', 'error', true);
                    }
                });
            }else{
            }
            e.preventDefault();
            return false;
        });

        $('#google_analytic_script').redactor({
                //  toolbar: false,
                buttons: ['html'],
                focus: true

            },
        );
        $('#google_analytic_script_head').redactor({
                //  toolbar: false,
                buttons: ['html'],
                focus: true

            },
        );

        $('#myModalImage').on('shown.bs.modal', function (e) {
            if ($('.redactor_btn').first().hasClass("redactor_act")) {
                jQuery(".redactor_btn").first()[0].click();
                jQuery(".redactor_btn").first()[0].click();
            }else{
            jQuery(".redactor_btn").first()[0].click();
            }
        });
        $('#myModalImage_head').on('shown.bs.modal', function (e) {
            if ($('.redactor_btn').eq(1).hasClass("redactor_act")) {
                jQuery(".redactor_btn")[1].click();
                jQuery(".redactor_btn")[1].click();
            }else{
                jQuery(".redactor_btn")[1].click();
            }
        });
    });

</script>
