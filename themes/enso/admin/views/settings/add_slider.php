<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel">Add Slider</h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open("system_settings/add_slider", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <label>Slider Name*</label>
                <?= form_input('name', set_value('name'), 'class="form-control tip gen_slug" id="name" required="required"'); ?>
            </div>

            <div class="form-group">
                <label>Short Code </label>
                <?= form_input('short_code', set_value('short_code'), 'class="form-control tip" id="slug" '); ?>
            </div>


            <div class="form-group">
                <label>Status</label>
              <select name="status" style="width: 100%">
                  <option value="1"> Enabled </option>
                  <option value="0"> Disabled </option>
              </select>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_submit('add_slider', 'Add Slider', 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>
<?= $modal_js ?>

<script type="text/javascript">
    $(document).ready(function () {
        $('.gen_slug').change(function (e) {
            getSlug($(this).val(), 'products');
        });
    });
</script>
