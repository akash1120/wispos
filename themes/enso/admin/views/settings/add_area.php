<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel">Add area</h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open("system_settings/add_area", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <label>Area Name*</label>
                <?= form_input('name', set_value('name'), 'class="form-control tip" id="name" required="required"'); ?>
            </div>

            <div class="form-group">
                <label>Area Code </label>
                <?= form_input('code', set_value('code'), 'class="form-control tip" id="code" '); ?>
            </div>


            <div class="form-group">
                <label>Country</label>
              <select name="country_id" style="width: 100%">
                  <?php
                 foreach ($countries as $country) {
                     ?>
                     <option value="<?php echo  $country->country_id ?>"> <?php echo  $country->name ?> </option>
                  <?php
                 }
                 ?>
              </select>
            </div>
            <div class="form-group">
                <label>Status</label>
              <select name="status" style="width: 100%">
                  <option value="1"> Enabled </option>
                  <option value="0"> Disabled </option>
              </select>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_submit('add_area', 'Add Area', 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>
<?= $modal_js ?>
