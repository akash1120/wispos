<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="box">
    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-home"></i><?= $page_title ?></h2>


    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-4">
                <div class="list-group">
                    <a href="<?= admin_url('system_settings/countries') ?>" class="list-group-item ">
                        <h1 class="list-group-item-heading">Countries</h1>
                        <p>Add and update country detail here</p>
                    </a>
                </div>

            </div>
            <div class="col-lg-4">
                <div class="list-group">
                    <a href="<?= admin_url('system_settings/areas') ?>" class="list-group-item ">
                        <h1 class="list-group-item-heading">Areas</h1>
                        <p>Add and update area detail here</p>
                    </a>
                </div>

            </div>
            <div class="col-lg-4">
                <div class="list-group">
                    <a href="<?= admin_url('system_settings/zones') ?>" class="list-group-item ">
                        <h1 class="list-group-item-heading">Zones</h1>
                        <p>Add and update zone detail here</p>
                    </a>
                </div>

            </div>

        </div>
    </div>
</div>

