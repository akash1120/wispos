<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel">Edit area</h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open("system_settings/edit_area/" . $id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <label>Area Name* </label>
                <input type="text" class="form-control" name="name" id="name" value="<?= $area->name ?>" required>
            </div>

            <div class="form-group">
                <label>Area Code </label>
                <?= form_input('code', set_value('code', $area->code), 'class="form-control tip" id="code" '); ?>
            </div>


            <div class="form-group">
                <label>Country</label>
              <select name="country_id" style="width: 100%">
                  <?php
                 foreach ($countries as $country) {
                     ?>
                     <option value="<?php echo  $country->country_id ?>" <?php if($area->country_id == $country->country_id ){ echo 'selected';}?>> <?php echo  $country->name ?> </option>
                  <?php
                 }
                 ?>
              </select>
            </div>
            <div class="form-group">
                <label>Status</label>
              <select name="status" style="width: 100%">
                  <option value="1" <?php if($area->status == 1) echo 'selected';?>> Enabled </option>
                  <option value="0" <?php if($area->status == 0) echo 'selected';?>> Disabled </option>
              </select>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_submit('edit_area', 'Edit Area', 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>
<?= $modal_js ?>
