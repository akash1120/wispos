<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel">Edit Slider</h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open("system_settings/edit_slider/" . $id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <label>Name* </label>
                <input type="text" class="form-control" name="name" id="name" value="<?= $slider->name ?>" required>
            </div>

            <div class="form-group">
                <label>Short Code </label>
                <?= form_input('short_code', set_value('short_code', $slider->short_code), 'class="form-control tip" id="slug" readonly'); ?>
            </div>


            <div class="form-group">
                <label>Status</label>
              <select name="status" style="width: 100%">
                  <option value="1" <?php if($slider->status == 1) echo 'selected';?>> Enabled </option>
                  <option value="0" <?php if($slider->status == 0) echo 'selected';?>> Disabled </option>
              </select>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_submit('edit_slider', 'Edit Slider', 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>
<?= $modal_js ?>
