<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<link href="<?= $assets ?>styles/dropzone.css" rel="stylesheet"/>
<script src="<?= $assets ?>js/dropzone.js"></script>

<?php
//Get product sizes(Extras)
$sizes_data = $this->site->getAllSizeGuide();

//GEt selected authors
$get_selected_authors = $this->site->selectedAuthors($product->id);
//print_r($get_selected_authors);
//die;
if (!empty($variants)) {
    foreach ($variants as $variant) {
        $vars[] = addslashes($variant->name);
    }
} else {
    $vars = array();
}
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('.gen_slug').change(function (e) {
            getSlug($(this).val(), 'products');
        });
        $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_category_to_load') ?>").select2({
            placeholder: "<?= lang('select_category_to_load') ?>", data: [
                {id: '', text: '<?= lang('select_category_to_load') ?>'}
            ]
        });
        $('#category').change(function () {
            var v = $(this).val();
            $('#modal-loading').show();
            if (v) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: "<?= admin_url('products/getSubCategories') ?>/" + v,
                    dataType: "json",
                    success: function (scdata) {
                        if (scdata != null) {
                            scdata.push({id: '', text: '<?= lang('select_subcategory') ?>'});
                            $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
                                placeholder: "<?= lang('select_category_to_load') ?>",
                                minimumResultsForSearch: 7,
                                data: scdata
                            });
                        } else {
                            $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('no_subcategory') ?>").select2({
                                placeholder: "<?= lang('no_subcategory') ?>",
                                minimumResultsForSearch: 7,
                                data: [{id: '', text: '<?= lang('no_subcategory') ?>'}]
                            });
                        }
                    },
                    error: function () {
                        bootbox.alert('<?= lang('ajax_error') ?>');
                        $('#modal-loading').hide();
                    }
                });
            } else {
                $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_category_to_load') ?>").select2({
                    placeholder: "<?= lang('select_category_to_load') ?>",
                    minimumResultsForSearch: 7,
                    data: [{id: '', text: '<?= lang('select_category_to_load') ?>'}]
                });
            }
            $('#modal-loading').hide();
        });
        $('#code').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                return false;
            }
        });
    });
</script>
<style>
   .select2-locked{
      padding: 6px !important;
      padding-left: 12px !important;
   }
    .form-control {
        border-radius: 3px !important;
    }

    .padding10 {
        padding: 10px !important;
    }

    .border_radius3 {
        border-radius: 3px !important;
    }

    .redactor_box, .redactor_toolbar {
        border-radius: 3px !important;
    }

    .grey_background {
        background: #FAFAF9 !important;
    }

    .image_padding {
        padding-top: 20px !important;
        padding-left: 0px !important;
        padding-right: 0px !important;
    }

    .margintop15 {
        margin-top: 15px;
    }

    .paddingleftright0 {
        padding-right: 0px;
        padding-left: 0px;
    }

    .paddingtop15 {
        padding-top: 15px;
    }

    .paddingtop28 {
        padding-top: 28px;
    }
    .dz-progress {
        /* progress bar covers file name */
        /*display: none !important;*/
    }

    .dz-image img {
        width: 100% !important;
        height: 100% !important;
    }

    .dz-size {
        display: none !important;
    }
    .dropzone .dz-preview .dz-image{
        max-width: 115px !important;
        max-height: 115px !important;
    }
    .cpointer{
        cursor: pointer;
    }
</style>
<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 48px;
        height: 24px;
    }

    .switch input {display:none;}

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 22px;
        width: 22px;
        left: 0px;
        bottom: 1px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #00BA70;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #00BA70;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>
<div class="box">

    <div class="box-header">
        <h2 class="blue"><i class="fa-fw fa fa-edit"></i><?= lang('edit_product'); ?></h2>
    </div>
    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <p class="introtext"><?php echo lang('update_info'); ?></p>
                <?php
                $attrib = array('data-toggle' => 'validator', 'role' => 'form');
                echo admin_form_open_multipart("products/edit/" . $product->id, $attrib)
                ?>
                <div class="col-md-7">
                    <div class="panel panel-default padding10 border_radius3">
                        <h2 class="blue">General Detail</h2>
                        <input type="hidden" id="product_saved_id"
                               value="<?php echo $product->id; ?>">
                        <div class="form-group">
                            <?= lang("product_type", "type") ?>
                            <?php
                            $opts = array('standard' => lang('standard'), 'combo' => lang('combo'), 'digital' => lang('digital'), 'service' => lang('service'));
                            echo form_dropdown('type', $opts, (isset($_POST['type']) ? $_POST['type'] : ($product ? $product->type : '')), 'class="form-control" id="type" required="required"');
                            ?>
                        </div>
                        <div class="form-group all">
                            <?= lang("product_name", "name") ?>
                            <?= form_input('name', (isset($_POST['name']) ? $_POST['name'] : ($product ? $product->name : '')), 'class="form-control gen_slug" id="name" required="required"'); ?>
                        </div>
                        <div class="form-group all">
                            <?= lang('slug', 'slug'); ?>
                            <?= form_input('slug', set_value('slug', ($product ? $product->slug : '')), 'class="form-control tip" id="slug" required="required"'); ?>
                        </div>
                        <div class="form-group all">
                            <?= lang("product_code", "code") ?>
                            <?= form_input('code', (isset($_POST['code']) ? $_POST['code'] : ($product ? $product->code : '')), 'class="form-control" id="code"  required="required"') ?>
                            <span class="help-block"><?= lang('you_scan_your_barcode_too') ?></span>
                        </div>
                        <div class="form-group all hidden">
                            <?= lang("translate_product_name", "translated_name") ?>
                            <?= form_input('translated_name', (isset($_POST['translated_name']) ? $_POST['translated_name'] : ($product ? $product->translated_name : '')), 'class="form-control" id="translated_name" '); ?>
                        </div>
                        <div class="form-group all hidden">
                            <?= lang('second_name', 'second_name'); ?>
                            <?= form_input('second_name', set_value('second_name', ($product ? $product->second_name : '')), 'class="form-control tip" id="second_name"'); ?>
                        </div>
                        <div class="form-group all">
                            <?= lang("product_details", "product_details") ?>
                            <?= form_textarea('product_details', (isset($_POST['product_details']) ? $_POST['product_details'] : ($product ? $product->product_details : '')), 'class="form-control" id="details"'); ?>
                        </div>
                    </div>

                    <div class="panel panel-default padding10 border_radius3">
                        <h2 class="blue">Images</h2>
                        <div class="form-group1">
                            <?php
                          
                            $image_product_saved = 'ucloud/plugins/filepreviewer/site/resize_image_inline.php?f='.$product->image.'&w=160&h=134&m=medium&uh=&o=jpg';
                            ?>
                            <div class="form-group all col-md-7 image_padding">
                                <?= lang("product_front_image", "product_image") ?>
                                <div class="input-group">
                                    <input type="hidden" id="pimage_id" name="image" readonly value="<?= $product->image ?>" class="form-control mediaimage_id" placeholder="Select Image">
                                    <input type="text" id="pimage"  readonly value="<?= $image_product_saved ?>" class="form-control mediaimage" placeholder="Select Image">
                                    <span class="input-group-btn ">
                                    <button id="mediagallery" class="btn btn-primary mediagallery width120" type="button">Select Image</button>
                                </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label for="main_image" id="main_image" class="img_gallery_class mainboxmain_image" ><img id="main_image" class="img-thumbnail  admin_product_imgsize boxmain_image" alt="" src="<?= base_url($image_product_saved); ?>"></label>
                            </div>
                            <input class="alt_text" name="image_alt" value="<?= $product->image_alt;?>" type="hidden">
                            <div class="col-md-3 paddingtop55">
                                <label><input type="checkbox" name="image_remove" > Remove</label>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group1">
                            <?php


                            $image_product_back_saved = 'ucloud/plugins/filepreviewer/site/resize_image_inline.php?f='.$product->back_image.'&w=160&h=134&m=medium&uh=&o=jpg';


                            ?>

                            <div class="form-group all col-md-7 image_padding">
                                <?= lang("product_back_image", "back_image") ?>
                                <div class="input-group">
                                    <input type="hidden" id="pbimage_id" name="back_image" readonly value="<?= $product->back_image ?>" class="form-control mediaimage_id" placeholder="Select Image">
                                    <input type="text" id="pbimage"  readonly value="<?= $image_product_back_saved ?>" class="form-control mediaimage" placeholder="Select Image">
                                    <span class="input-group-btn ">
                                    <button id="mediagallery" class="btn btn-primary mediagallery width120" type="button">Select Image</button>
                                </span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label for="main_image" id="main_image" class="img_gallery_class mainboxmain_image" ><img id="main_image" class="img-thumbnail admin_product_imgsize boxmain_image" alt="" src="<?= base_url($image_product_back_saved); ?>"></label>
                            </div>
                            <input class="alt_text" name="back_image_alt" value="<?= $product->back_image_alt; ?>" type="hidden">
                            <div class="col-md-3 paddingtop55">
                                <label><input type="checkbox" name="back_image_remove" > Remove</label>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group all text-writing">
                            <label for="product_image">Gallery Images</label>
                        </div>

                        <?php
                        $conutimag=1;
                        if(!empty($galleryimages)){
                            foreach ($galleryimages as $ky => $img_data){
                                $image_product_gal_saved = 'ucloud/plugins/filepreviewer/site/resize_image_inline.php?f='.$img_data->photo.'&w=160&h=134&m=medium&uh=&o=jpg';
                                ?>

                                <div class="form-group1 gallery_wrapper">
                                    <div class="form-group all col-md-7" style="padding-top: 50px; padding-left: 0px;">
                                        <div class="input-group">
                                            <input type="hidden" id="gimage_id<?= $conutimag;?>" name="gimage[]" readonly  value="<?= $img_data->photo ?>" class="form-control mediaimage_id" placeholder="Select Image">
                                            <input type="text" id="gimage<?= $conutimag;?>" name="" readonly  value="<?= $image_product_gal_saved; ?>" class="form-control mediaimage" placeholder="Select Image">
                                            <span class="input-group-btn ">
                                    <button id="mediagallery" class="btn btn-primary mediagallery width120" type="button">Select Image</button>
                                </span>
                                        </div>
                                    </div>
                                    <div class="col-md-2 ">
                                        <label for="main_image" id="main_image" class="img_gallery_class mainboxmain_image" ><img id="main_image" class="img-thumbnail admin_product_imgsize boxmain_image" alt="" src="<?= base_url($image_product_gal_saved); ?>"></label>
                                    </div>
                                    <input class="alt_text" name="gimage_alt[]" value="<?= $img_data->alt_text;?>" type="hidden">
                                    <div class="col-md-3" style="padding-top: 50px">
                                        <a class="btn btn-success  remVargallery" ><i class="fa fa-trash"></i></a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>



                                <?php
                                $conutimag++;
                            }

                        }else{
                            ?>
                            <div class="form-group all text-writing">
                                <div class="input-group">
                                    <input type="hidden" id="gimage1_id" name="gimage[]" class="form-control mediaimage_id" placeholder="Select Image">
                                    <input type="text" id="gimage1"  class="form-control mediaimage" placeholder="Select Image">
                                    <span class="input-group-btn ">
                                    <button id="mediagallery" class="btn btn-primary mediagallery width120" type="button">Select Image</button>
                                </span>
                                </div>
                                <input class="alt_text" name="gimage_alt[]" value="" type="hidden">
                            </div>
                        <?php } ?>



                        <div id="newgalleryimage"></div>

                        <a class="btn btn-success" id="add_more_gallery_image"> Add More Gallery Image</a>



                        <!--   <div class="form-group all">
                            <?/*= lang("product_gallery_images", "images") */?>
                            <input id="images" type="file" data-browse-label="<?/*= lang('browse'); */?>" name="userfile[]"
                                   multiple="true" data-show-upload="false"
                                   data-show-preview="false" class="form-control file" accept="image/*">
                        </div>
                        <div id="img-details"></div>
                        <div class="gallery_bucket">
                            <?php /*if ($images) { */?>
                                <strong><p>Select Images to delete</p></strong>
                            <?php /*} */?>
                            <?php
                        /*                            foreach ($images as $ph) {
                                                        echo '<span class="hide"><input class="gallery_multiple" name="multiple_del[]" type="checkbox" value="' . $ph->id . '" id="' . $ph->id . '" /></span>';
                                                        echo '<label for="' . $ph->id . '" id="' . $ph->id . '" class="img_gallery_class mainbox' . $ph->id . '" ><img id="' . $ph->id . '" class="img-thumbnail yes-delete admin_product_imgsize box' . $ph->id . '" alt="" src="' . base_url('assets/uploads/thumbs/' . $ph->photo) . '"></label>';
                                                    }
                                                    */?>
                        </div>-->

                    </div>
                    <div class="panel panel-default padding10 border_radius3">
                        <h2 class="blue">Price & Tax Details </h2>
                        <?php if ($Owner || $Admin) { ?>
                            <div class="form-group standard">
                                <?= lang("product_cost", "cost") ?>
                                <?= form_input('cost', (isset($_POST['cost']) ? $_POST['cost'] : ($product ? $this->sma->formatDecimal($product->cost) : '')), 'class="form-control tip" id="cost" ') ?>
                            </div>
                        <?php } ?>

                        <div class="form-group all">
                            <?= lang("product_price", "price") ?>
                            <?= form_input('price', (isset($_POST['price']) ? $_POST['price'] : ($product ? $this->sma->formatDecimal($product->price) : '')), 'class="form-control tip" id="price" required="required"') ?>
                        </div>

                        <div class="form-group">
                            <input type="checkbox" class="checkbox" value="1" name="promotion"
                                   id="promotion" <?= $this->input->post('promotion') ? 'checked="checked"' : ''; ?>>
                            <label for="promotion" class="padding05">
                                <?= lang('promotion'); ?>
                            </label>
                        </div>

                        <div id="promo"<?= $product->promotion ? '' : ' style="display:none;"'; ?>>
                            <div class="well well-sm border_radius3">
                                <div class="form-group">
                                    <?= lang('promo_price', 'promo_price'); ?>
                                    <?= form_input('promo_price', set_value('promo_price', $product->promo_price ? $this->sma->formatMoney($product->promo_price) : ''), 'class="form-control tip" id="promo_price"'); ?>
                                </div>
                                <div class="form-group">
                                    <?= lang('start_date', 'start_date'); ?>
                                    <?= form_input('start_date', set_value('start_date', $product->start_date ? $this->sma->hrsd($product->start_date) : ''), 'class="form-control tip date" id="start_date"'); ?>
                                </div>
                                <div class="form-group">
                                    <?= lang('end_date', 'end_date'); ?>
                                    <?= form_input('end_date', set_value('end_date', $product->end_date ? $this->sma->hrsd($product->end_date) : ''), 'class="form-control tip date" id="end_date"'); ?>
                                </div>
                            </div>
                        </div>

                        <?php if ($Settings->invoice_view == 2) { ?>
                            <div class="form-group">
                                <?= lang('hsn_code', 'hsn_code'); ?>
                                <?= form_input('hsn_code', set_value('hsn_code', ($product ? $product->hsn_code : '')), 'class="form-control" id="hsn_code"'); ?>
                            </div>
                        <?php } ?>

                        <?php if ($Settings->tax1) { ?>
                            <div class="form-group all">
                                <?= lang('product_tax', "tax_rate") ?>
                                <?php
                                $tr[""] = "";
                                foreach ($tax_rates as $tax) {
                                    $tr[$tax->id] = $tax->name;
                                }
                                echo form_dropdown('tax_rate', $tr, (isset($_POST['tax_rate']) ? $_POST['tax_rate'] : ($product ? $product->tax_rate : $Settings->default_tax_rate)), 'class="form-control select" id="tax_rate" placeholder="' . lang("select") . ' ' . lang("product_tax") . '" style="width:100%"')
                                ?>
                            </div>
                            <div class="form-group all">
                                <?= lang("tax_method", "tax_method") ?>
                                <?php
                                $tm = array('0' => lang('inclusive'), '1' => lang('exclusive'));
                                echo form_dropdown('tax_method', $tm, (isset($_POST['tax_method']) ? $_POST['tax_method'] : ($product ? $product->tax_method : '')), 'class="form-control select" id="tax_method" placeholder="' . lang("select") . ' ' . lang("tax_method") . '" style="width:100%"')
                                ?>
                            </div>
                        <?php } ?>
                        <div class="form-group standard">
                            <?= lang("alert_quantity", "alert_quantity") ?>
                            <div
                                    class="input-group"> <?= form_input('alert_quantity', (isset($_POST['alert_quantity']) ? $_POST['alert_quantity'] : ($product ? $this->sma->formatDecimal($product->alert_quantity) : '')), 'class="form-control tip" id="alert_quantity"') ?>
                                <span class="input-group-addon">
                            <input type="checkbox" name="track_quantity" id="inlineCheckbox1"
                                   value="1" <?= ($product ? (!empty($product->track_quantity) ? 'checked="checked"' : '') : 'checked="checked"') ?>>
                        </span>
                            </div>
                        </div>
                    </div>

                    <div class="panel panel-default hidden padding10 border_radius3">
                        <h2 class="blue"></h2>
                        <div class="form-group all">
                            <?= lang("barcode_symbology", "barcode_symbology") ?>
                            <?php
                            $bs = array('code25' => 'Code25', 'code39' => 'Code39', 'code128' => 'Code128', 'ean8' => 'EAN8', 'ean13' => 'EAN13', 'upca' => 'UPC-A', 'upce' => 'UPC-E');
                            echo form_dropdown('barcode_symbology', $bs, (isset($_POST['barcode_symbology']) ? $_POST['barcode_symbology'] : ($product ? $product->barcode_symbology : 'code128')), 'class="form-control select" id="barcode_symbology" required="required" style="width:100%;"');
                            ?>
                        </div>

                    </div>


                </div>
                <div class="col-md-5">


                    <div class="panel panel-default padding10 border_radius3">
                        <h2 class="blue">Category Detail</h2>
                        <div class="form-group all">
                            <?= lang("brand", "brand") ?>
                            <?php
                            $br[''] = "";
                            foreach ($brands as $brand) {
                                $br[$brand->id] = $brand->name;
                            }

                            echo form_dropdown('brand', $br, (isset($_POST['brand']) ? $_POST['brand'] : ($product ? $product->brand : '1')), 'class="form-control select" id="language" placeholder="' . lang("select") . " " . lang("language") . '" required="required" style="width:100%"')
                            ?>
                        </div>
                        <div class="form-group all">
                            <?= lang("category", "category") ?>
                            <?php
                            $cat[''] = "";
                            foreach ($categories as $category) {
                                $cat[$category->id] = $category->name;
                            }
                            echo form_dropdown('category', $cat, (isset($_POST['category']) ? $_POST['category'] : ($product ? $product->category_id : '')), 'class="form-control select" id="category" placeholder="' . lang("select") . " " . lang("category") . '" required="required" style="width:100%"')
                            ?>
                        </div>
                        <div class="form-group all">
                            <?= lang("subcategory", "subcategory") ?>
                            <div class="controls" id="subcat_data"> <?php
                                echo form_input('subcategory', ($product ? $product->subcategory_id : ''), 'class="form-control" id="subcategory"  placeholder="' . lang("select_category_to_load") . '"');
                                ?>
                            </div>
                        </div>





                        <div class="form-group standard_combo">
                            <?= lang('weight', 'weight'); ?>
                            <?= form_input('weight', set_value('weight', ($product ? $product->weight : '')), 'class="form-control tip" id="weight"'); ?>
                        </div>

                        <div class="form-group col-md-5 paddingleftright0 standard_combo">
                            <?= lang('dimensions', 'dimensions'); ?>
                            <?= form_input('width', set_value('width', ($product ? $product->width : '')), 'class="form-control col-md-6 tip" id="width" placeholder="Width"'); ?>
                        </div>
                        <div class="col-md-2 text-center paddingleftright0 paddingtop15">
                            <h2>x</h2>
                        </div>
                        <div class="form-group col-md-5 paddingleftright0 paddingtop28 standard_combo">
                            <?= form_input('height', set_value('height', ($product ? $product->height : '')), 'class="form-control col-md-6 tip" id="height"  placeholder="Height"'); ?>
                        </div>
                        <div class="clearfix"></div>





                    </div>

                    <div class="panel panel-default hidden padding10 border_radius3">
                        <h2 class="blue">Units Detail</h2>

                        <div class="form-group standard ">
                            <?= lang('product_unit', 'unit'); ?>
                            <?php
                            $pu[''] = lang('select') . ' ' . lang('unit');
                            foreach ($base_units as $bu) {
                                $pu[$bu->id] = $bu->name . ' (' . $bu->code . ')';
                            }
                            ?>
                            <?= form_dropdown('unit', $pu, set_value('unit', $product->unit), 'class="form-control tip" id="unit" style="width:100%;"'); ?>
                        </div>
                        <div class="form-group standard">
                            <?= lang('default_sale_unit', 'default_sale_unit'); ?>
                            <?php
                            $uopts[''] = lang('select') . ' ' . lang('unit');
                            foreach ($subunits as $sunit) {
                                $uopts[$sunit->id] = $sunit->name . ' (' . $sunit->code . ')';
                            }
                            ?>
                            <?= form_dropdown('default_sale_unit', $uopts, $product->sale_unit, 'class="form-control" id="default_sale_unit" style="width:100%;"'); ?>
                        </div>
                        <div class="form-group standard">
                            <?= lang('default_purchase_unit', 'default_purchase_unit'); ?>
                            <?= form_dropdown('default_purchase_unit', $uopts, $product->purchase_unit, 'class="form-control" id="default_purchase_unit" style="width:100%;"'); ?>
                        </div>

                    </div>
                    <div class="panel panel-default padding10 border_radius3">
                        <h2 class="blue">Feature Options</h2>

                        <div class="form-group">
                            <input name="approved_by_nmc" type="checkbox" class="checkbox" id="approved_by_nmc" value="1" <?= $product->approved_by_nmc == 1 ? 'checked' : '' ?>/>
                            <label for="approved_by_nmc" class="padding05"><?= lang('approved_by_nmc') ?></label>
                        </div>

                        <div class="form-group">
                            <input name="featured" type="checkbox" class="checkbox" id="featured"
                                   value="1" <?= empty($product->featured) ? '' : 'checked="checked"' ?>/>
                            <label for="featured" class="padding05"><?= lang('featured') ?></label>
                        </div>
                        <div class="form-group">
                            <input name="hide" type="checkbox" class="checkbox" id="hide"
                                   value="1" <?= empty($product->hide) ? '' : 'checked="checked"' ?>/>
                            <label for="hide" class="padding05"><?= lang('hide_in_shop') ?></label>
                        </div>
                        <div class="form-group">
                            <input name="product_freeship" type="checkbox" class="checkbox" id="product_freeship"
                                   value="1" <?= $product->free_shipment == 1 ? 'checked' : '' ?> /><label
                                    for="product_freeship" class="padding05">
                                <?= lang('Free Shipping') ?></label>
                        </div>
                        <div class="form-group ">
                            <input name="show_variable_price_on_shop" type="checkbox" class="checkbox" id="show_variable_price_on_shop"
                                   value="1" <?= $product->show_variable_price_on_shop == 1 ? 'checked' : '' ?> />
                            <label for="show_variable_price_on_shop" class="padding05">Show Variable Price on Shop</label>
                        </div>
                        <div class="form-group ">
                            <input name="cf" type="checkbox" class="checkbox" id="extras" value=""
                                   checked="checked"/><label
                                    for="extras" class="padding05"><?= lang('custom_fields') ?></label>
                        </div>
                        <div class="row " id="extras-con">

                            <div class="col-md-6">
                                <div class="form-group all">
                                    <?= lang('pcf1', 'cf1') ?>
                                    <?= form_input('cf1', (isset($_POST['cf1']) ? $_POST['cf1'] : ($product ? $product->cf1 : '')), 'class="form-control tip" id="cf1"') ?>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group all">
                                    <?= lang('pcf2', 'cf2') ?>
                                    <?= form_input('cf2', (isset($_POST['cf2']) ? $_POST['cf2'] : ($product ? $product->cf2 : '')), 'class="form-control tip" id="cf2"') ?>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group all">
                                    <?= lang('pcf3', 'cf3') ?>
                                    <?= form_input('cf3', (isset($_POST['cf3']) ? $_POST['cf3'] : ($product ? $product->cf3 : '')), 'class="form-control tip" id="cf3"') ?>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group all">
                                    <?= lang('pcf4', 'cf4') ?>
                                    <?= form_input('cf4', (isset($_POST['cf4']) ? $_POST['cf4'] : ($product ? $product->cf4 : '')), 'class="form-control tip" id="cf4"') ?>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group all">
                                    <?= lang('pcf5', 'cf5') ?>
                                    <?= form_input('cf5', (isset($_POST['cf5']) ? $_POST['cf5'] : ($product ? $product->cf5 : '')), 'class="form-control tip" id="cf5"') ?>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group all">
                                    <?= lang('pcf6', 'cf6') ?>
                                    <?= form_input('cf6', (isset($_POST['cf6']) ? $_POST['cf6'] : ($product ? $product->cf6 : '')), 'class="form-control tip" id="cf6"') ?>
                                </div>
                            </div>


                        </div>

                    </div>


                    <div class="standard ">
                        <div>
                            <?php
                            if (!empty($warehouses) || !empty($warehouses_products)) {
                                echo '<div class="row"><div class="col-md-12"><div class="well">';
                                echo '<p><strong>' . lang("warehouse_quantity") . '</strong></p>';
                                if (!empty($warehouses_products)) {
                                    foreach ($warehouses_products as $wh_pr) {
                                        echo '<span class="bold text-info">' . $wh_pr->name . ': <input type="hidden" value="' . $this->sma->formatDecimal($wh_pr->quantity) . '" id="vwh_qty_' . $wh_pr->id . '"><span class="padding05" id="rwh_qty_' . $wh_pr->id . '">' . $this->sma->formatQuantity($wh_pr->quantity) . '</span>' . ($wh_pr->rack ? ' (<span class="padding05" id="rrack_' . $wh_pr->id . '">' . $wh_pr->rack . '</span>)' : '') . '</span><br>';
                                    }
                                }
                                echo '<div class="clearfix"></div></div></div></div>';
                            }
                            ?>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                    <div class="combo " style="display:none;">

                        <div class="form-group">
                            <?= lang("add_product", "add_item") . ' (' . lang('not_with_variants') . ')'; ?>
                            <?php echo form_input('add_item', '', 'class="form-control ttip" id="add_item" data-placement="top" data-trigger="focus" data-bv-notEmpty-message="' . lang('please_add_items_below') . '" placeholder="' . $this->lang->line("add_item") . '"'); ?>
                        </div>
                        <div class="control-group table-group">
                            <label class="table-label" for="combo"><?= lang("combo_products"); ?></label>
                            <!--<div class="row"><div class="ccol-md-10 col-sm-10 col-xs-10"><label class="table-label" for="combo"><?= lang("combo_products"); ?></label></div>
                            <div class="ccol-md-2 col-sm-2 col-xs-2"><div class="form-group no-help-block" style="margin-bottom: 0;"><input type="text" name="combo" id="combo" value="" data-bv-notEmpty-message="" class="form-control" /></div></div></div>-->
                            <div class="controls table-controls">
                                <table id="prTable"
                                       class="table items table-striped table-bordered table-condensed table-hover">
                                    <thead>
                                    <tr>
                                        <th class="col-md-5 col-sm-5 col-xs-5"><?= lang('product') . ' (' . lang('code') . ' - ' . lang('name') . ')'; ?></th>
                                        <th class="col-md-2 col-sm-2 col-xs-2"><?= lang("quantity"); ?></th>
                                        <th class="col-md-3 col-sm-3 col-xs-3"><?= lang("unit_price"); ?></th>
                                        <th class="col-md-1 col-sm-1 col-xs-1 text-center">
                                            <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>

                    </div>

                    <div class="digital" style="display:none;">
                        <?php
                        if (filter_var($product->file, FILTER_VALIDATE_URL) === FALSE) {
                            $file = $product->file;
                            $file_link = '';
                        } else {
                            $file_link = $product->file;
                            $file = '';
                        }
                        ?>
                        <div class="form-group digital">
                            <?= lang("digital_file", "digital_file") ?>
                            <input id="digital_file" type="file" data-browse-label="<?= lang('browse'); ?>"
                                   name="digital_file" data-show-upload="false"
                                   data-show-preview="false" class="form-control file">
                        </div>
                        <div class="form-group digital">
                            <?= lang('file_link', 'file_link'); ?>
                            <?= form_input('file_link', $file_link, 'class="form-control" id="file_link"'); ?>
                        </div>
                    </div>

                    <div class="form-group standard ">
                        <div class="form-group">
                            <?= lang("supplier", "supplier") ?>
                            <button type="button" class="btn btn-primary btn-xs" id="addSupplier"><i
                                        class="fa fa-plus"></i>
                            </button>
                        </div>
                        <div class="row" id="supplier-con">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <?php
                                    echo form_input('supplier', (isset($_POST['supplier']) ? $_POST['supplier'] : ''), 'class="form-control ' . ($product ? '' : 'suppliers') . '" id="' . ($product && !empty($product->supplier1) ? 'supplier1' : 'supplier') . '" placeholder="' . lang("select") . ' ' . lang("supplier") . '" style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <?= form_input('supplier_part_no', (isset($_POST['supplier_part_no']) ? $_POST['supplier_part_no'] : ""), 'class="form-control tip" id="supplier_part_no" placeholder="' . lang('supplier_part_no') . '"'); ?>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <?= form_input('supplier_price', (isset($_POST['supplier_price']) ? $_POST['supplier_price'] : ""), 'class="form-control tip" id="supplier_price" placeholder="' . lang('supplier_price') . '"'); ?>
                                </div>
                            </div>
                        </div>
                        <div id="ex-suppliers"></div>
                    </div>

                </div>


                <div class="col-md-12">
                    <div class="panel panel-default padding10 border_radius3">
                        <h2 class="blue">Varients </h2>

                        <div id="attrs"></div>

                        <div class="form-group">
                            <input type="checkbox" class="checkbox" name="attributes"
                                   id="attributes" <?= $this->input->post('attributes') || $product_options ? 'checked="checked"' : ''; ?>><label
                                    for="attributes"
                                    class="padding05"><?= lang('product_has_attributes'); ?></label> <?= lang('eg_sizes_colors'); ?>
                        </div>

                        <div class="well well-sm border_radius3" id="attr-con"
                             style="<?= $this->input->post('attributes') || $product_options ? '' : 'display:block;'; ?>">
                            <!--  <div class="new_variant">
                                <div class="col-sm-4">
                                    <select  class="form-control variants_dropdown" name="variants[]"
                                             id="variants_dropdown1">
                                        <option value="">Select Varient</option>
                                        <?php /*foreach ($variants as $key => $varient) { */?>
                                            <option value="<?/*= $varient->name */?>"><?/*= $varient->name; */?></option>
                                        <?php /*} */?>
                                    </select>
                                </div>
                                <div class="col-sm-8">
                                    <input id="attributesInput2" class="form-control select-tagse" name="attributesInput2[]"
                                           placeholder="<?/*= $this->lang->line("enter_attributes") */?>">
                                </div>
                            </div>-->


                            <div id="newvariants"></div>

                            <div class="col-sm-12" style="margin-top: 10px;">
                                <a class="btn btn-success" id="add_more_variant"> Add more Variant</a>
                                <a class="btn btn-success" id="gen_combinations">Generate combinations</a>
                            </div>


                            <div class="form-group" id="ui" style="margin-bottom: 0;">
                                <!--<div class="input-group">
                                    <?php /*// echo form_input('attributesInput', '', 'class="form-control select-tags" id="attributesInput" placeholder="' . $this->lang->line("enter_attributes") . '"'); */ ?>
                                    <div class="input-group-addon" style="padding: 2px 5px;">
                                        <a href="#" id="addAttributes">
                                            <i class="fa fa-2x fa-plus-circle" id="addIcon"></i>
                                        </a>
                                    </div>
                                </div>-->
                                <div style="clear:both;"></div>
                            </div>

                            <div class="table-responsive" style=" padding-left: 15px; padding-right: 15px;">
                                <table id="attrTable" class="table table-bordered table-condensed table-striped"
                                       style="<?= $this->input->post('attributes') || $product_variants ? '' : 'display:none;'; ?>margin-bottom: 0; margin-top: 10px;">
                                    <thead>
                                    <tr class="active">
                                        <th>Variant Name</th>
                                        <th>Supplier Code</th>
                                        <th>Supplier Price</th>
                                        <th>Price Addition</th>
                                        <th>Weight (Kg)</th>
                                        <th>Website Display</th>
                                        <th><i class="fa fa-times attr-remove-all1"></i></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $html1='';
                                    if ($product_variants) {

                                        foreach ($product_variants as $pv) {
                                            if($pv->del_check == 1){
                                                //   echo '<tr class="attr1"><td><input type="hidden" name="attr_name[]" value="' . $pv->name . '"><span>' . $pv->name . '</span></td><td class="code text-center form-group"><input type="text"  class="form-control" name="attr_supplier_code[]" value="' . $pv->supplier_code . '"></td><td class="supplier_price text-center form-group"><input type="text"  class="form-control" name="attr_supplier_price[]" value="' . $pv->cost . '"></td><td class="price text-center form-group"><input type="text" min="0.00" class="form-control" name="attr_price[]" value="' . $pv->price . '"></span></td><td class="text-center"><i class="fa fa-times delAttr"></i></td></tr>';
                                            }else{

                                                $html.= '<tr class="attr1"><td><input type="hidden" name="attr_name[]"  value="' . $pv->name . '"><span>' . $pv->name . '</span></td><td class="code text-center form-group"><input type="text" name="attr_supplier_code[]"  class="form-control" value="' . $pv->supplier_code . '" ></td><td class="supplier_price text-center form-group"><input type="text"  class="form-control"  name="attr_supplier_price[]"  value="' . $pv->cost . '"></td><td class="price text-center form-group"><input type="text" name="attr_price[]" min="0.00" class="form-control"  value="' . $pv->price . '"></span></td>';

                                                $html .= '<td class="price text-center form-group"><input required type="text" name="attr_weight[]" min="0.00" class="form-control"  value="' . $pv->weight . '"></span></td>';
                                                if($pv->website_status == 1) {
                                                    $html .= '<td class="price text-center form-group tdr"><label class="switch"> <input type="checkbox" class="slidet" checked  > <span class="slider round"></span> </label>  <input id="website_status_val"  type="hidden" value="'.$pv->website_status.'"  name="website_status[]"  > </td>';
                                                }else {
                                                    $html .= '<td class="price text-center form-group tdr"><label class="switch"> <input type="checkbox" class="slidet"  > <span class="slider round"></span> </label>  <input id="website_status_val" type="hidden" value="0" name="website_status[]"> </td>';
                                                }

                                                $html.= '<td class="text-center"><i class="fa fa-times"></i></td></tr>';




                                            }
                                            //  echo '<tr><td class="col-xs-3"><input type="hidden" name="variant_id_' . $pv->id . '" value="' . $pv->id . '"><input type="text" name="variant_name_' . $pv->id . '" value="' . $pv->name . '" class="form-control"></td><td class="price text-right col-xs-2"><input type="text" name="variant_price_' . $pv->id . '" value="' . $pv->price . '" class="form-control"></td></tr>';
                                        }echo $html;
                                    }
                                    ?>
                                    </tbody>
                                    <tbody id="tbodayreq">
                                    <?php
                                    $html='';
                                    if ($product_variants) {

                                        foreach ($product_variants as $pv) {
                                            if($pv->del_check == 1){
                                                $html.= '<tr class="attr1"><td><input type="hidden" name="attr_name[]" value="' . $pv->name . '"><span>' . $pv->name . '</span></td><td class="code text-center form-group"><input type="text"  class="form-control" name="attr_supplier_code[]" value="' . $pv->supplier_code . '"></td><td class="supplier_price text-center form-group"><input type="text"  class="form-control" name="attr_supplier_price[]" value="' . $pv->cost . '"></td><td class="price text-center form-group"><input type="text" min="0.00" class="form-control" name="attr_price[]" value="' . $pv->price . '"></span></td>';

                                                $html .= '<td class="price text-center form-group"><input required type="text" name="attr_weight[]" min="0.00" class="form-control"  value="' . $pv->weight . '"></span></td>';
                                                if($pv->website_status == 1) {
                                                    $html .= '<td class="price text-center form-group tdr"><label class="switch"> <input type="checkbox" class="slidet"  checked> <span class="slider round"></span> </label>  <input id="website_status_val" type="hidden" value="'.$pv->website_status.'" name="website_status[]" > </td>';
                                                }else {
                                                    $html .= '<td class="price text-center form-group tdr"><label class="switch"> <input type="checkbox" class="slidet"  > <span class="slider round"></span> </label>  <input id="website_status_val" type="hidden" value="0" name="website_status[]"> </td>';
                                                }

                                                $html.= '<td class="text-center"><i class="fa fa-times delAttr"></i></td></tr>';

                                            }else{
                                                //  echo '<tr class="attr1"><td><input type="hidden"  value="' . $pv->name . '"><span>' . $pv->name . '</span></td><td class="code text-center form-group"><input type="text"  class="form-control" value="' . $pv->supplier_code . '" disabled></td><td class="supplier_price text-center form-group"><input type="text"  class="form-control" disabled value="' . $pv->cost . '"></td><td class="price text-center form-group"><input type="text" min="0.00" class="form-control" disabled value="' . $pv->price . '"></span></td><td class="text-center"><i class="fa fa-ban desAttr"></i></td></tr>';
                                            }
                                            //  echo '<tr><td class="col-xs-3"><input type="hidden" name="variant_id_' . $pv->id . '" value="' . $pv->id . '"><input type="text" name="variant_name_' . $pv->id . '" value="' . $pv->name . '" class="form-control"></td><td class="price text-right col-xs-2"><input type="text" name="variant_price_' . $pv->id . '" value="' . $pv->price . '" class="form-control"></td></tr>';
                                        }
                                        echo $html;
                                    }
                                    ?>

                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div class="clearfix"></div>


                    </div>
                </div>





                <div class="col-md-12">


                    <div class="form-group all hidden">
                        <?= lang("product_details_for_invoice", "details") ?>
                        <?= form_textarea('details', (isset($_POST['details']) ? $_POST['details'] : ($product ? $product->details : '')), 'class="form-control" id="details"'); ?>
                    </div>

                    <div class="form-group hidden">
                        <label for="extra">Extra (Product meaurement)</label>
                        <?php
                        $size[''] = 'Select product size';
                        foreach ($sizes_data as $sizes_data) {
                            $size[$sizes_data->id] = $sizes_data->title;
                        }
                        echo form_dropdown('extra_size', $size, (isset($_POST['extra_size']) ? $_POST['extra_size'] : ($product ? $product->extra_size : '')), 'class="form-control" id="extra_size"');
                        ?>
                    </div>


                    <div class="form-group">
                        <?php echo form_submit('edit_product', $this->lang->line("edit_product"), 'class="btn btn-primary btn_edit_product_custom"'); ?>
                    </div>

                </div>
                <?= form_close(); ?>

            </div>

        </div>
    </div>
</div>
<script src="<?= $assets ?>js/sweetalert.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        //  $('form[data-toggle="validator"]').bootstrapValidator({excluded: [':disabled']});
        var audio_success = new Audio('<?= $assets ?>sounds/sound2.mp3');
        var audio_error = new Audio('<?= $assets ?>sounds/sound3.mp3');
        var items = {};
        <?php
        if ($combo_items) {
            echo '
                var ci = ' . json_encode($combo_items) . ';
                $.each(ci, function() { add_product_item(this); });
                ';
        }
        ?>
        <?=isset($_POST['cf']) ? '$("#extras").iCheck("check");' : '' ?>
        $('#extras').on('ifChecked', function () {
            $('#extras-con').slideDown();
        });
        $('#extras').on('ifUnchecked', function () {
            $('#extras-con').slideUp();
        });

        <?= isset($_POST['promotion']) || $product->promotion ? '$("#promotion").iCheck("check");' : '' ?>
        $('#promotion').on('ifChecked', function (e) {
            $('#promo').slideDown();
        });
        $('#promotion').on('ifUnchecked', function (e) {
            $('#promo').slideUp();
        });

        $('.attributes').on('ifChecked', function (event) {
            $('#options_' + $(this).attr('id')).slideDown();
        });
        $('.attributes').on('ifUnchecked', function (event) {
            $('#options_' + $(this).attr('id')).slideUp();
        });

        //$('#cost').removeAttr('required');
        $('#type').change(function () {
            var t = $(this).val();
            if (t !== 'standard') {
                $('.standard').slideUp();
                $('#unit').attr('disabled', true);
                $('#cost').attr('disabled', true);
            } else {
                $('.standard').slideDown();
                $('#unit').attr('disabled', false);
                $('#cost').attr('disabled', false);
            }
            if (t !== 'digital') {
                $('.digital').slideUp();
            } else {
                $('.digital').slideDown();
            }
            if (t !== 'combo') {
                $('.combo').slideUp();
            } else {
                $('.combo').slideDown();
            }
            if (t == 'standard' || t == 'combo') {
                $('.standard_combo').slideDown();
            } else {
                $('.standard_combo').slideUp();
            }
        });

        $("#add_item").autocomplete({
            source: '<?= admin_url('products/suggestions'); ?>',
            minLength: 1,
            autoFocus: false,
            delay: 5,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).val('');
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).val('');

                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    var row = add_product_item(ui.item);
                    if (row) {
                        $(this).val('');
                        $('#add_item').removeAttr('required');
                        $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'add_item');
                    }
                } else {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>');
                }
            }
        });
        $('#add_item').removeAttr('required');
        $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'add_item');

        function add_product_item(item) {
            if (item == null) {
                return false;
            }
            item_id = item.id;
            if (items[item_id]) {
                items[item_id].qty = (parseFloat(items[item_id].qty) + 1).toFixed(2);
            } else {
                items[item_id] = item;
            }

            $("#prTable tbody").empty();
            $.each(items, function () {
                var row_no = this.id;
                var newTr = $('<tr id="row_' + row_no + '" class="item_' + this.id + '"></tr>');
                tr_html = '<td><input name="combo_item_id[]" type="hidden" value="' + this.id + '"><input name="combo_item_name[]" type="hidden" value="' + this.name + '"><input name="combo_item_code[]" type="hidden" value="' + this.code + '"><span id="name_' + row_no + '">' + this.code + ' - ' + this.name + '</span></td>';
                tr_html += '<td><input class="form-control text-center rquantity" name="combo_item_quantity[]" type="text" value="' + formatQuantity2(this.qty) + '" data-id="' + row_no + '" data-item="' + this.id + '" id="quantity_' + row_no + '" onClick="this.select();"></td>';
                tr_html += '<td><input class="form-control text-center rprice" name="combo_item_price[]" type="text" value="' + formatDecimal(this.price) + '" data-id="' + row_no + '" data-item="' + this.id + '" id="combo_item_price_' + row_no + '" onClick="this.select();"></td>';
                tr_html += '<td class="text-center"><i class="fa fa-times tip del" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';
                newTr.html(tr_html);
                newTr.prependTo("#prTable");
            });
            $('.item_' + item_id).addClass('warning');
            //audio_success.play();
            return true;

        }

        function calculate_price() {
            var rows = $('#prTable').children('tbody').children('tr');
            var pp = 0;
            $.each(rows, function () {
                pp += formatDecimal(parseFloat($(this).find('.rprice').val()) * parseFloat($(this).find('.rquantity').val()));
            });
            $('#price').val(pp);
            return true;
        }

        $(document).on('change', '.rquantity, .rprice', function () {
            calculate_price();
        });

        $(document).on('click', '.del', function () {
            var id = $(this).attr('id');
            delete items[id];
            $(this).closest('#row_' + id).remove();
            calculate_price();
        });

        var su = 2;
        $('#addSupplier').click(function () {
            if (su <= 5) {
                $('#supplier_1').select2('destroy');
                $('#supplier_1').select2('destroy');
                var html = '<div style="clear:both;height:5px;"></div><div class="row"><div class="col-xs-12"><div class="form-group"><input type="hidden" name="supplier_' + su + '", class="form-control" id="supplier_' + su + '" placeholder="<?= lang("select") . ' ' . lang("supplier") ?>" style="width:100%;display: block !important;" /></div></div><div class="col-xs-6"><div class="form-group"><input type="text" name="supplier_' + su + '_part_no" class="form-control tip" id="supplier_' + su + '_part_no" placeholder="<?= lang('supplier_part_no') ?>" /></div></div><div class="col-xs-6"><div class="form-group"><input type="text" name="supplier_' + su + '_price" class="form-control tip" id="supplier_' + su + '_price" placeholder="<?= lang('supplier_price') ?>" /></div></div></div>';
                $('#ex-suppliers').append(html);
                var sup = $('#supplier_' + su);
                suppliers(sup);
                su++;
            } else {
                bootbox.alert('<?= lang('max_reached') ?>');
                return false;
            }
        });

        var _URL = window.URL || window.webkitURL;
        $("input#images").on('change.bs.fileinput', function () {
            var ele = document.getElementById($(this).attr('id'));
            var result = ele.files;
            $('#img-details').empty();
            for (var x = 0; x < result.length; x++) {
                var fle = result[x];
                for (var i = 0; i <= result.length; i++) {
                    var img = new Image();
                    img.onload = (function (value) {
                        return function () {
                            ctx[value].drawImage(result[value], 0, 0);
                        }
                    })(i);

                    img.src = 'images/' + result[i];
                }
            }
        });



        var variants = <?=json_encode($vars);?>;
        $(".select-tags").select2({
            tags: variants,
            tokenSeparators: [","],
            multiple: true
        });
        $(document).on('ifChecked', '#attributes', function (e) {
            $('#attr-con').slideDown();
        });
        $(document).on('ifUnchecked', '#attributes', function (e) {
            $(".select-tags").select2("val", "");
            $('.attr-remove-all').trigger('click');
            $('#attr-con').slideUp();
        });
        $('#addAttributes').click(function (e) {
            e.preventDefault();
            var attrs_val = $('#attributesInput').val(), attrs;
            attrs = attrs_val.split(',');
            for (var i in attrs) {
                if (attrs[i] !== '') {
                    $('#attrTable').show().append('<tr class="attr"><td><input type="hidden" name="attr_name[]" value="' + attrs[i] + '"><span>' + attrs[i] + '</span></td><td class="code text-center"><input type="hidden" name="attr_warehouse[]" value=""><span></span></td><td class="quantity text-center"><input type="hidden" name="attr_quantity[]" value="0"><span></span></td><td class="price text-right"><input type="hidden" name="attr_price[]" value="0"><span>0</span></span></td><td class="text-center"><i class="fa fa-times delAttr"></i></td></tr>');
                }
            }
        });
        $(document).on('click', '.delAttr', function () {
            $(this).closest("tr").remove();
        });
        $(document).on('click', '.attr-remove-all', function () {
            $('#attrTable tbody').empty();
            $('#attrTable').hide();
        });
        var row, warehouses = <?= json_encode($warehouses); ?>;
        $(document).on('click', '.attr td:not(:last-child)', function () {
            row = $(this).closest("tr");
            $('#aModalLabel').text(row.children().eq(0).find('span').text());
            $('#awarehouse').select2("val", (row.children().eq(1).find('input').val()));
            $('#aquantity').val(row.children().eq(2).find('span').text());
            $('#aprice').val(row.children().eq(3).find('span').text());
            $('#aModal').appendTo('body').modal('show');
        });

        $(document).on('click', '#updateAttr', function () {
            var wh = $('#awarehouse').val(), wh_name;
            $.each(warehouses, function () {
                if (this.id == wh) {
                    wh_name = this.name;
                }
            });
            row.children().eq(1).html('<input type="hidden" name="attr_warehouse[]" value="' + wh + '"><input type="hidden" name="attr_wh_name[]" value="' + wh_name + '"><span>' + wh_name + '</span>');
            row.children().eq(2).html('<input type="hidden" name="attr_quantity[]" value="' + ($('#aquantity').val() ? $('#aquantity').val() : 0) + '"><span>' + $('#aquantity').val() + '</span>');
            row.children().eq(3).html('<input type="hidden" name="attr_price[]" value="' + $('#aprice').val() + '"><span>' + currencyFormat($('#aprice').val()) + '</span>');

            $('#aModal').modal('hide');
        });
    });

    <?php if ($product) { ?>
    $(document).ready(function () {
        $('#enable_wh').click(function () {
            var whs = $('.wh');
            $.each(whs, function () {
                $(this).val($('#v' + $(this).attr('id')).val());
            });
            $('#warehouse_quantity').val(1);
            $('.wh').attr('disabled', false);
            $('#show_wh_edit').slideDown();
        });
        $('#disable_wh').click(function () {
            $('#warehouse_quantity').val(0);
            $('#show_wh_edit').slideUp();
        });
        $('#show_wh_edit').hide();
        $('.wh').attr('disabled', true);
        var t = "<?=$product->type?>";
        if (t !== 'standard') {
            $('.standard').slideUp();
            $('#unit').attr('disabled', true);
            $('#cost').attr('disabled', true);
        } else {
            $('.standard').slideDown();
            $('#unit').attr('disabled', false);
            $('#cost').attr('disabled', false);
        }
        if (t !== 'digital') {
            $('.digital').slideUp();
        } else {
            $('.digital').slideDown();
        }
        if (t !== 'combo') {
            $('.combo').slideUp();
        } else {
            $('.combo').slideDown();
        }
        if (t == 'standard' || t == 'combo') {
            $('.standard_combo').slideDown();
        } else {
            $('.standard_combo').slideUp();
        }
        $('#add_item').removeAttr('required');
        $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'add_item');
        //$("#code").parent('.form-group').addClass("has-error");
        //$("#code").focus();
        $("#product_image").parent('.form-group').addClass("text-warning");
        $("#images").parent('.form-group').addClass("text-warning");
        $.ajax({
            type: "get", async: false,
            url: "<?= admin_url('products/getSubCategories') ?>/" + <?= $product->category_id ?>,
            dataType: "json",
            success: function (scdata) {
                if (scdata != null) {
                    $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('select_subcategory') ?>").select2({
                        placeholder: "<?= lang('select_category_to_load') ?>",
                        minimumResultsForSearch: 7,
                        data: scdata
                    });
                } else {
                    $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= lang('no_subcategory') ?>").select2({
                        placeholder: "<?= lang('no_subcategory') ?>",
                        minimumResultsForSearch: 7,
                        data: [{id: '', text: '<?= lang('no_subcategory') ?>'}]
                    });
                }
            }
        });
        <?php if ($product->supplier1) { ?>
        select_supplier('supplier1', "<?= $product->supplier1; ?>");
        $('#supplier_price').val("<?= $this->sma->formatDecimal($product->supplier1price); ?>");
        $('#supplier_part_no').val("<?= $product->supplier1_part_no; ?>");
        <?php } else { ?>
        $('#supplier1').addClass('rsupplier');
        <?php } ?>
        <?php if ($product->supplier2) { ?>
        $('#addSupplier').click();
        select_supplier('supplier_2', "<?= $product->supplier2; ?>");
        $('#supplier_2_price').val("<?= $this->sma->formatDecimal($product->supplier2price); ?>");
        $('#supplier_2_part_no').val("<?= $product->supplier2_part_no; ?>");
        <?php } ?>
        <?php if ($product->supplier3) { ?>
        $('#addSupplier').click();
        select_supplier('supplier_3', "<?= $product->supplier3; ?>");
        $('#supplier_3_price').val("<?= $this->sma->formatDecimal($product->supplier3price); ?>");
        $('#supplier_3_part_no').val("<?= $product->supplier3_part_no; ?>");
        <?php } ?>
        <?php if ($product->supplier4) { ?>
        $('#addSupplier').click();
        select_supplier('supplier_4', "<?= $product->supplier4; ?>");
        $('#supplier_4_price').val("<?= $this->sma->formatDecimal($product->supplier4price); ?>");
        $('#supplier_4_part_no').val("<?= $product->supplier4_part_no; ?>");
        <?php } ?>
        <?php if ($product->supplier5) { ?>
        $('#addSupplier').click();
        select_supplier('supplier_5', "<?= $product->supplier5; ?>");
        $('#supplier_5_price').val("<?= $this->sma->formatDecimal($product->supplier5price); ?>");
        $('#supplier_5_part_no').val("<?= $product->supplier5_part_no; ?>");
        <?php } ?>
        function select_supplier(id, v) {
            $('#' + id).val(v).select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: "<?= admin_url('suppliers/getSupplier') ?>/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "suppliers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: 'No Match Found'}]};
                        }
                    }
                }
            });
        }
    });
    <?php } ?>
    $(document).ready(function () {
        var variantsAll = <?=json_encode($variants);?>;
        var saved_variants = <?=json_encode($saved_variants);?>;
        var allsavedvariants_name = <?=json_encode($allsavedvariants_name);?>;
        console.log(saved_variants);
        var count =1;
        var disable=0;
        $.each(saved_variants, function (keyp, valuep) {

            disable=0;


            $.ajax({
                type: "get",
                async: false,
                url: "<?= admin_url('products/getVarientValuesById') ?>?variant=" + valuep.variant+"&pid="+valuep.product_id,
                dataType: "json",
                success: function (data) {
                    if(data.disable==1){
                        disable =1;
                    }
                },
                error: function () {
                }
            });


            var html = '';
            html += '<div class="new_variant" style="margin-top: 30px; padding-top: 20px;">';
            html += '<div class="col-sm-3">';
            html += '<select class="form-control variants_dropdown iddd test1'+count+'" name="variants[]" id="variants_dropdown1">';
            if(disable == 0) {
            html += '<option value="">Select Varient</option>';
            }
            $.each(variantsAll, function (key, value) {
                if(valuep.variant == value.name){
                    html += '<option selected value="' + value.name + '">' + value.name + '</option>';
                }else {
                if(jQuery.inArray(value.name, allsavedvariants_name) !== -1){
                       // html += '<option value="' + value.name + '">' + value.name + '</option>';
                    }else{
                    if(disable == 0) {
                    html += '<option value="' + value.name + '">' + value.name + '</option>';
                }
                }
                }
            });

            html += '</select>';
            html += '</div>';
            html += ' <div class="col-sm-4">';
            html += '<input id="attributesInput2" name="attributesInput2[]" class="form-control select-tagse test'+count+'" placeholder="Enter Attributes">';
            html += ' </div>';


            var allAditionalGuide = <?=json_encode($additional_guide);?>;
            html += ' <div class="col-sm-3">';
            html += ' <select class="form-control" name="additional_guide[]"> <option value="">Additional Guide</option>';

            $.each(allAditionalGuide, function (key, value) {
                if(valuep.additional_guide == value.id){
                    html += '<option selected value="' + value.id + '">' + value.title + '</option>';
                }else {
                    html += '<option value="' + value.id + '">' + value.title + '</option>';
                }
            });
            html += '</select>';
            html += ' </div>';
            html += ' <div class="col-sm-1">';
            html += '<a class="img-variant cpointer" data-toggle="modal" data-target="#myModalImage">Images</a>';
            html += '</div>';

            html += ' <div class="col-sm-1">';
            html += '<a class="btn btn-success pull-right remVar testrem'+count+'"><i class="fa fa-trash"></i></a>';
            html += '</div>';

            html += '</div>';




            $('#newvariants').append(html);


            $.ajax({
                type: "get",
                async: false,
                url: "<?= admin_url('products/getVarientValuesById') ?>?variant=" + valuep.variant+"&pid="+valuep.product_id,
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    $('.test'+count).select2({
                        allowClear: true,
                        tags: true,
                        data: data.data
                    });
                    /*if(data.disable==1){
                        disable =1;
                    }*/

                    $('.test'+count).on("change", function (e) {
                        if (e.removed) {
                            genHtml();
                        }

                    });

                },
                error: function () {
                }
            });



            var array_saved_values = valuep.variant_value.split(',');
            console.log(array_saved_values);
            $('.test'+count).val(array_saved_values).trigger('change');
            if(disable == 1){
               // $('.test1'+count).attr('disabled',true);
                $('.testrem'+count).removeClass('remVar');
            }
            count++;
        });


        $('.variants_dropdown').on('change', function () {
            var variant = $(this).val();
            var that = $(this);
            // console.log(that.parent().next().find('.select-tagse'));
            if (variant) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: "<?= admin_url('products/getVarientValuesById_add') ?>/" + variant,
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        that.parent().next().find('.select-tagse').select2('destroy');
                        that.parent().next().find('.select-tagse').val('');
                        that.parent().next().find('.select-tagse').select2({
                            allowClear: true,
                            tags: true,
                            data: data
                        });

                        that.parent().next().find('.select-tagse').on("change", function (e) {
                            if (e.removed) {
                                genHtml();
                            }

                        });


                    },
                    error: function () {
                    }
                });

            } else {
                that.parent().next().find('.select-tagse').select2('destroy');
                that.parent().next().find('.select-tagse').val('');
            }
            //alert(variant);

        });


        var dynamic_id = 1;

        $('#add_more_variant').on('click', function () {
            var selected_varients = [];
            var inputs = $(".variants_dropdown");
            for (var i = 0; i < inputs.length; i++) {
                selected_varients.push($(inputs[i]).val());
            }

            var variantsAll = <?=json_encode($variants);?>;
            var html = '';
            html += '<div class="new_variant" style="margin-top: 30px; padding-top: 20px;">';
            html += '<div class="col-sm-4">';
            html += '<select class="form-control variants_dropdown" name="variants[]" id="s2id_variants_dropdown1">';
            html += '<option value="">Select Varient</option>';
            $.each(variantsAll, function (key, value) {
                if ($.inArray(value.name, selected_varients) !== -1) {
                } else {
                    html += '<option value="' + value.name + '">' + value.name + '</option>';
                }
            });

            html += '</select>';
            html += '</div>';
            html += ' <div class="col-sm-7">';
            html += '<input id="attributesInput2" name="attributesInput2[]" class="form-control select-tagse" placeholder="Enter Attributes">';
            html += ' </div>';
            html += ' <div class="col-sm-1">';
            html += '<a class="btn btn-success pull-right remVar" "><i class="fa fa-trash"></i></a>';
            html += '</div>';
            html += '</div>';

            $('#newvariants').append(html);

            $('.variants_dropdown').on('change', function () {
                var variant = $(this).val();
                var that = $(this);
                // console.log(that.parent().next().find('.select-tagse'));
                if (variant) {
                    $.ajax({
                        type: "get",
                        async: false,
                        url: "<?= admin_url('products/getVarientValuesById_add') ?>/" + variant,
                        dataType: "json",
                        success: function (data) {
                            console.log(data);
                            that.parent().next().find('.select-tagse').select2({
                                allowClear: true,
                                tags: true,
                                data: data
                            });
                            that.parent().next().find('.select-tagse').on("change", function (e) {
                                if (e.removed) {
                                    genHtml();
                                }

                            });
                        },
                        error: function () {
                        }
                    });


                } else {
                    that.parent().next().find('.select-tagse').select2('destroy');
                    that.parent().next().find('.select-tagse').val('');

                }
                //alert(variant);

            });

            $('.remVar').on('click', function () {
                // var tags = $("#attributesInput2").val();
                // console.log(tags+'sdsd');
                $(this).parents('.new_variant').remove();
                genHtml();

                return false;
            });


        });
        $('.remVar').on('click', function () {
            // var tags = $("#attributesInput2").val();
            // console.log(tags+'sdsd');
            $(this).parents('.new_variant').remove();
            genHtml();

            return false;
        });
        $('#gen_combinations').on('click', function () {
            genHtml();
            return false;
        });


        function genHtml() {
            var selected_varients = [];
            var selected_variant_values = [];
            var selected_variant_values_cm2 = [];
            var selected_variant_values_cm_all = [];
            var inputs = $(".variants_dropdown#s2id_variants_dropdown1");
            for (var i = 0; i < inputs.length; i++) {
                // selected_varients.push($(inputs[i]).val());
                selected_variant_values.push($(inputs[i]).parent().next().find('#attributesInput2').val());
            }
            var inputse = $(".variants_dropdown");
            for (var i = 0; i < inputse.length; i++) {
                selected_varients.push($(inputse[i]).val());
                // selected_variant_values.push($(inputs[i]).parent().next().find('#attributesInput2').val());
            }



            console.log(selected_varients);
            console.log(selected_variant_values);
            for (var i = 0; i < selected_variant_values.length; i++) {
                var array_values = selected_variant_values[i].split(',');
                var selected_variant_values_cm = [];
                for (j = 0; j < array_values.length; j++) {
                    selected_variant_values_cm.push(array_values[j]);

                }
                selected_variant_values_cm_all.push(selected_variant_values_cm)
            }

            var final_results = $.combinations(selected_variant_values_cm_all);
            var html = '';
            for (var i = 0; i < final_results.length; i++) {
                var newdata = final_results[i];
                var name = '';
                for (j = 0; j < newdata.length; j++) {
                    if (j > 0) {
                        if (newdata[0] != '') {
                            name += ' / ' + newdata[j];
                        } else {
                            name += newdata[j];
                        }

                    } else {
                        name += newdata[j];
                    }
                }
                html += '<tr class="attr1"><td><input type="hidden" name="attr_name[]" value="' + name + '"><span>' + name + '</span></td><td class="code1 text-center form-group "><input type="text"  class=" form-control" name="attr_supplier_code[]" value="0"></td><td class="supplier_price text-center form-group"><input type="text"  class="form-control" name="attr_supplier_price[]" value="0"></td><td class="price text-center form-group"><input type="text" class="form-control" name="attr_price[]" value="0"></span></td><td class="price text-center form-group"><label class="switch"> <input type="checkbox" class="slidet" checked > <span class="slider round"></span> </label>  <input id="website_status_val" type="hidden" value="1" name="website_status[]"> </td><td class="text-center"><i class="fa fa-times delAttr"></i></td></tr>';


            }
            $('#attrTable').show();
            $('#attrTable tbody#tbodayreq').html(html);
            $('.slidet').on('change', function(){
                console.log($(this).parent().next().find('#website_status_val'));
                var that =$(this).parent().next();
                this.checked ? that.val(1) : that.val(0);
                // alert(this.value);
            });
            console.log(final_results);
        }





        $('#enable_wh').trigger('click');
        $('#unit').change(function (e) {
            var v = $(this).val();
            if (v) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: "<?= admin_url('products/getSubUnits') ?>/" + v,
                    dataType: "json",
                    success: function (data) {
                        $('#default_sale_unit').select2("destroy").empty().select2({minimumResultsForSearch: 7});
                        $('#default_purchase_unit').select2("destroy").empty().select2({minimumResultsForSearch: 7});
                        $.each(data, function () {
                            $("<option />", {
                                value: this.id,
                                text: this.name + ' (' + this.code + ')'
                            }).appendTo($('#default_sale_unit'));
                            $("<option />", {
                                value: this.id,
                                text: this.name + ' (' + this.code + ')'
                            }).appendTo($('#default_purchase_unit'));
                        });
                        $('#default_sale_unit').select2('val', v);
                        $('#default_purchase_unit').select2('val', v);
                    },
                    error: function () {
                        bootbox.alert('<?= lang('ajax_error') ?>');
                    }
                });
            } else {
                $('#default_sale_unit').select2("destroy").empty();
                $('#default_purchase_unit').select2("destroy").empty();
                $("<option />", {
                    value: '',
                    text: '<?= lang('select_unit_first') ?>'
                }).appendTo($('#default_sale_unit'));
                $("<option />", {
                    value: '',
                    text: '<?= lang('select_unit_first') ?>'
                }).appendTo($('#default_purchase_unit'));
                $('#default_sale_unit').select2({minimumResultsForSearch: 7}).select2('val', '');
                $('#default_purchase_unit').select2({minimumResultsForSearch: 7}).select2('val', '');
            }
        });
        $('#digital_file').removeAttr('required');
        $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'digital_file');
    });


    /* Gallery image edit/delete */
    $(document).ready(function () {

        $('.yes-delete').click(function () {
            var gallery_img_id = $(this).attr('id');
            $('.box' + gallery_img_id).removeClass('yes-delete');

            $('.box' + gallery_img_id).before('<span id="' + gallery_img_id + '" class="img-overlay-style innerbox' + gallery_img_id + '"><span class="admin-img-overlay-text">x</span></span>');
        });

        $('.img_gallery_class').on('click', '.img-overlay-style', function () {
            var gallery_img_id = $(this).attr('id');
            $('.box' + gallery_img_id).addClass('yes-delete');

            $('.innerbox' + gallery_img_id).empty();
            $('.innerbox' + gallery_img_id).removeClass('img-overlay-style');
        });

        var product_saved_id= $('#product_saved_id').val();
        var file_up_names = new Array;



        $('.img-variant').on('click', function () {
            var variantGroup = $(this).parent().prev().prev().prev().find('.variants_dropdown option:selected').text();
            var variantValues = $(this).parent().prev().prev().find('#s2id_attributesInput2').val();
            var product_id = "<?= $product->id ?>";

            var html = '';
            var urlimag = '<?= site_url() . 'ucloud/plugins/filepreviewer/site/resize_image_inline.php?f='; ?>';
            var variableimages = <?=json_encode($variableimages);?>;
            console.log(variableimages);
            var dynamic_id = 2;
            $.each( variantValues, function( key, value ) {
                html += '<input type="hidden" name="product_id" value="'+product_id+'" >';
                html += '<input type="hidden" name="variable[]" value="'+value+'" >';

                html += ' <div class="col-sm-2" style="padding-top: 5px">';
                html += '<a class="">'+value+'</a>';
                html += '</div>';

                html += ' <div class="col-sm-2">';

                $.each(variableimages, function (ky, valuew) {
                    if(value == valuew.variable) {

                        html += ' <img src="' + urlimag + valuew.image + '&w=160&h=134&m=medium&uh=&o=jpg" class="img-thumbnail" alt="image" width="70" height="70"> ';

                    }
                });


                // html += ' <img src="https://www.w3schools.com/bootstrap/cinqueterre.jpg" class="img-thumbnail" alt="Cinque Terre" width="70" height="70"> ';
                html += '</div>';





                html += ' <div class="col-sm-2"> ';
                $.each(variableimages, function (ky, valuew) {
                    if(value == valuew.variable) {
                        html += ' Remove <input type="checkbox"  name="'+value+'" > ';
                    }
                });
                html += '</div>';



                html += ' <div class="col-sm-6" style="padding-top: 5px">';

                html +='<div class="input-group">';
                html +='   <input type="hidden" id="vimage_id'+dynamic_id+'" name="file['+value+']"  readonly class="form-control mediaimage_id" placeholder="Select Image">';
                html +='   <input type="text" id="vimage'+dynamic_id+'"  readonly class="form-control mediaimage" placeholder="Select Image">';
                html +='   <span class="input-group-btn ">';
                html +='   <button id="mediagallery" class="btn btn-primary mediagallery width120" type="button">Select Image</button>';
                html +='</span>';
                html +='</div>';

                var my_check = 0;

                $.each(variableimages, function (ky, valuew) {
                    if (value == valuew.variable) {
                        my_check = 1;
                        html += '<input class="alt_text" name="vimage_alt[' + value + ']" value="' + valuew.image_alt + '" type="hidden">';
                    }
                });
                if(my_check == 0) {
                    html += '<input class="alt_text" name="vimage_alt[' + value + ']" value="" type="hidden">';
                }





                // html += '<input type="file" name="'+value+'"/>';
                html += '</div>';
                html += '<div style="margin-bottom: 10px;" class="clearfix"></div>';


                //alert( key + ": " + value );
                dynamic_id = dynamic_id + 1;
            });

            $("#myModalImage .modal-body #modalpanel").html(html);
            mediaSettings();

        });


    });
    var dynamic_id =  <?php echo $galleryimagesCount; ?>;
    $('#add_more_gallery_image').on('click', function () {

        var html = '';
        html +='<div class="gallery_wrapper" style=" margin-top: 20px;">';
        html +='<div class="form-group all col-sm-11" style="padding-left: 0px; padding-right: 0px;">';
        html +='<div class="input-group">';
        html +='   <input type="hidden" id="gimage_id'+dynamic_id+'" name="gimage[]" readonly class="form-control mediaimage_id" placeholder="Select Image">';
        html +='   <input type="text" id="gimage'+dynamic_id+'" readonly class="form-control mediaimage" placeholder="Select Image">';
        html +='   <span class="input-group-btn ">';
        html +='   <button id="mediagallery" class="btn btn-primary mediagallery width120" type="button">Select Image</button>';
        html +='</span>';
        html +='</div>';
        html +='<input class="alt_text" name="gimage_alt[]" value="" type="hidden">';
        html +='</div>';
        html += ' <div class="col-sm-1" style="padding-left: 0px; padding-right: 0px;">';
        html += '<a class="btn btn-success pull-right remVargallery" ><i class="fa fa-trash"></i></a>';
        html += '</div>';
        html +=' </div>';
        dynamic_id=dynamic_id+1;
        $('#newgalleryimage').append(html);

        $('.remVargallery').on('click', function () {
            $(this).parents('.gallery_wrapper').remove();
            return false;
        });
        mediaSettings();


    });
    $('.remVargallery').on('click', function () {
        $(this).parents('.gallery_wrapper').remove();
        return false;
    });

</script>

<div class="modal" id="aModal" tabindex="-1" role="dialog" aria-labelledby="aModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                                class="fa fa-2x">&times;</i></span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="aModalLabel"><?= lang('add_product_manually') ?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="awarehouse" class="col-sm-4 control-label"><?= lang('warehouse') ?></label>
                        <div class="col-sm-8">
                            <?php
                            $wh[''] = '';
                            foreach ($warehouses as $warehouse) {
                                $wh[$warehouse->id] = $warehouse->name;
                            }
                            echo form_dropdown('warehouse', $wh, '', 'id="awarehouse" class="form-control"');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="aquantity" class="col-sm-4 control-label"><?= lang('quantity') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="aquantity">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="aprice" class="col-sm-4 control-label"><?= lang('price') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="aprice">
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="updateAttr"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="myModalImage" tabindex="-1" role="dialog" aria-labelledby="myModalImage" aria-hidden="true">
    <div class="modal-dialog cus_sizechart_modal_width">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" id="myModalLabel">Select Images For Saved Variables</h3>
            </div>
            <div class="modal-body">
                <div role="tabpanel" >
                    <form enctype="multipart/form-data" action="<?= base_url().'admin/products/variableimages' ?>" method="post">

                        <div id="modalpanel"></div>

                        <!--<input type="hidden" name="product_id" value="770" >
                        <input type="hidden" name="variable[]" value="large" >
                        <input type="text" name="variable[]" value="medium" >
                        <div class="form-group">
                            <label>Choose Files</label>
                            <input type="file" class="form-control" name="large" multiple/>
                        </div>
                        <div class="form-group">
                            <label>Choose Files</label>
                            <input type="file" class="form-control" name="medium" multiple/>
                        </div>-->
                        <div class="form-group">
                            <input class="btn btn-success pull-right" type="submit" name="fileSubmit" value="Save"/>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
