
<?php include __DIR__ . '/../header.php'; ?>
<div class="container normalfontsize">
    <div class="row justify-content-sm-center">
        <div class="col-sm-10">
      	<?php
      		echo section_header($title,lang('megamenu_manage_navigation'));
      		//echo infoMessage($message);
      	?>
        <div class="mt-3 mb-3 pull-right" style=" margin-bottom: 10px;">
            <a id="btn-add" style="background: #DB0B4E;" class="btn <?php echo config_item('button_color') .' '. config_item('button_color_text');?>-text waves-effect waves-light normalfontsize" href="<?php echo admin_url('megamenu/add_group'); ?>"><i class="fa fa-plus <?php echo config_item('button_color_text');?>-text"></i> <?php echo lang('megamenu_add_group')?></a>
        </div>
            <div class="clearfix"></div>

        <?php if ($groups): ?>
			<?php foreach ($groups as $group): ?>
    		<div class="info_wrapper">
    			<div class="info_header">
    				<span class="info_title"><?php echo $group['gname']; ?> (<?php echo $group['slug']; ?>)</span>
    				<a style="margin-right:15px; float: right;" class="delete float-right" data-type="group" data-href="<?php echo site_url('admin/megamenu/delete_group/'.$group['id']); ?>" data-name="<?php echo $group['gname']; ?>'" href="javascript:void();"><i class="fa fa-trash red-text"></i></a>
    				<a style="margin-right:15px; float: right;" class="float-right" href="<?php echo site_url('admin/megamenu/edit_group/'.$group['id']); ?>"><i class="fa fa-edit blue-text"></i></a>
    				<a style="margin-right:15px; float: right;" class="float-right" href="<?php echo site_url('admin/megamenu/add/'.$group['id']); ?>"><i class="fa fa-plus green-text"></i></a>
    			</div>
    			<div class="info_box">
    				<ol class="sortable">
    					<?php echo build_admin_tree($group['items']); ?>
    				</ol>
    			</div>
    		</div>
			<?php endforeach; ?>
        <?php else: ?>
            <div class="text-center mt-5">
                <h3><?php echo lang('megamenu_no_group')?></h3>
            </div>
        <?php endif; ?>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="confirmmodal" tabindex="-1" role="dialog" aria-labelledby="confirmmodalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="confirmmodalLabel"><?php echo lang('megamenu_are_you_sure'); ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <p><?php echo lang('megamenu_delete_sub_desc'); ?></p>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
<?php include __DIR__ . '/../footer.php'; ?>