<!--<link rel="stylesheet" href="<?php /*echo base_url('assets/assets-menu/css/fontawesome-all.min.css');*/?>">
  <link rel="stylesheet" href="<?php /*echo base_url('assets/assets-menu/css/bootstrap.min.css');*/?>">-->
<link rel="stylesheet" href="<?php echo base_url('assets/assets-menu/css/mdb.min.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/assets-menu/css/style.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/assets-menu/css/megamenu.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/assets-menu/css/spectrum.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/assets-menu/css/fontawesome-iconpicker.min.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/assets-menu/css/fontselect.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/assets-menu/css/bootstrap-slider.min.css');?>">
<link rel="stylesheet" href="<?php echo base_url('assets/assets-menu/css/frontend.css');?>">
<script type="text/javascript" src="<?php echo base_url('assets/assets-menu/js/jquery-3.2.1.min.js');?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/assets-menu/js/jquery-ui.min.js');?>"></script>



<script>
    var BASE_URL = "<?php echo base_url(); ?>";
    var LIST_MAX_LEVELS = "<?php echo $this->config->item('max_levels', 'megamenu');?>";
</script>

<style>
    .user-wrapper::-webkit-scrollbar-track
    {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        background-color: #F5F5F5;
    }

    .user-wrapper::-webkit-scrollbar
    {
        width: 6px;
        background-color: #F5F5F5;
    }

    .user-wrapper::-webkit-scrollbar-thumb
    {
        background-color: <?php echo config_item('scroller_color')?>;
    }

    .side-nav.fixed::-webkit-scrollbar-track
    {
        -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
        background-color: #F5F5F5;
    }

    .side-nav.fixed::-webkit-scrollbar
    {
        width: 6px;
        background-color: #F5F5F5;
    }

    .side-nav.fixed::-webkit-scrollbar-thumb
    {
        background-color: <?php echo config_item('scroller_color')?>;
    }
    .normalfontsize{
        font-size: 14px !important;
    }
    .normalfontbutton{
        font-size: 14px !important;
        background: #DB0B4E;
    }
    .width100percent{
        width:100%;
    }
    .font-select,.select2-container{
        width:100% !important;
    }
    td, th {
        padding: 10px;
    }
    .header_menu_heading {
        height: 100px;
        background: #DB0B4E;
        font-size: 18px;
        font-weight: bold;
        margin-bottom: 20px;
    }
    .inlinedisplay{
        display: inline;
    }
    .iconpickercss{
        display: inline;
        border: none;
        padding: 0px;
    }
</style>
<div style="background: #ffffff; padding-left: 100px">